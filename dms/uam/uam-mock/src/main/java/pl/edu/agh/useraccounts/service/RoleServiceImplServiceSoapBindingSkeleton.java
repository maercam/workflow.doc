/**
 * RoleServiceImplServiceSoapBindingSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package pl.edu.agh.useraccounts.service;

public class RoleServiceImplServiceSoapBindingSkeleton implements pl.edu.agh.useraccounts.service.RoleService, org.apache.axis.wsdl.Skeleton {
    private pl.edu.agh.useraccounts.service.RoleService impl;
    private static java.util.Map _myOperations = new java.util.Hashtable();
    private static java.util.Collection _myOperationsList = new java.util.ArrayList();

    /**
    * Returns List of OperationDesc objects with this name
    */
    public static java.util.List getOperationDescByName(java.lang.String methodName) {
        return (java.util.List)_myOperations.get(methodName);
    }

    /**
    * Returns Collection of OperationDescs
    */
    public static java.util.Collection getOperationDescs() {
        return _myOperationsList;
    }

    static {
        org.apache.axis.description.OperationDesc _oper;
        org.apache.axis.description.FaultDesc _fault;
        org.apache.axis.description.ParameterDesc [] _params;
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "login"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "role"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("addRole", _params, new javax.xml.namespace.QName("", "return"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        _oper.setElementQName(new javax.xml.namespace.QName("http://service.useraccounts.agh.edu.pl/", "addRole"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("addRole") == null) {
            _myOperations.put("addRole", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("addRole")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "login"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("getUserRole", _params, new javax.xml.namespace.QName("", "return"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        _oper.setElementQName(new javax.xml.namespace.QName("http://service.useraccounts.agh.edu.pl/", "getUserRole"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("getUserRole") == null) {
            _myOperations.put("getUserRole", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("getUserRole")).add(_oper);
        _fault = new org.apache.axis.description.FaultDesc();
        _fault.setName("UserException");
        _fault.setQName(new javax.xml.namespace.QName("http://service.useraccounts.agh.edu.pl/", "UserException"));
        _fault.setClassName("pl.edu.agh.useraccounts.service.UserException");
        _fault.setXmlType(new javax.xml.namespace.QName("http://service.useraccounts.agh.edu.pl/", "UserException"));
        _oper.addFault(_fault);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "role"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("createRole", _params, new javax.xml.namespace.QName("", "return"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        _oper.setElementQName(new javax.xml.namespace.QName("http://service.useraccounts.agh.edu.pl/", "createRole"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("createRole") == null) {
            _myOperations.put("createRole", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("createRole")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
        };
        _oper = new org.apache.axis.description.OperationDesc("getAllRoles", _params, new javax.xml.namespace.QName("", "return"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        _oper.setElementQName(new javax.xml.namespace.QName("http://service.useraccounts.agh.edu.pl/", "getAllRoles"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("getAllRoles") == null) {
            _myOperations.put("getAllRoles", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("getAllRoles")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "role"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("removeRole", _params, new javax.xml.namespace.QName("", "return"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        _oper.setElementQName(new javax.xml.namespace.QName("http://service.useraccounts.agh.edu.pl/", "removeRole"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("removeRole") == null) {
            _myOperations.put("removeRole", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("removeRole")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "login"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "role"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("revokeRole", _params, new javax.xml.namespace.QName("", "return"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        _oper.setElementQName(new javax.xml.namespace.QName("http://service.useraccounts.agh.edu.pl/", "revokeRole"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("revokeRole") == null) {
            _myOperations.put("revokeRole", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("revokeRole")).add(_oper);
    }

    public RoleServiceImplServiceSoapBindingSkeleton() {
        this.impl = new pl.edu.agh.useraccounts.service.RoleServiceImplServiceSoapBindingImpl();
    }

    public RoleServiceImplServiceSoapBindingSkeleton(pl.edu.agh.useraccounts.service.RoleService impl) {
        this.impl = impl;
    }
    public int addRole(java.lang.String login, java.lang.String role) throws java.rmi.RemoteException
    {
        int ret = impl.addRole(login, role);
        return ret;
    }

    public java.lang.String[] getUserRole(java.lang.String login) throws java.rmi.RemoteException, pl.edu.agh.useraccounts.service.UserException
    {
        java.lang.String[] ret = impl.getUserRole(login);
        return ret;
    }

    public int createRole(java.lang.String role) throws java.rmi.RemoteException
    {
        int ret = impl.createRole(role);
        return ret;
    }

    public java.lang.String[] getAllRoles() throws java.rmi.RemoteException
    {
        java.lang.String[] ret = impl.getAllRoles();
        return ret;
    }

    public int removeRole(java.lang.String role) throws java.rmi.RemoteException
    {
        int ret = impl.removeRole(role);
        return ret;
    }

    public int revokeRole(java.lang.String login, java.lang.String role) throws java.rmi.RemoteException
    {
        int ret = impl.revokeRole(login, role);
        return ret;
    }

}
