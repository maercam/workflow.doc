/**
 * RoleServiceImplServiceSoapBindingImpl.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package pl.edu.agh.useraccounts.service;

import java.rmi.RemoteException;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class RoleServiceImplServiceSoapBindingImpl implements RoleService {
	
	
	static Map<String,Set<String>> users = new ConcurrentHashMap<String, Set<String>>();
	static Set<String> roles =  Collections.newSetFromMap(new ConcurrentHashMap<String, Boolean>());
	
	
	public int addRole(String login, String role) throws RemoteException {
		Set<String> userRoles = getUserRoles(login);
		if(!userRoles.contains(role)){
			userRoles.add(role);
		}
		if(!roles.contains(role)){
			roles.add(role);
		}
		return 0;
	}

	private Set<String> getUserRoles(String login) {
		Set< String> userRoles = users.get(login);
		if(userRoles == null){
			userRoles = Collections.newSetFromMap(new ConcurrentHashMap<String, Boolean>());
		}
		users.put(login, userRoles);
		return userRoles;
	}

	public String[] getUserRole(String login) throws RemoteException, UserException {
		Set<String> rolesSet = getUserRoles(login);
		if(rolesSet != null){
			return rolesSet.toArray(new String[rolesSet.size()]);
		}
		throw new UserException();
	}

	public int createRole(String role) throws RemoteException {
		roles.add(role);
		return 0;
	}

	public String[] getAllRoles() throws RemoteException {
		return roles.toArray(new String[roles.size()]);
	}

	public int removeRole(String role) throws RemoteException {
		roles.remove(role);
		return 0;
	}

	public int revokeRole(String login, String role) throws RemoteException {
		Set<String> userRoles = getUserRoles(login);
		if(userRoles.contains(role)){
			userRoles.remove(role);
			return 0;
		}else{
			return 1;
		}
		
	}

}
