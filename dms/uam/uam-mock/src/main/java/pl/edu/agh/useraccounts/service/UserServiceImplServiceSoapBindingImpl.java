/**
 * UserServiceImplServiceSoapBindingImpl.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package pl.edu.agh.useraccounts.service;

import java.rmi.RemoteException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import pl.edu.agh.useraccounts.service.UserService;

public class UserServiceImplServiceSoapBindingImpl implements UserService {
	
	static Map<String,User> users = new ConcurrentHashMap<String, User>();
	
	private static class User{
		
		public User(String password, String email) {
			this.password = password;
			this.email = email;
		}
		public String password;
		public String email;
	}

	public int changePassword(String login, String oldPassword, String newPassword) throws RemoteException {
		if(login == null || oldPassword == null || newPassword == null) {
			return 3;
		}
		if(users.containsKey(login)){
			User user = users.get(login);
			if(user.password.equals(oldPassword)){
				user.password = newPassword;
				return 0;
			}else{
				return 2;
			}
		}else{
			return 1;
		}
	}

	public int register(String login, String email, String password) throws RemoteException {
		if(login == null || email == null || password == null) {
			return 3;
		}
		if(!users.containsKey(login)){
			users.put(login, new User(password, email));
		}
		return 0;
	}

	public int changeEmail(String login, String email) throws RemoteException {
		if(login == null || email == null) {
			return 3;
		}
		if( users.containsKey(login)){
			users.get(login).email = email;
			return 0;
		}else{
			return 1;
		}
		
	}

	public String[] getUsers() throws RemoteException {
		return users.keySet().toArray(new String[users.size()]);
	}

	public int remindPassword(String login) throws RemoteException {
		return -3;
	}

	public int authorization(String login, String password) throws RemoteException {
		if(login == null || password == null) {
			return 3;
		}
		if(users.containsKey(login)){
			if(users.get(login).password.equals(password)){
				return 0;
			}else{
				return 2;
			}
		}else{
			return 1;
		}
	}

	public int removeUser(String login) throws RemoteException {
		if(login == null) {
			return 3;
		}
		if(users.containsKey(login)){
			users.remove(login);
			return 0;
		}else{
			return 1;
		}
	}

}
