/**
 * UsersParametersServiceImplServiceSoapBindingSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package pl.edu.agh.useraccounts.service;

public class UsersParametersServiceImplServiceSoapBindingSkeleton implements pl.edu.agh.useraccounts.service.UsersParametersService, org.apache.axis.wsdl.Skeleton {
    private pl.edu.agh.useraccounts.service.UsersParametersService impl;
    private static java.util.Map _myOperations = new java.util.Hashtable();
    private static java.util.Collection _myOperationsList = new java.util.ArrayList();

    /**
    * Returns List of OperationDesc objects with this name
    */
    public static java.util.List getOperationDescByName(java.lang.String methodName) {
        return (java.util.List)_myOperations.get(methodName);
    }

    /**
    * Returns Collection of OperationDescs
    */
    public static java.util.Collection getOperationDescs() {
        return _myOperationsList;
    }

    static {
        org.apache.axis.description.OperationDesc _oper;
        org.apache.axis.description.FaultDesc _fault;
        org.apache.axis.description.ParameterDesc [] _params;
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "login"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "paramKey"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("getUserParam", _params, new javax.xml.namespace.QName("", "return"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        _oper.setElementQName(new javax.xml.namespace.QName("http://service.useraccounts.agh.edu.pl/", "getUserParam"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("getUserParam") == null) {
            _myOperations.put("getUserParam", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("getUserParam")).add(_oper);
        _fault = new org.apache.axis.description.FaultDesc();
        _fault.setName("UserException");
        _fault.setQName(new javax.xml.namespace.QName("http://service.useraccounts.agh.edu.pl/", "UserException"));
        _fault.setClassName("pl.edu.agh.useraccounts.service.UserException");
        _fault.setXmlType(new javax.xml.namespace.QName("http://service.useraccounts.agh.edu.pl/", "UserException"));
        _oper.addFault(_fault);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "login"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "paramKey"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "paramValue"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("setUserParam", _params, new javax.xml.namespace.QName("", "return"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        _oper.setElementQName(new javax.xml.namespace.QName("http://service.useraccounts.agh.edu.pl/", "setUserParam"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("setUserParam") == null) {
            _myOperations.put("setUserParam", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("setUserParam")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "login"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("getUserParams", _params, new javax.xml.namespace.QName("", "return"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://service.useraccounts.agh.edu.pl/", "parameters"));
        _oper.setElementQName(new javax.xml.namespace.QName("http://service.useraccounts.agh.edu.pl/", "getUserParams"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("getUserParams") == null) {
            _myOperations.put("getUserParams", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("getUserParams")).add(_oper);
        _fault = new org.apache.axis.description.FaultDesc();
        _fault.setName("UserException");
        _fault.setQName(new javax.xml.namespace.QName("http://service.useraccounts.agh.edu.pl/", "UserException"));
        _fault.setClassName("pl.edu.agh.useraccounts.service.UserException");
        _fault.setXmlType(new javax.xml.namespace.QName("http://service.useraccounts.agh.edu.pl/", "UserException"));
        _oper.addFault(_fault);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "startTime"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"), java.util.Calendar.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "endTime"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"), java.util.Calendar.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("getLogs", _params, new javax.xml.namespace.QName("", "return"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        _oper.setElementQName(new javax.xml.namespace.QName("http://service.useraccounts.agh.edu.pl/", "getLogs"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("getLogs") == null) {
            _myOperations.put("getLogs", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("getLogs")).add(_oper);
        _fault = new org.apache.axis.description.FaultDesc();
        _fault.setName("UserException");
        _fault.setQName(new javax.xml.namespace.QName("http://service.useraccounts.agh.edu.pl/", "UserException"));
        _fault.setClassName("pl.edu.agh.useraccounts.service.UserException");
        _fault.setXmlType(new javax.xml.namespace.QName("http://service.useraccounts.agh.edu.pl/", "UserException"));
        _oper.addFault(_fault);
    }

    public UsersParametersServiceImplServiceSoapBindingSkeleton() {
        this.impl = new pl.edu.agh.useraccounts.service.UsersParametersServiceImplServiceSoapBindingImpl();
    }

    public UsersParametersServiceImplServiceSoapBindingSkeleton(pl.edu.agh.useraccounts.service.UsersParametersService impl) {
        this.impl = impl;
    }
    public java.lang.String getUserParam(java.lang.String login, java.lang.String paramKey) throws java.rmi.RemoteException, pl.edu.agh.useraccounts.service.UserException
    {
        java.lang.String ret = impl.getUserParam(login, paramKey);
        return ret;
    }

    public int setUserParam(java.lang.String login, java.lang.String paramKey, java.lang.String paramValue) throws java.rmi.RemoteException
    {
        int ret = impl.setUserParam(login, paramKey, paramValue);
        return ret;
    }

    public pl.edu.agh.useraccounts.service.Parameters getUserParams(java.lang.String login) throws java.rmi.RemoteException, pl.edu.agh.useraccounts.service.UserException
    {
        pl.edu.agh.useraccounts.service.Parameters ret = impl.getUserParams(login);
        return ret;
    }

    public java.lang.String[] getLogs(java.util.Calendar startTime, java.util.Calendar endTime) throws java.rmi.RemoteException, pl.edu.agh.useraccounts.service.UserException
    {
        java.lang.String[] ret = impl.getLogs(startTime, endTime);
        return ret;
    }

}
