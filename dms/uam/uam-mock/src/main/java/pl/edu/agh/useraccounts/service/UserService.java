/**
 * UserService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package pl.edu.agh.useraccounts.service;

public interface UserService extends java.rmi.Remote {
    public int register(java.lang.String login, java.lang.String email, java.lang.String password) throws java.rmi.RemoteException;
    public int changePassword(java.lang.String login, java.lang.String oldPassword, java.lang.String newPassword) throws java.rmi.RemoteException;
    public int changeEmail(java.lang.String login, java.lang.String email) throws java.rmi.RemoteException;
    public int remindPassword(java.lang.String login) throws java.rmi.RemoteException;
    public java.lang.String[] getUsers() throws java.rmi.RemoteException;
    public int authorization(java.lang.String login, java.lang.String password) throws java.rmi.RemoteException;
    public int removeUser(java.lang.String login) throws java.rmi.RemoteException;
}
