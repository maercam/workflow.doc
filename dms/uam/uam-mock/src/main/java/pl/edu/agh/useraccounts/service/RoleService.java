/**
 * RoleService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package pl.edu.agh.useraccounts.service;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface RoleService extends Remote {
	
	public String[] getUserRole(String login) throws RemoteException, UserException;

	public int addRole(String login, String role) throws RemoteException;

	public String[] getAllRoles() throws RemoteException;

	public int createRole(String role) throws RemoteException;

	public int removeRole(String role) throws RemoteException;

	public int revokeRole(String login, String role) throws RemoteException;
}
