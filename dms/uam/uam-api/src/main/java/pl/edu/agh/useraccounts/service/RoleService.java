/**
 * RoleService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package pl.edu.agh.useraccounts.service;

public interface RoleService extends java.rmi.Remote {
    public java.lang.String[] getUserRole(java.lang.String login) throws java.rmi.RemoteException, pl.edu.agh.useraccounts.service.UserException;
    public int addRole(java.lang.String login, java.lang.String role) throws java.rmi.RemoteException;
    public java.lang.String[] getAllRoles() throws java.rmi.RemoteException;
    public int createRole(java.lang.String role) throws java.rmi.RemoteException;
    public int removeRole(java.lang.String role) throws java.rmi.RemoteException;
    public int revokeRole(java.lang.String login, java.lang.String role) throws java.rmi.RemoteException;
}
