/**
 * UsersParametersService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package pl.edu.agh.useraccounts.service;

public interface UsersParametersService extends java.rmi.Remote {
    public int setUserParam(java.lang.String login, java.lang.String paramKey, java.lang.String paramValue) throws java.rmi.RemoteException;
    public java.lang.String getUserParam(java.lang.String login, java.lang.String paramKey) throws java.rmi.RemoteException, pl.edu.agh.useraccounts.service.UserException;
    public pl.edu.agh.useraccounts.service.Parameters getUserParams(java.lang.String login) throws java.rmi.RemoteException, pl.edu.agh.useraccounts.service.UserException;
    public java.lang.String[] getLogs(java.util.Calendar startTime, java.util.Calendar endTime) throws java.rmi.RemoteException, pl.edu.agh.useraccounts.service.UserException;
}
