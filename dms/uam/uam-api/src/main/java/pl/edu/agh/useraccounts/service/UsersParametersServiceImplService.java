/**
 * UsersParametersServiceImplService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package pl.edu.agh.useraccounts.service;

public interface UsersParametersServiceImplService extends javax.xml.rpc.Service {
    public java.lang.String getUsersParametersServiceImplPortAddress();

    public pl.edu.agh.useraccounts.service.UsersParametersService getUsersParametersServiceImplPort() throws javax.xml.rpc.ServiceException;

    public pl.edu.agh.useraccounts.service.UsersParametersService getUsersParametersServiceImplPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
