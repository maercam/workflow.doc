package pl.edu.agh.useraccounts.service;

public class UsersParametersServiceProxy implements pl.edu.agh.useraccounts.service.UsersParametersService {
  private String _endpoint = null;
  private pl.edu.agh.useraccounts.service.UsersParametersService usersParametersService = null;
  
  public UsersParametersServiceProxy() {
    _initUsersParametersServiceProxy();
  }
  
  public UsersParametersServiceProxy(String endpoint) {
    _endpoint = endpoint;
    _initUsersParametersServiceProxy();
  }
  
  private void _initUsersParametersServiceProxy() {
    try {
      usersParametersService = (new pl.edu.agh.useraccounts.service.UsersParametersServiceImplServiceLocator()).getUsersParametersServiceImplPort();
      if (usersParametersService != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)usersParametersService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)usersParametersService)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (usersParametersService != null)
      ((javax.xml.rpc.Stub)usersParametersService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public pl.edu.agh.useraccounts.service.UsersParametersService getUsersParametersService() {
    if (usersParametersService == null)
      _initUsersParametersServiceProxy();
    return usersParametersService;
  }
  
  public int setUserParam(java.lang.String login, java.lang.String paramKey, java.lang.String paramValue) throws java.rmi.RemoteException{
    if (usersParametersService == null)
      _initUsersParametersServiceProxy();
    return usersParametersService.setUserParam(login, paramKey, paramValue);
  }
  
  public java.lang.String getUserParam(java.lang.String login, java.lang.String paramKey) throws java.rmi.RemoteException, pl.edu.agh.useraccounts.service.UserException{
    if (usersParametersService == null)
      _initUsersParametersServiceProxy();
    return usersParametersService.getUserParam(login, paramKey);
  }
  
  public pl.edu.agh.useraccounts.service.Parameters getUserParams(java.lang.String login) throws java.rmi.RemoteException, pl.edu.agh.useraccounts.service.UserException{
    if (usersParametersService == null)
      _initUsersParametersServiceProxy();
    return usersParametersService.getUserParams(login);
  }
  
  public java.lang.String[] getLogs(java.util.Calendar startTime, java.util.Calendar endTime) throws java.rmi.RemoteException, pl.edu.agh.useraccounts.service.UserException{
    if (usersParametersService == null)
      _initUsersParametersServiceProxy();
    return usersParametersService.getLogs(startTime, endTime);
  }
  
  
}