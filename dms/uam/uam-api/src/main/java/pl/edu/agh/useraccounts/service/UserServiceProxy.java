package pl.edu.agh.useraccounts.service;

public class UserServiceProxy implements pl.edu.agh.useraccounts.service.UserService {
  private String _endpoint = null;
  private pl.edu.agh.useraccounts.service.UserService userService = null;
  
  public UserServiceProxy() {
    _initUserServiceProxy();
  }
  
  public UserServiceProxy(String endpoint) {
    _endpoint = endpoint;
    _initUserServiceProxy();
  }
  
  private void _initUserServiceProxy() {
    try {
      userService = (new pl.edu.agh.useraccounts.service.UserServiceImplServiceLocator()).getUserServiceImplPort();
      if (userService != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)userService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)userService)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (userService != null)
      ((javax.xml.rpc.Stub)userService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public pl.edu.agh.useraccounts.service.UserService getUserService() {
    if (userService == null)
      _initUserServiceProxy();
    return userService;
  }
  
  public int register(java.lang.String login, java.lang.String email, java.lang.String password) throws java.rmi.RemoteException{
    if (userService == null)
      _initUserServiceProxy();
    return userService.register(login, email, password);
  }
  
  public int changePassword(java.lang.String login, java.lang.String oldPassword, java.lang.String newPassword) throws java.rmi.RemoteException{
    if (userService == null)
      _initUserServiceProxy();
    return userService.changePassword(login, oldPassword, newPassword);
  }
  
  public int changeEmail(java.lang.String login, java.lang.String email) throws java.rmi.RemoteException{
    if (userService == null)
      _initUserServiceProxy();
    return userService.changeEmail(login, email);
  }
  
  public int remindPassword(java.lang.String login) throws java.rmi.RemoteException{
    if (userService == null)
      _initUserServiceProxy();
    return userService.remindPassword(login);
  }
  
  public java.lang.String[] getUsers() throws java.rmi.RemoteException{
    if (userService == null)
      _initUserServiceProxy();
    return userService.getUsers();
  }
  
  public int authorization(java.lang.String login, java.lang.String password) throws java.rmi.RemoteException{
    if (userService == null)
      _initUserServiceProxy();
    return userService.authorization(login, password);
  }
  
  public int removeUser(java.lang.String login) throws java.rmi.RemoteException{
    if (userService == null)
      _initUserServiceProxy();
    return userService.removeUser(login);
  }
  
  
}