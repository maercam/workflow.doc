/**
 * UserServiceImplService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package pl.edu.agh.useraccounts.service;

public interface UserServiceImplService extends javax.xml.rpc.Service {
    public java.lang.String getUserServiceImplPortAddress();

    public pl.edu.agh.useraccounts.service.UserService getUserServiceImplPort() throws javax.xml.rpc.ServiceException;

    public pl.edu.agh.useraccounts.service.UserService getUserServiceImplPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
