package pl.edu.agh.useraccounts.service;

public class RoleServiceProxy implements pl.edu.agh.useraccounts.service.RoleService {
  private String _endpoint = null;
  private pl.edu.agh.useraccounts.service.RoleService roleService = null;
  
  public RoleServiceProxy() {
    _initRoleServiceProxy();
  }
  
  public RoleServiceProxy(String endpoint) {
    _endpoint = endpoint;
    _initRoleServiceProxy();
  }
  
  private void _initRoleServiceProxy() {
    try {
      roleService = (new pl.edu.agh.useraccounts.service.RoleServiceImplServiceLocator()).getRoleServiceImplPort();
      if (roleService != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)roleService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)roleService)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (roleService != null)
      ((javax.xml.rpc.Stub)roleService)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public pl.edu.agh.useraccounts.service.RoleService getRoleService() {
    if (roleService == null)
      _initRoleServiceProxy();
    return roleService;
  }
  
  public java.lang.String[] getUserRole(java.lang.String login) throws java.rmi.RemoteException, pl.edu.agh.useraccounts.service.UserException{
    if (roleService == null)
      _initRoleServiceProxy();
    return roleService.getUserRole(login);
  }
  
  public int addRole(java.lang.String login, java.lang.String role) throws java.rmi.RemoteException{
    if (roleService == null)
      _initRoleServiceProxy();
    return roleService.addRole(login, role);
  }
  
  public java.lang.String[] getAllRoles() throws java.rmi.RemoteException{
    if (roleService == null)
      _initRoleServiceProxy();
    return roleService.getAllRoles();
  }
  
  public int createRole(java.lang.String role) throws java.rmi.RemoteException{
    if (roleService == null)
      _initRoleServiceProxy();
    return roleService.createRole(role);
  }
  
  public int removeRole(java.lang.String role) throws java.rmi.RemoteException{
    if (roleService == null)
      _initRoleServiceProxy();
    return roleService.removeRole(role);
  }
  
  public int revokeRole(java.lang.String login, java.lang.String role) throws java.rmi.RemoteException{
    if (roleService == null)
      _initRoleServiceProxy();
    return roleService.revokeRole(login, role);
  }
  
  
}