/**
 * UserService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package pl.edu.agh.useraccounts.service;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface UserService extends Remote {
	
    public int register(String login, String email, String password) throws RemoteException;
    
    public int changePassword(String login, String oldPassword, String newPassword) throws RemoteException;
    
    public int changeEmail(String login, String email) throws RemoteException;
    
    public int remindPassword(String login) throws RemoteException;
    
    public String[] getUsers() throws RemoteException;
    
    public int authorization(String login, String password) throws RemoteException;
    
    public int removeUser(String login) throws RemoteException;
}
