package pl.dms.ejb;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

import javax.management.JMX;
import javax.management.MBeanServerConnection;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;

import org.junit.Ignore;
import org.junit.Test;

import pl.dms.commons.ServerConfig;
import pl.dms.commons.ServerConfigManager;

public class ServerConfigServiceL2Test  {

	private static final String SERVER_CONFIG_JMX_NAME = "type=pl.dms.ejb.ServerConfigService";

	@Test
	@Ignore
	public void testGetProperty() throws Throwable {
		// Get a connection to the JBoss AS MBean server on localhost
		ServerConfig cfg = ServerConfigManager.getSeverConfig();
		String host = cfg.documentManagementSystemHost();
		String port = cfg.jbossJmxPort(); 
		String urlString = System.getProperty("jmx.service.url", "service:jmx:remoting-jmx://" + host + ":" + port);
		JMXServiceURL serviceURL = new JMXServiceURL(urlString);
		JMXConnector jmxConnector = JMXConnectorFactory.connect(serviceURL, null);
		MBeanServerConnection connection = jmxConnector.getMBeanServerConnection();


		ObjectName mbeanName = new ObjectName("DMSManagement","type", "pl.dms.ejb.ServerConfigService");
		ServerConfigServiceMBean newMBeanProxy = JMX.newMBeanProxy(connection, mbeanName,
				ServerConfigServiceMBean.class, true);
		String dmsUrl = newMBeanProxy.documentManagementSystemUrl();
		
		assertThat(dmsUrl, is("http://localhost:8080/dms-ejb/DMSServiceWS/DMSServiceWSPortType"));
		String prevPropValue = newMBeanProxy.setProperty(ServerConfig.DMS_API_HOST, "ala");
		
		dmsUrl = newMBeanProxy.documentManagementSystemUrl();
		assertThat(dmsUrl, is("http://ala:8080/dms-ejb/DMSServiceWS/DMSServiceWSPortType"));
		newMBeanProxy.setProperty(ServerConfig.DMS_API_HOST, prevPropValue);
		
		jmxConnector.close();
	}
}
