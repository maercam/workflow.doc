package pl.dms.ejb;


import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

import java.rmi.RemoteException;

import org.junit.BeforeClass;
import org.junit.Test;

import pl.dms.service.ws.RemoteBaseL2Test;
import pl.edu.agh.useraccounts.service.RoleService;
import pl.edu.agh.useraccounts.service.RoleServiceProxy;
import pl.edu.agh.useraccounts.service.UserService;
import pl.edu.agh.useraccounts.service.UserServiceProxy;

public class StartupL2Test extends RemoteBaseL2Test{

	@Test
	public void testStart() throws RemoteException {
		
		boolean found = false;
		UserService userService = new UserServiceProxy();
		for(String user : userService.getUsers()){
			if(InstallService.DMS_ADMIN_USER.equals(user)){
				found = true;
			}
		}
		assertThat(found, is(true));
		
		found = false;
		
		RoleService roleService = new RoleServiceProxy();
		String[] userRoles = roleService.getUserRole(InstallService.DMS_ADMIN_USER);
		
		for(String role : userRoles){
			if(InstallService.DMS_ADMIN_ROLE.equals(role)){
				found = true;
			}
		}
		assertThat(found, is(true));
		
	}

}
