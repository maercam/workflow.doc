package pl.dms.ejb;

import javax.management.MXBean;

import pl.dms.commons.ServerConfigServiceApi;

@MXBean
public interface ServerConfigServiceMBean extends ServerConfigServiceApi {

}
