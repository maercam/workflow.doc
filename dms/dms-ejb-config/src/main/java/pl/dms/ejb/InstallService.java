package pl.dms.ejb;

import java.rmi.RemoteException;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import pl.dms.commons.Install;
import pl.dms.commons.ServerConfig;
import pl.dms.commons.ServerConfigManager;
import pl.edu.agh.useraccounts.service.RoleService;
import pl.edu.agh.useraccounts.service.RoleServiceProxy;
import pl.edu.agh.useraccounts.service.UserService;
import pl.edu.agh.useraccounts.service.UserServiceProxy;
@Singleton
@Startup
public class InstallService implements Install {

	Logger logger = LogManager.getLogger(InstallService.class);

	final static ServerConfig cfg = ServerConfigManager.getSeverConfig();
	
	public final static String DMS_ADMIN_ROLE = cfg.dmsAdminRole();
	
	private final static String DMS_ADMIN_PASS = "admin";
	
	

	RoleService roleService;
	UserService userService;

	public final String DMS_REGISTER_ROLE = cfg.dmsRegisteDocumentRole();
	public final String DMS_USE_ROLE = cfg.dmsUserRole();

	/* (non-Javadoc)
	 * @see pl.dms.ejb.Installa#createAdminAccount(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public void createAdminAccount(String user, String password, String email) {
		createAccount(user, password, email, DMS_ADMIN_ROLE);
	}
	
	
	public void createAccount(String user, String password, String email, String role) {
		try {
			roleService = new RoleServiceProxy();
			userService = new UserServiceProxy();
			if (!roleExists(role)) {
				if(roleService.createRole(role) != 0){
					throw new RuntimeException("Not created role=" + role);
				}
			}
			createUser(user, password, email, role);
		} catch (Exception e) {
			logger.error("Intialize exception", e);
			throw new RuntimeException("Intialize exception", e);
		}
	}
	
	

	private boolean roleExists(String role) throws RemoteException {
		String[] roles = roleService.getAllRoles();
		if(roles == null){
			return false;
		}
		for (String _role : roles) {
			if (role.equals(_role)) {
				return true;
			}
		}
		return false;
	}
	

	private void createUser(String adminLogin, String password, String email, String role) throws RemoteException {
		if(adminLogin == null){
			adminLogin = DMS_ADMIN_USER;
		}
		if(password == null){
			password = DMS_ADMIN_PASS;
		}
		if(email == null){
			email = "";
		}
		int status;
		String[] users = userService.getUsers();
		if(users != null){
			for (String user : users) {
				if (adminLogin.equals(user)) {
					String[] userRoles = roleService.getUserRole(user);
					if(userRoles != null){
						for (String userRole : userRoles) {
							if (role.equals(userRole)) {
								return;
							}
						}
					}
					if( roleService.addRole(user, role) != 0){
						throw new RuntimeException("Not revoke role=" + role + " user=" + user);
					}
				}
			}
		}
		if(userService.register(adminLogin, email, password) != 0){
			throw new RuntimeException("Not register user=DMS_ADMIN_USER");
		}
		status = roleService.addRole(adminLogin, role);
		if(status != 0){
			throw new RuntimeException("Not add role=" + role + " user=" + adminLogin + " with status  " + status);
		}
	}
	@PostConstruct
	public void init(){
		createAdminAccount(DMS_ADMIN_USER, DMS_ADMIN_PASS, "");
		logger.info("create operator");
		createAccount("operator", "operator", "", cfg.dmsRegisteDocumentRole());
		logger.info("create user");
		createAccount("user", "user", "", cfg.dmsUserRole());
	}
	
}
