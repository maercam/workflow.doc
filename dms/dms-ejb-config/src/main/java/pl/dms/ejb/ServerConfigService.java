package pl.dms.ejb;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.Reader;
import java.lang.management.ManagementFactory;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.management.MBeanServer;
import javax.management.ObjectName;

import pl.dms.commons.Install;
import pl.dms.commons.ServerConfig;
import pl.dms.commons.ServerConfigManager;

@Singleton
@Startup
public class ServerConfigService implements ServerConfigServiceMBean {

	private MBeanServer platformMBeanServer;
	private ObjectName objectName = null;
	private Install installService = new InstallService();
	@PostConstruct
	public void registerInJMX() {
		try {
			objectName = new ObjectName("DMSManagement:type=" + this.getClass().getName());
			platformMBeanServer = ManagementFactory.getPlatformMBeanServer();
			platformMBeanServer.registerMBean(this, objectName);
		} catch (Exception e) {
			throw new IllegalStateException("Problem during registration of Monitoring into JMX:" + e);
		}
	}

	public ServerConfigService() {
		System.out.println("ServerConfigService start");
	}

	final ServerConfig config = ServerConfigManager.getSeverConfig();

	@Override
	public String uamUserServiceUrl() {
		return config.uamRoleServiceUrl();
	}

	@Override
	public String uamRoleServiceUrl() {
		return config.uamRoleServiceUrl();
	}

	@Override
	public String uamUsersParamsServiceUrl() {
		return config.uamUsersParamsServiceUrl();
	}

	@Override
	public String documentManagementSystemUrl() {
		return config.documentManagementSystemUrl();
	}

	@Override
	public String documentManagementSystemHost() {
		return config.documentManagementSystemHost();
	}

	@Override
	public String documentManagementSystemPort() {
		return config.documentManagementSystemPort();
	}

	@Override
	public String documentManagementSystemPath() {
		return config.documentManagementSystemPath();
	}

	@Override
	public String setProperty(String key, String value) {
		return config.setProperty(key, value);
	}

	@Override
	public String removeProperty(String key) {
		return config.removeProperty(key);
	}

	@Override
	public void clear() {
		config.clear();
	}

	@Override
	public void load(InputStream inStream) throws IOException {
		config.load(inStream);
	}

	@Override
	public void load(Reader reader) throws IOException {
		config.load(reader);
	}

	@PreDestroy
	public void unregisterFromJMX() {
		try {
			platformMBeanServer.unregisterMBean(this.objectName);
		} catch (Exception e) {
			throw new IllegalStateException("Problem during unregistration of Monitoring into JMX:" + e);
		}
	}

	@Override
	public String jbossJmxPort() {
		return config.jbossJmxPort();
	}

	@Override
	public String uamUserServiceHost() {
		return config.uamUserServiceHost();
	}

	@Override
	public String uamUserServicePort() {
		return config.uamUserServicePort();
	}

	@Override
	public String uamUserServicePath() {
		return config.uamUserServicePath();
	}

	@Override
	public String uamRoleServiceHost() {
		return config.uamRoleServiceHost();
	}

	@Override
	public String uamRoleServicePort() {
		return config.uamRoleServicePort();
	}

	@Override
	public String uamRoleServicePath() {
		return config.uamRoleServicePath();
	}

	@Override
	public String uamUsersParamsServiceHost() {
		return config.uamUsersParamsServiceHost();
	}

	@Override
	public String uamUsersParamsServicePort() {
		return config.uamUserServicePort();
	}

	@Override
	public String uamUsersParamsServicePath() {
		return config.uamUsersParamsServicePath();
	}

	@Override
	public String uamHost() {
		return config.uamHost();
	}

	@Override
	public String uamPort() {
		return config.uamPort();
	}

	@Override
	public String dmsAdminRole() {
		return config.dmsAdminRole();
	}

	@Override
	public void createAdminAccount(String user, String password, String email) {
		installService.createAdminAccount(user, password, email);
	}

	@Override
	public void list(PrintStream out) {
		config.list(out);
		
	}

	@Override
	public void list(PrintWriter out) {
		config.list(out);
		
	}

	@Override
	public void store(OutputStream out, String comments) throws IOException {
		config.store(out, comments);
		
	}

	@Override
	public String getProperty(String key) {
		return config.getProperty(key);
	}

	@Override
	public String getProperty(String key, String defaultValue) {
		return config.getProperty(key,defaultValue);
	}

	@Override
	public String dmsRegisteDocumentRole() {
		return config.dmsRegisteDocumentRole();
	}

	@Override
	public String dmsUserRole() {
		return config.dmsUserRole();
	}

}
