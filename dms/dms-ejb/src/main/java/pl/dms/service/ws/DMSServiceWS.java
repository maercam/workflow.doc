package pl.dms.service.ws;


import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;
import javax.jws.soap.SOAPBinding.Use;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import pl.dms.ejb.Auth;
import pl.dms.ejb.UserAccountService;
import pl.dms.ejb.UserManager;
import pl.dms.service.DMSService;
import pl.dms.service.ws.model.ActionDTO;
import pl.dms.service.ws.model.AuthenticationResponse;
import pl.dms.service.ws.model.BaseResponse;
import pl.dms.service.ws.model.CreationResponse;
import pl.dms.service.ws.model.DocumentProcessDTO;
import pl.dms.service.ws.model.StateDTO;
import pl.dms.service.ws.model.WorkflowDTO;

@Stateless
@WebService(serviceName = "DMSServiceWS", name = "DMSServiceWSPortType", portName = "DMSServiceWSPort", targetNamespace = "http://service.dms.pl/ws")
@SOAPBinding(style = Style.RPC, use = Use.LITERAL)
public class DMSServiceWS implements DMSService {
	
	private static final String SERVER_EXCEPTION = "Server exception";

	Logger logger = LogManager.getLogger(DMSServiceWS.class);
	
	@Inject
	Auth auth;
	
	@Inject
	UserAccountService userAccountService;
	
	@Override
	@WebMethod(action = "login")
	@WebResult(name = "AuthenticationResponse", targetNamespace = "http://service.dms.pl/ws")
	public AuthenticationResponse login(@WebParam(name = "login") String login, @WebParam(name = "password") String password) {
		try{
			return userAccountService.authorization(login, password) ;
		}catch(Exception e){
			logger.error("Server error", e);
			AuthenticationResponse response = new AuthenticationResponse();
			createErrorResponse(response);
			return response;
		}
	}

	@Override
	@WebMethod(action = "logout")
	@WebResult(name = "LogoutResponse", targetNamespace = "http://service.dms.pl/ws")
	public BaseResponse logout(@WebParam(name = "sid") String sid) {
		try {
			return userAccountService.logout(sid);
		} catch (Exception e) {
			logger.error(SERVER_EXCEPTION, e);
			return createErrorBaseResponse();
		}
	}

	@Override
	@WebMethod(action = "getWorkflows")
	@WebResult(name = "WorkflowsResponse", targetNamespace = "http://service.dms.pl/ws")
	public WorkflowDTO[] getWorkflows(@WebParam(name = "sid") String sid) {
		try{
		UserManager userBean = auth.getUserManager(sid);
		return userBean.getWorkflows();
		}catch(Exception e){
			logger.error(SERVER_EXCEPTION, e);
			return new WorkflowDTO[0];
		}
	}

	@Override
	@WebMethod(action = "addWorkflow")
	@WebResult(name = "CreateWorkflowResponse", targetNamespace = "http://service.dms.pl/ws")
	public CreationResponse addWorkflow(@WebParam(name = "sid") String sid, @WebParam(name = "workflow") WorkflowDTO workflowDTO) {
		try {
			UserManager userBean = auth.getUserManager(sid);
			return userBean.addWorkflow(workflowDTO);
		} catch (Exception e) {
			logger.error(SERVER_EXCEPTION, e);
			return createErrorCreationResponse();
		}
	}

	@Override
	@WebMethod(action = "deleteWorkflow")
	@WebResult(name = "DeleteWorkflowResponse", targetNamespace = "http://service.dms.pl/ws")
	public BaseResponse deleteWorkflow(@WebParam(name = "sid") String sid, @WebParam(name = "workflowId") long workflowId) {
		try {
			UserManager userBean = auth.getUserManager(sid);
			return userBean.deleteWorkflow(workflowId);
		} catch (Exception e) {
			logger.error(SERVER_EXCEPTION, e);
			return createErrorBaseResponse();
		}
	}

	@Override
	@WebMethod(action = "getStates")
	@WebResult(name = "StatesResponse", targetNamespace = "http://service.dms.pl/ws")
	public StateDTO[] getStates(@WebParam(name = "sid") String sid, @WebParam(name = "workflowId") long workflowId) {
		try {
			UserManager userBean = auth.getUserManager(sid);
			return userBean.getStates(workflowId);
		} catch (Exception e) {
			logger.error(SERVER_EXCEPTION, e);
			return new StateDTO[0];
		}
	}

	@Override
	@WebMethod(action = "addState")
	@WebResult(name = "CreateStateResponse", targetNamespace = "http://service.dms.pl/ws")
	public CreationResponse addState(@WebParam(name = "sid") String sid, @WebParam(name = "state") StateDTO stateDTO, @WebParam(name = "workflowId") long workflowId) {
		try {
			UserManager userBean = auth.getUserManager(sid);
			return userBean.addState(stateDTO, workflowId);
		} catch (Exception e) {
			logger.error(SERVER_EXCEPTION, e);
			return createErrorCreationResponse();
		}
	}

	@Override
	@WebMethod(action = "deleteState")
	@WebResult(name = "DeleteStateResponse", targetNamespace = "http://service.dms.pl/ws")
	@TransactionAttribute(TransactionAttributeType.REQUIRED) 
	public BaseResponse deleteState(@WebParam(name = "sid") String sid, @WebParam(name = "stateId") long stateId) {
		try {
			UserManager userBean = auth.getUserManager(sid);
			return userBean.deleteState(stateId);
		} catch (Exception e) {
			logger.error(SERVER_EXCEPTION, e);
			return createErrorBaseResponse();
		}
	}

	@Override
	@WebMethod(action = "getActions")
	@WebResult(name = "ActionsResponse", targetNamespace = "http://service.dms.pl/ws")
	public ActionDTO[] getActions(@WebParam(name = "sid") String sid, @WebParam(name = "stateId") long stateId) {
		try {
			UserManager userBean = auth.getUserManager(sid);
			return userBean.getActions(stateId);
		} catch (Exception e) {
			logger.error(SERVER_EXCEPTION, e);
			return new ActionDTO[0];
		}
	}

	@Override
	@WebMethod(action = "addAcction")
	@WebResult(name = "CreateActionResponse", targetNamespace = "http://service.dms.pl/ws")
	public CreationResponse addAcction(@WebParam(name = "sid") String sid, @WebParam(name = "action") ActionDTO actionDTO, @WebParam(name = "stateId") long stateId) {
		try {
			UserManager userBean = auth.getUserManager(sid);
			return userBean.addAcction(actionDTO, stateId);
		} catch (Exception e) {
			logger.error(SERVER_EXCEPTION, e);
			return createErrorCreationResponse();
		}
	}

	@Override
	@WebMethod(action = "deleteAction")
	@WebResult(name = "DeleteActionResponse", targetNamespace = "http://service.dms.pl/ws")
	public BaseResponse deleteAction(@WebParam(name = "sid") String sid, @WebParam(name = "actionId") long actionId) {
		try {
			UserManager userBean = auth.getUserManager(sid);
			return userBean.deleteAction(actionId);
		} catch (Exception e) {
			logger.error(SERVER_EXCEPTION, e);
			return createErrorBaseResponse();
		}
		
	}

	@Override
	@WebMethod(action = "addDocument")
	@WebResult(name = "AddDocumentResponse", targetNamespace = "http://service.dms.pl/ws")
	public CreationResponse addDocument(@WebParam(name = "sid") String sid, @WebParam(name = "documentProcess") DocumentProcessDTO documentProcessDTO) {
		try {
			UserManager userBean = auth.getUserManager(sid);
			return userBean.addDocument(documentProcessDTO);
		} catch (Exception e) {
			logger.error(SERVER_EXCEPTION, e);
			return createErrorCreationResponse();
		}
	}

	@Override
	@WebMethod(action = "executeAction")
	@WebResult(name = "ExecuteActionResponse", targetNamespace = "http://service.dms.pl/ws")
	public BaseResponse executeAction(@WebParam(name = "sid") String sid, @WebParam(name = "documentId") long documentId, @WebParam(name = "actionId") long actionId) {
		try {
			UserManager userBean = auth.getUserManager(sid);
			return userBean.executeAction(documentId, actionId);
		} catch (Exception e) {
			logger.error(SERVER_EXCEPTION, e);
			return createErrorBaseResponse();
		}
	}

	@Override
	@WebMethod(action = "getPendingDocuments")
	@WebResult(name = "PendingDocumentsResponse", targetNamespace = "http://service.dms.pl/ws")
	public DocumentProcessDTO[] getPendingDocuments(@WebParam(name = "sid") String sid) {
		try {
			UserManager userBean = auth.getUserManager(sid);
			return userBean.getPendingDocuments();
		} catch (Exception e) {
			logger.error(SERVER_EXCEPTION, e);
			return new DocumentProcessDTO[0];
		}
	}

	@Override
	@WebMethod(action = "getAllDocuments")
	@WebResult(name = "AllDocumentsResponse", targetNamespace = "http://service.dms.pl/ws")
	public DocumentProcessDTO[] getAllDocuments(@WebParam(name = "sid") String sid) {
		try {
			UserManager userBean = auth.getUserManager(sid);
			return userBean.getAllDocuments();
		} catch (Exception e) {
			logger.error(SERVER_EXCEPTION, e);
			return new DocumentProcessDTO[0];
		}
	}

	@Override
	@WebMethod(action = "getArchiveDocuments")
	@WebResult(name = "ArchiveDocumentsResponse", targetNamespace = "http://service.dms.pl/ws")
	public DocumentProcessDTO[] getArchiveDocuments(@WebParam(name = "sid") String sid) {
		try {
			UserManager userBean = auth.getUserManager(sid);
			return userBean.getArchiveDocuments();
		} catch (Exception e) {
			logger.error(SERVER_EXCEPTION, e);
			return new DocumentProcessDTO[0];
		}
	}
	
	@Override
	@WebMethod(action = "setWorkflowCompleted")
	@WebResult(name = "BaseResponse", targetNamespace = "http://service.dms.pl/ws")
	public BaseResponse setWorkflowCompleted(
			@WebParam(name = "sid")String sid,
			@WebParam(name = "workflowId")long workflowId,
			@WebParam(name = "completed")boolean completed){
		return null;
	}
	
	void createErrorResponse(BaseResponse response){
		response.setDescription("Server error");
		response.setStatus(BaseResponse.SERVER_ERROR);
	}
	
	BaseResponse createErrorBaseResponse(){
		BaseResponse response = new BaseResponse();
		createErrorResponse(response);
		return response;
	}
	
	CreationResponse createErrorCreationResponse(){
		CreationResponse response = new CreationResponse();
		createErrorResponse(response);
		return response;
	}

}
