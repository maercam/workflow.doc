package pl.dms.service.ws.model;

import java.io.Serializable;
import java.util.ArrayList;

import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement
@XmlType(
		namespace = "http://service.dms.pl/ws/types/",
		name = "DocumentProcess", propOrder = { "id", "name", "workflowId", "documentId", "currentStateId", "pending" , "actionTraces"  }
)
public class DocumentProcessDTO implements Serializable {

	private static final long serialVersionUID = -6694216773438649711L;

	private long id;
	private String name;
	private long workflowId;
	private long documentId;
	private long currentStateId;
	private boolean pending;
	private ArrayList<ActionTraceDTO> actionTraces;

	public DocumentProcessDTO() {
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getWorkflowId() {
		return workflowId;
	}

	public void setWorkflowId(long workflowId) {
		this.workflowId = workflowId;
	}

	public long getDocumentId() {
		return documentId;
	}

	public void setDocumentId(long documentId) {
		this.documentId = documentId;
	}

	public long getCurrentStateId() {
		return currentStateId;
	}

	public void setCurrentStateId(long currentStateId) {
		this.currentStateId = currentStateId;
	}

	public boolean isPending() {
		return pending;
	}

	public void setPending(boolean pending) {
		this.pending = pending;
	}

	@XmlElementWrapper(name = "ActionTraces")
	public ArrayList<ActionTraceDTO> getActionTraces() {
		return actionTraces;
	}

	public void setActionTraces(ArrayList<ActionTraceDTO> actionTraces) {
		this.actionTraces = actionTraces;
	}



}
