package pl.dms.service.ws.model;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement
@XmlType(
		namespace = "http://service.dms.pl/ws/types/",
		name = "CreationResponse", propOrder = { "id"  }
)
public class CreationResponse extends BaseResponse {

	private static final long serialVersionUID = 4237713521525026650L;

	private long id;

	public CreationResponse() {
	}

	public CreationResponse(int status, String description) {
		super(status, description);
	}

	public CreationResponse(int status, String description, long id) {
		super(status, description);
		this.id = id;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

}
