package pl.dms.service;

import javax.ejb.Remote;

import pl.dms.service.ws.model.ActionDTO;
import pl.dms.service.ws.model.AuthenticationResponse;
import pl.dms.service.ws.model.BaseResponse;
import pl.dms.service.ws.model.CreationResponse;
import pl.dms.service.ws.model.DocumentProcessDTO;
import pl.dms.service.ws.model.StateDTO;
import pl.dms.service.ws.model.WorkflowDTO;

/**
 * Interfejs zapewniający przetwarzanie operacji związanych z systemem DMS
 */
@Remote
public interface DMSService {

	/*
	 * Zarządzanie sesją użytkownika
	 */
	
	/**
	 * 
	 * @param login
	 * @param password
	 * @return
	 */
	AuthenticationResponse login(String login, String password);

	/**
	 * @param sid
	 * @return
	 */
	BaseResponse logout(String sid);

	/*
	 * Operacje dotyczące workflow
	 */
	/**
	 * 
	 * @param sid
	 * @return
	 */
	WorkflowDTO[] getWorkflows(String sid);
	
	/**
	 * 
	 * @param sid
	 * @param workflowDTO
	 * @return
	 */
	CreationResponse addWorkflow(String sid, WorkflowDTO workflowDTO);
	
	/**
	 * @param sid
	 * @param workflowId
	 * @return
	 */
	BaseResponse deleteWorkflow(String sid, long workflowId);

	/*
	 * Operacje dotyczące stanów
	 */
	/**
	 * @param sid
	 * @param workflowId
	 * @return
	 */
	StateDTO[] getStates(String sid, long workflowId);

	/**
	 * @param sid
	 * @param stateDTO
	 * @param workflowId
	 * @return
	 */
	CreationResponse addState(String sid, StateDTO stateDTO, long workflowId);

	/**
	 * @param sid
	 * @param stateId
	 * @return
	 */
	BaseResponse deleteState(String sid, long stateId);

	/**
	 * @param sid
	 * @param workflowId
	 * @param completed
	 * @return
	 */
	BaseResponse setWorkflowCompleted(String sid, long workflowId, boolean completed);
	/*
	 * Operacje dotyczące akcji
	 */
	/**
	 * @param sid
	 * @param stateId
	 * @return
	 */
	ActionDTO[] getActions(String sid, long stateId);

	/**
	 * @param sid
	 * @param actionDTO
	 * @param stateId
	 * @return
	 */
	CreationResponse addAcction(String sid, ActionDTO actionDTO, long stateId);

	/**
	 * @param sid
	 * @param actionId
	 * @return
	 */
	BaseResponse deleteAction(String sid, long actionId);

	/*
	 * Operacje dodawania dokumentu
	 */
	/**
	 * @param sid
	 * @param documentProcessDTO
	 * @return
	 */
	CreationResponse addDocument(String sid, DocumentProcessDTO documentProcessDTO);

	/*
	 * Operacje wykonania akcji na dokumencie
	 */
	/**
	 * @param sid
	 * @param documentId
	 * @param actionId
	 * @return
	 */
	BaseResponse executeAction(String sid, long documentId, long actionId);

	/*
	 * Operacje do pobierania dokumentów dla danego użytkownika
	 */
	/**
	 * @param sid
	 * @return
	 */
	DocumentProcessDTO[] getPendingDocuments(String sid);

	/**
	 * @param sid
	 * @return
	 */
	DocumentProcessDTO[] getAllDocuments(String sid);

	/**
	 * @param sid
	 * @return
	 */
	DocumentProcessDTO[] getArchiveDocuments(String sid);

}
