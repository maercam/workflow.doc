package pl.dms.service.ws.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement
@XmlType(namespace = "http://service.dms.pl/ws/types/", name = "Workflow", propOrder = { "id", "name" })
public class WorkflowDTO implements Serializable {

	private static final long serialVersionUID = -8668575448299806167L;

	private long id;
	private String name;


	public WorkflowDTO() {
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
