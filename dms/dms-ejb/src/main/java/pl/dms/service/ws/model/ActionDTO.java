package pl.dms.service.ws.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement
@XmlType(namespace = "http://service.dms.pl/ws/types/", name = "Action", propOrder = { "id", "fromStateId", "toStateId", "role", "name", "type" })
public class ActionDTO implements Serializable {

	private static final long serialVersionUID = 6828365887130934381L;

	private long id;

	private long fromStateId;
	private long toStateId;
	private String role;
	private String name;
	private int type;

	public ActionDTO() {
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getFromStateId() {
		return fromStateId;
	}

	public void setFromStateId(long fromStateId) {
		this.fromStateId = fromStateId;
	}

	public long getToStateId() {
		return toStateId;
	}

	public void setToStateId(long toStateId) {
		this.toStateId = toStateId;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

}
