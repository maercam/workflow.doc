package pl.dms.service.ws.model;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement
@XmlType(
		namespace = "http://service.dms.pl/ws/types/",
		name = "AuthenticationResponse", propOrder = { "sid"}
)
public class AuthenticationResponse extends BaseResponse {

	private static final long serialVersionUID = -2341145521373960484L;

	private String sid;

	public AuthenticationResponse() {
	}

	public AuthenticationResponse(int status, String description) {
		super(status, description);
	}

	public AuthenticationResponse(int status, String description, String sid) {
		super(status, description);
		this.sid = sid;
	}

	public String getSid() {
		return sid;
	}

	public void setSid(String sid) {
		this.sid = sid;
	}

}
