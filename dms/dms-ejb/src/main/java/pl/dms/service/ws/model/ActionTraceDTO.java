package pl.dms.service.ws.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement
@XmlType(namespace = "http://service.dms.pl/ws/types/", name = "ActionTrace", propOrder = { "actionId", "login" })
public class ActionTraceDTO implements Serializable {

	private static final long serialVersionUID = 8850485040300894852L;

	private long actionId;
	private String login;

	public ActionTraceDTO() {
	}

	public long getActionId() {
		return actionId;
	}

	public void setActionId(long actionId) {
		this.actionId = actionId;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	

}
