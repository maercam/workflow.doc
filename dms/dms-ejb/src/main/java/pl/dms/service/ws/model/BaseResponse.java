package pl.dms.service.ws.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


@XmlRootElement
@XmlType(
		namespace = "http://service.dms.pl/ws/types/",
		name = "BaseResponse", propOrder = { "status", "description" }
)
public class BaseResponse implements Serializable {

	private static final long serialVersionUID = -1447304977734773083L;

	private int status;
	private String description;

	// TODO trzeba dokończyć
	public static final int OK = 0;
	public static final int FATAL = 1;
	public static final int DB_ERROR = 2;
	public static final int AUTHORIZATION_ERROR = 3;
	public static final int SERVER_ERROR = 4;
	public static final int ALREADY_EXISTS = 5;
	public static final int NOT_FOUND = 6;
	public static final int NOT_ALLOWED_OPERATION = 7;
	public static final int VALIDATION_ERROR = 8;

	public BaseResponse() {
	}

	public BaseResponse(int status, String description) {
		this.status = status;
		this.description = description;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
