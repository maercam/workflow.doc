package pl.dms.service.ws.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement
@XmlType(namespace = "http://service.dms.pl/ws/types/", name = "State", propOrder = { "id", "name", "status", "type" })
public class StateDTO implements Serializable {

	private static final long serialVersionUID = -5596085284277210971L;

	private long id;
	private String name;
	private int status;
	private int type;

	public StateDTO() {
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

}
