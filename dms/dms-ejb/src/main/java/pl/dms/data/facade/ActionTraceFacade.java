package pl.dms.data.facade;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import pl.dms.jpa.ActionTrace;

@Stateless
public class ActionTraceFacade extends AbstractFacade<ActionTrace> {

	public ActionTraceFacade() {
		super(ActionTrace.class);
	}

	@Inject
	EntityManager em;
	
	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	@Override
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}

}
