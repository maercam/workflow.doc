package pl.dms.data.converters;

import pl.dms.jpa.State;
import pl.dms.jpa.status.StateStatus;
import pl.dms.jpa.status.StateType;
import pl.dms.service.ws.model.StateDTO;

public class StateConverter implements Converter<State, StateDTO>{

	@Override
	public State dto2Entity(StateDTO dto) {
		State state = new State();
		state.setId(dto.getId());
		state.setName(dto.getName());
		state.setType(StateType.of(dto.getType()));
		state.setStatus(StateStatus.of(dto.getStatus()));
		return state;
	}

	@Override
	public StateDTO entity2Dto(State entity) {
		StateDTO dto = new StateDTO();
		dto.setId(entity.getId());
		dto.setName(entity.getName());
		dto.setStatus(entity.getStatus().getValue());
		dto.setType(entity.getType().getValue());
		return dto;
	}

}
