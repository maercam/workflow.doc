package pl.dms.data.converters;

public interface Converter<E,D>{
	E dto2Entity(D dto);
	D entity2Dto(E entity);
}
