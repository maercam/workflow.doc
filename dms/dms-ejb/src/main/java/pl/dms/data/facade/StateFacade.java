package pl.dms.data.facade;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import pl.dms.jpa.State;
@Stateless
public class StateFacade extends AbstractFacade<State> {

	public StateFacade() {
		super(State.class);
	}

	@Inject
	EntityManager em;
	
	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	@Override
	public void setEntityManager(EntityManager em) {
		this.em = em;	
	}
	
	

}
