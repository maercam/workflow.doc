package pl.dms.data.converters;

import pl.dms.jpa.Workflow;
import pl.dms.service.ws.model.WorkflowDTO;

public class WorkflowConverter implements Converter<Workflow, WorkflowDTO>{

	@Override
	public Workflow dto2Entity(WorkflowDTO dto) {
		Workflow entity = new Workflow();
		entity.setId(dto.getId());
		entity.setName(dto.getName());
		return entity;
	}

	@Override
	public WorkflowDTO entity2Dto(Workflow entity) {
		WorkflowDTO dto = new WorkflowDTO();
		dto.setId(entity.getId());
		dto.setName(entity.getName());
		return dto;
	}

}
