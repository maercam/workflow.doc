package pl.dms.data.facade;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import pl.dms.jpa.State;
import pl.dms.jpa.Workflow;
import pl.dms.jpa.status.StateType;

@Stateless
public class WorkflowFacade extends AbstractFacade<Workflow> {

	@Inject
	private
	EntityManager em;
	
	public WorkflowFacade(){
		super(Workflow.class);
	}

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	public void setEntityManager(EntityManager entityManager) {
		this.em = entityManager;
	}
	
	public State getStartSate(Workflow workflow){
		for(State state : workflow.getStates()){
			if(state.getType() == StateType.START){
				return state;
			}
		}
		throw new RuntimeException("Start state not exist");
	}
	
	public State getUnregisterdSate(Workflow workflow){
		for(State state : workflow.getStates()){
			if(state.getType() == StateType.NOTREGISTERED){
				return state;
			}
		}
		throw new RuntimeException("Start state not exist");
	}
	
	

}
