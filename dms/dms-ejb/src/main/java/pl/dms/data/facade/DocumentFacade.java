package pl.dms.data.facade;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import pl.dms.jpa.Action;
import pl.dms.jpa.ActionTrace;
import pl.dms.jpa.DocumentProcess;
import pl.dms.jpa.State;
import pl.dms.jpa.status.StateType;

@Stateless
public class DocumentFacade extends AbstractFacade<DocumentProcess> {

	public DocumentFacade() {
		super(DocumentProcess.class);
	}

	@Inject
	EntityManager em;
	
	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	@Override
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}
	
	public List<DocumentProcess> getPendingDocuments(String [] roles){
		List<String> userRoles = Arrays.asList(roles);
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<DocumentProcess> q = cb.createQuery(DocumentProcess.class);
		Root<DocumentProcess> d = q.from(DocumentProcess.class);
		Join<DocumentProcess, State> s = d.join("currentState");
		Join<State, Action> a = s.join("actionsFrom");
		q.select(d);
		q.where(a.get("role").in(userRoles));
		List<DocumentProcess> documentProcesses = em.createQuery(q).getResultList();
		return documentProcesses;
	}
	
	public List<DocumentProcess> getArchiveDocuments(String login){
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<DocumentProcess> q = cb.createQuery(DocumentProcess.class);
		Root<DocumentProcess> d = q.from(DocumentProcess.class);
		Join<DocumentProcess, ActionTrace> at = d.join("actionTraces");
		q.select(d);
		q.where(cb.equal(at.get("login"),login));
		List<DocumentProcess> documentProcesses = em.createQuery(q).getResultList();
		return documentProcesses;
	}
	
	
	
	

}
