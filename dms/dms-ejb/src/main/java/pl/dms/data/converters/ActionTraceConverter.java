package pl.dms.data.converters;

import pl.dms.jpa.ActionTrace;
import pl.dms.service.ws.model.ActionTraceDTO;

public class ActionTraceConverter implements Converter<ActionTrace, ActionTraceDTO>{

	@Override
	public ActionTrace dto2Entity(ActionTraceDTO dto) {
		ActionTrace entity = new ActionTrace();
		entity.setLogin(dto.getLogin());
		return entity;
	}

	@Override
	public ActionTraceDTO entity2Dto(ActionTrace entity) {
		ActionTraceDTO dto = new ActionTraceDTO();
		dto.setActionId(entity.getAction().getId());
		dto.setLogin(entity.getLogin());
		return dto;
	}

}
