package pl.dms.data.converters;

import java.util.ArrayList;

import pl.dms.jpa.ActionTrace;
import pl.dms.jpa.DocumentProcess;
import pl.dms.service.ws.model.ActionTraceDTO;
import pl.dms.service.ws.model.DocumentProcessDTO;

public class DocumentProcessConverter implements Converter<DocumentProcess, DocumentProcessDTO>{

	ActionTraceConverter actionTraceConverter = new ActionTraceConverter();
	
	@Override
	public DocumentProcess dto2Entity(DocumentProcessDTO dto) {
		DocumentProcess entity = new DocumentProcess();
		entity.setDocumentId(dto.getCurrentStateId());
		entity.setId(dto.getId());
		entity.setName(dto.getName());
		return entity;
	}

	@Override
	public DocumentProcessDTO entity2Dto(DocumentProcess entity) {
		DocumentProcessDTO dto = new DocumentProcessDTO();
		dto.setId(entity.getId());
		dto.setCurrentStateId(entity.getCurrentState().getId());
		dto.setDocumentId(entity.getDocumentId());
		dto.setName(entity.getName());
		dto.setPending(entity.isPending());
		dto.setWorkflowId(entity.getWorkflow().getId());

		
		ArrayList<ActionTraceDTO> actionDTOs = new ArrayList<ActionTraceDTO>();
		
		for(ActionTrace actionTrace : entity.getActionTraces()) {
			ActionTraceDTO actionTraceDTO = actionTraceConverter.entity2Dto(actionTrace);
			actionDTOs.add(actionTraceDTO);
		}
		dto.setActionTraces(actionDTOs);
		return dto;
	}

}
