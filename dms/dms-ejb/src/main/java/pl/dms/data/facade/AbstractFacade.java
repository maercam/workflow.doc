package pl.dms.data.facade;

import java.util.List;

import javax.persistence.EntityManager;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public abstract class AbstractFacade<T>{
	
	private Logger logger = LogManager.getLogger(AbstractFacade.class);
	
	private Class<T> entityClass;
	
	public AbstractFacade(Class<T> entityClass) {
		logger.info("create abstract class...");
		this.entityClass = entityClass;
	}

	protected abstract EntityManager getEntityManager();

	protected abstract void setEntityManager(EntityManager em);
	
	public void create(T entity) {
		logger.info("create entity...");
		getEntityManager().persist(entity);
	}

	public void edit(T entity) {
		logger.info("edit entity...");
		getEntityManager().merge(entity);
	}

	public void remove(T entity) {
		logger.info("remove entity...");
		getEntityManager().remove(getEntityManager().merge(entity));
	}

	public T find(Object id) {
		logger.info("find entity...");
		return getEntityManager().find(entityClass, id);
	}
	
	@SuppressWarnings("unchecked")
	public List<T> findBy(String field, Object value){
		logger.info("findBy entity...");
		StringBuilder sb = new StringBuilder("SELECT c FROM ");
		sb.append(entityClass.getName()).append(" c WHERE c.");
		sb.append(field);
		sb.append(" LIKE :value");
		logger.info(sb.toString());
		return getEntityManager()
				.createQuery(sb.toString())
				.setParameter("value", value)
				.getResultList();
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List<T> findAll() {
		
		javax.persistence.criteria.CriteriaQuery cq = getEntityManager()
				.getCriteriaBuilder().createQuery();
		cq.select(cq.from(entityClass));
		return getEntityManager().createQuery(cq).getResultList();
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List<T> findRange(int[] range) {

		javax.persistence.criteria.CriteriaQuery cq = getEntityManager()
				.getCriteriaBuilder().createQuery();
		cq.select(cq.from(entityClass));
		javax.persistence.Query q = getEntityManager().createQuery(cq);
		q.setMaxResults(range[1] - range[0]);
		q.setFirstResult(range[0]);
		return q.getResultList();
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public int count() {
		javax.persistence.criteria.CriteriaQuery cq = getEntityManager()
				.getCriteriaBuilder().createQuery();
		javax.persistence.criteria.Root<T> rt = cq.from(entityClass);
		cq.select(getEntityManager().getCriteriaBuilder().count(rt));
		javax.persistence.Query q = getEntityManager().createQuery(cq);
		return ((Long) q.getSingleResult()).intValue();
	}
}