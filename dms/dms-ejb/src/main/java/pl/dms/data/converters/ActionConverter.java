package pl.dms.data.converters;

import pl.dms.jpa.Action;
import pl.dms.jpa.State;
import pl.dms.jpa.status.ActionType;
import pl.dms.service.ws.model.ActionDTO;

public class ActionConverter implements Converter<Action, ActionDTO>{
	
	@Override
	public Action dto2Entity(ActionDTO dto){
		
		Action action = new Action();
		
		action.setId(dto.getId());
		action.setFromState(new State(dto.getFromStateId()));
		action.setToState(new State(dto.getToStateId()));
		action.setName(dto.getName());
		action.setRole(dto.getRole());
		action.setType(ActionType.of(dto.getType()));
		
		return action;
	}
	
	@Override
	public ActionDTO entity2Dto(Action entity){
		
		ActionDTO dto = new ActionDTO();
		
		dto.setId(entity.getId());
		dto.setFromStateId(entity.getFromState().getId());
		dto.setToStateId(entity.getToState().getId());
		dto.setName(entity.getName());
		dto.setRole(entity.getRole());
		dto.setType(entity.getType().getValue());
		
		return dto;
	}
}
