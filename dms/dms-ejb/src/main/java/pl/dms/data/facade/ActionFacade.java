package pl.dms.data.facade;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import pl.dms.jpa.Action;
@Stateless
public class ActionFacade extends AbstractFacade<Action> {

	@Inject
	EntityManager em;
	
	public ActionFacade() {
		super(Action.class);
	}
	
	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

	@Override
	public void setEntityManager(EntityManager em) {
		this.em = em;
		
	}

	
	

}
