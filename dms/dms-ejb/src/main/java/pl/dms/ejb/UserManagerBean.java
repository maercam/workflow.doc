package pl.dms.ejb;

import static pl.dms.service.ws.model.BaseResponse.OK;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateful;
import javax.inject.Inject;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import pl.dms.commons.ServerConfig;
import pl.dms.commons.ServerConfigManager;
import pl.dms.data.converters.ActionConverter;
import pl.dms.data.converters.DocumentProcessConverter;
import pl.dms.data.converters.StateConverter;
import pl.dms.data.converters.WorkflowConverter;
import pl.dms.data.facade.ActionFacade;
import pl.dms.data.facade.ActionTraceFacade;
import pl.dms.data.facade.DocumentFacade;
import pl.dms.data.facade.StateFacade;
import pl.dms.data.facade.WorkflowFacade;
import pl.dms.jpa.Action;
import pl.dms.jpa.ActionTrace;
import pl.dms.jpa.DocumentProcess;
import pl.dms.jpa.State;
import pl.dms.jpa.Workflow;
import pl.dms.jpa.status.ActionType;
import pl.dms.jpa.status.StateStatus;
import pl.dms.jpa.status.StateType;
import pl.dms.service.ws.model.ActionDTO;
import pl.dms.service.ws.model.BaseResponse;
import pl.dms.service.ws.model.CreationResponse;
import pl.dms.service.ws.model.DocumentProcessDTO;
import pl.dms.service.ws.model.StateDTO;
import pl.dms.service.ws.model.WorkflowDTO;
import pl.edu.agh.useraccounts.service.RoleService;
import pl.edu.agh.useraccounts.service.RoleServiceProxy;
import pl.edu.agh.useraccounts.service.UserException;

@Stateful
public class UserManagerBean implements UserManager {

	private static final String ROLE_SERVICE_ERROR = "role service error";

	private static final String DATABASE_ERROR = "Database error";

	private final Logger logger = LogManager.getLogger(UserManagerBean.class);

	private ServerConfig cfg = ServerConfigManager.getSeverConfig();

	@Inject
	private WorkflowFacade workflowFacade;

	@Inject
	private StateFacade stateFacade;

	@Inject
	private ActionFacade actionFacade;

	@Inject
	private UserAccountService userAccountService;

	@Inject
	private DocumentFacade documentFacade;

	@Inject
	private ActionTraceFacade actionTraceFacade;

	private String login;

	public UserManagerBean(String login) {
		this.setLogin(login);
	}

	public UserManagerBean() {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see pl.dms.ejb.UserManager#getWorkflows()
	 */
	@Override
	public WorkflowDTO[] getWorkflows() {
		List<Workflow> workflows = getWorkflowFacade().findAll();
		List<WorkflowDTO> workflowDTOs = new ArrayList<WorkflowDTO>();
		WorkflowConverter converter = new WorkflowConverter();
		for (Workflow workflow : workflows) {
			WorkflowDTO dto = converter.entity2Dto(workflow);
			workflowDTOs.add(dto);
		}
		return workflowDTOs.toArray(new WorkflowDTO[workflowDTOs.size()]);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * pl.dms.ejb.UserManager#addWorkflow(pl.dms.service.ws.model.WorkflowDTO)
	 */
	@Override
	public CreationResponse addWorkflow(WorkflowDTO workflowDTO) {
		logger.info("addWorkflow");
		List<Workflow> workflows = getWorkflowFacade().findAll();
		for (Workflow workflow : workflows) {
			if (workflow.getName().equals(workflowDTO.getName())) {
				CreationResponse response = new CreationResponse();
				response.setStatus(BaseResponse.ALREADY_EXISTS);
				response.setDescription("Object already exist");
				return response;
			}
		}

		WorkflowConverter converter = new WorkflowConverter();
		Workflow entity = converter.dto2Entity(workflowDTO);

		getWorkflowFacade().create(entity);
		State unregisteredState = new State();
		unregisteredState.setName("unregistered");
		unregisteredState.setType(StateType.NOTREGISTERED);
		unregisteredState.setWorkflow(entity);
		unregisteredState.setStatus(StateStatus.ACTIVE);
		entity.getStates().add(unregisteredState);

		getStateFacade().create(unregisteredState);

		State startState = new State();
		startState.setName("start");
		startState.setType(StateType.START);
		startState.setWorkflow(entity);
		startState.setStatus(StateStatus.ACTIVE);
		entity.getStates().add(startState);

		getStateFacade().create(startState);

		State endState = new State();
		endState.setName("end");
		endState.setType(StateType.END);
		endState.setWorkflow(entity);
		endState.setStatus(StateStatus.ACTIVE);
		entity.getStates().add(endState);

		getStateFacade().create(endState);

		Action registerAction = new Action();
		registerAction.setName("register");
		registerAction.setRole(cfg.dmsRegisteDocumentRole());

		registerAction.setType(ActionType.REGISTER);

		registerAction.setFromState(unregisteredState);
		unregisteredState.getActionsFrom().add(registerAction);

		registerAction.setToState(startState);
		startState.getActionsTo().add(registerAction);

		getActionFacade().create(registerAction);
		long id = entity.getId();
		return new CreationResponse(OK, "Workflow created", id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see pl.dms.ejb.UserManager#deleteWorkflow(long)
	 */
	@Override
	public BaseResponse deleteWorkflow(long workflowId) {
		BaseResponse response = new BaseResponse();
		Workflow workflow = getWorkflowFacade().find(workflowId);
		if (workflow == null) {
			response.setStatus(BaseResponse.NOT_FOUND);
			response.setDescription("Workflow with id " + workflowId + " not found");
			return response;
		}

		List<DocumentProcess> documentProcesses = workflow.getDocumentProcesses();
		for (DocumentProcess dp : documentProcesses) {
			dp.setWorkflow(null);
			dp.setCurrentState(null);
			for (ActionTrace at : dp.getActionTraces()) {
				at.setAction(null);
			}
		}

		getWorkflowFacade().remove(workflow);
		response = new BaseResponse(OK, "Workflow with id " + workflowId + " deleted");
		return response;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see pl.dms.ejb.UserManager#getStates(long)
	 */
	@Override
	public StateDTO[] getStates(long workflowId) {
		Workflow workflow = getWorkflowFacade().find(workflowId);
		if (workflow == null) {
			return new StateDTO[0];
		}
		List<State> states = workflow.getStates();
		List<StateDTO> stateDTOs = new ArrayList<StateDTO>();
		StateConverter converter = new StateConverter();
		StateDTO endStateDto = null;
		for (State state : states) {

			StateDTO stateDto = converter.entity2Dto(state);
			if (state.getType() != StateType.END) {
				stateDTOs.add(stateDto);
			} else {
				endStateDto = stateDto;
			}

		}
		stateDTOs.add(endStateDto);
		return stateDTOs.toArray(new StateDTO[stateDTOs.size()]);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see pl.dms.ejb.UserManager#addState(pl.dms.service.ws.model.StateDTO,
	 * long)
	 */
	@Override
	public CreationResponse addState(StateDTO stateDTO, long workflowId) {
		Workflow workflow = getWorkflowFacade().find(workflowId);
		if (workflow == null) {
			return new CreationResponse(BaseResponse.NOT_FOUND, "Workflow with id " + workflowId + "not found");
		}
		List<State> states = workflow.getStates();
		for (State state : states) {
			if (state.getName().equals(stateDTO.getName())) {
				CreationResponse response = new CreationResponse();
				response.setStatus(BaseResponse.ALREADY_EXISTS);
				response.setDescription("Object already exist");
				return response;
			}
		}
		StateConverter converter = new StateConverter();
		State newState = converter.dto2Entity(stateDTO);
		newState.setStatus(StateStatus.ACTIVE);
		newState.setType(StateType.STANDARD);
		states.add(newState);
		newState.setWorkflow(workflow);
		getStateFacade().create(newState);

		long id = newState.getId();
		CreationResponse response = new CreationResponse();
		if (id != 0) {
			response.setStatus(OK);
			response.setDescription("State with name " + newState.getName() + " created");
			response.setId(id);
		} else {
			response.setStatus(BaseResponse.DB_ERROR);
			response.setDescription(DATABASE_ERROR);
		}
		return response;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see pl.dms.ejb.UserManager#deleteState(long)
	 */
	@Override
	public BaseResponse deleteState(long stateId) {
		BaseResponse response = new BaseResponse();
		try {
			State state = getStateFacade().find(stateId);
			if (state == null) {
				response.setStatus(BaseResponse.NOT_FOUND);
				response.setDescription("State with id " + stateId + "not found");
				return response;
			}

			if (!state.getType().equals(StateType.STANDARD)) {
				response.setStatus(BaseResponse.NOT_ALLOWED_OPERATION);
				response.setDescription("This state cannot be removed");
				return response;
			}

			if (state.getActionsFrom().size() > 0 || state.getActionsTo().size() > 0) {
				response.setStatus(BaseResponse.VALIDATION_ERROR);
				response.setDescription("This state cannot be removed. Remove action from and action to before");
				return response;
			}

			String name = state.getName();

			Workflow workflow = state.getWorkflow();
			workflow.getStates().remove(state);
			getWorkflowFacade().edit(workflow);
			getStateFacade().remove(state);
			response.setStatus(OK);
			response.setDescription("State with name " + name + " deleted");

		} catch (Exception e) {
			response.setStatus(BaseResponse.DB_ERROR);
			response.setDescription(DATABASE_ERROR);
		}
		return response;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see pl.dms.ejb.UserManager#getActions(long)
	 */
	@Override
	public ActionDTO[] getActions(long stateId) {
		State state = getStateFacade().find(stateId);
		if (state == null) {
			return new ActionDTO[0];
		}
		List<Action> actions = state.getActionsFrom();

		ActionConverter converter = new ActionConverter();
		List<ActionDTO> actionDTOs = new ArrayList<ActionDTO>();
		for (Action action : actions) {
			ActionDTO dto = converter.entity2Dto(action);
			actionDTOs.add(dto);
		}
		return actionDTOs.toArray(new ActionDTO[actionDTOs.size()]);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see pl.dms.ejb.UserManager#addAcction(pl.dms.service.ws.model.ActionDTO,
	 * long)
	 */
	@Override
	public CreationResponse addAcction(ActionDTO actionDTO, long fromStateId) {
		CreationResponse response = new CreationResponse();

		if (actionDTO == null) {
			response.setStatus(BaseResponse.VALIDATION_ERROR);
			response.setDescription("Action cannot be null");
			return response;
		}
		if (actionDTO.getName() == null) {
			response.setStatus(BaseResponse.VALIDATION_ERROR);
			response.setDescription("Action name cannot be null");
			return response;
		}
		try {
			State fromState = getValidStateFrom(fromStateId);
			State toState = getValidStateTo(actionDTO.getToStateId());

			List<Action> actionsFrom = fromState.getActionsFrom();
			for (Action action : actionsFrom) {
				if (action.getName().equals(actionDTO.getName())) {
					response.setStatus(BaseResponse.ALREADY_EXISTS);
					response.setDescription("Action with name " + actionDTO.getName() + "already exist");
					return response;
				}
			}

			Action newAction = new ActionConverter().dto2Entity(actionDTO);

			newAction.setType(ActionType.STANDARD);
			
			actionsFrom.add(newAction);
			newAction.setFromState(fromState);
			
			toState.getActionsTo().add(newAction);
			newAction.setToState(toState);
			
			getActionFacade().create(newAction);

			for (Action action : actionsFrom) {
				if (action.getName().equals(actionDTO.getName())) {
					response.setStatus(OK);
					response.setDescription("Action with name " + actionDTO.getName() + " created");
					response.setId(newAction.getId());
					return response;
				}

			}
			response.setStatus(BaseResponse.DB_ERROR);
			response.setDescription(DATABASE_ERROR);
			return response;
		} catch (ValidationException e) {
			return e.getResponse();
		}
	}

	private State getValidStateTo(long stateId) throws ValidationException {
		State state = getStateFacade().find(stateId);
		validStateNull(stateId, state);
		validNoRegisteredState(state);

		if (state.getType() == StateType.START) {
			CreationResponse response = new CreationResponse();
			response.setStatus(BaseResponse.VALIDATION_ERROR);
			response.setDescription("Cannot add action to state start");
			throw new ValidationException(response);
		}
		return state;
	}

	private State getValidStateFrom(long stateId) throws ValidationException {
		State state = getStateFacade().find(stateId);
		validStateNull(stateId, state);
		validNoRegisteredState(state);

		if (state.getType() == StateType.END) {
			CreationResponse response = new CreationResponse();
			response.setStatus(BaseResponse.VALIDATION_ERROR);
			response.setDescription("Cannot add action to state end");
			throw new ValidationException(response);
		}
		return state;
	}

	private void validStateNull(long stateId, State state) throws ValidationException {
		if (state == null) {
			CreationResponse response = new CreationResponse();
			response.setStatus(BaseResponse.NOT_FOUND);
			response.setDescription("State with id " + stateId + "not found");
			throw new ValidationException(response);
		}
	}

	private void validNoRegisteredState(State state) throws ValidationException {
		if (state.getType() == StateType.NOTREGISTERED) {
			CreationResponse response = new CreationResponse();
			response.setStatus(BaseResponse.VALIDATION_ERROR);
			response.setDescription("Cannot add action to state unregister");
			throw new ValidationException(response);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see pl.dms.ejb.UserManager#deleteAction(long)
	 */
	@Override
	public BaseResponse deleteAction(long actionId) {
		BaseResponse response = new BaseResponse();
		try {
			Action action = getActionFacade().find(actionId);
			if (action == null) {
				response.setStatus(BaseResponse.NOT_FOUND);
				response.setDescription("Action with id " + actionId + " not found");
				return response;
			}
			if (action.getType().equals(ActionType.REGISTER)) {
				response.setStatus(BaseResponse.NOT_ALLOWED_OPERATION);
				response.setDescription("Remove action register is not allowed");
				return response;
			}
			
			if(!action.getActionTrace().isEmpty()){
				response.setStatus(BaseResponse.VALIDATION_ERROR);
				response.setDescription("Cannon remove executed actions. You only remove all workflow but all connected documents will be removed.");
				return response;
			}

			State fromState = action.getFromState();
			fromState.getActionsFrom().remove(action);
			getStateFacade().edit(fromState);
			getActionFacade().remove(action);
			response.setStatus(OK);
			response.setDescription("action deleted");
		} catch (Exception e) {
			response.setStatus(BaseResponse.DB_ERROR);
			response.setDescription(DATABASE_ERROR);
		}
		return response;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * pl.dms.ejb.UserManager#addDocument(pl.dms.service.ws.model.DocumentProcessDTO
	 * )
	 */
	@Override
	public CreationResponse addDocument(DocumentProcessDTO documentProcessDTO) {

		CreationResponse response = new CreationResponse();
		long workflowId = documentProcessDTO.getWorkflowId();
		Workflow workflow = getWorkflowFacade().find(workflowId);
		if (workflow == null) {
			response.setStatus(BaseResponse.NOT_FOUND);
			response.setDescription("Workflow with id " + workflowId + " not found");
			return response;
		}

		State unregisterdSate = getWorkflowFacade().getUnregisterdSate(workflow);
		Action register = unregisterdSate.getActionsFrom().get(0);
		try {
			if (!actionIsAllowed(register)) {
				response.setStatus(BaseResponse.NOT_ALLOWED_OPERATION);
				response.setDescription("operation not allowed for your roles. You must have role "
						+ register.getRole());
				return response;
			}
		} catch (ServerException e) {
			response.setStatus(BaseResponse.SERVER_ERROR);
			response.setDescription("Server exception");
			return response;
		}

		DocumentProcessConverter converter = new DocumentProcessConverter();
		DocumentProcess documentEntity = converter.dto2Entity(documentProcessDTO);

		documentEntity.setWorkflow(workflow);
		workflow.getDocumentProcesses().add(documentEntity);
		documentEntity.setCurrentState(unregisterdSate);
		unregisterdSate.getDocumentProcesses().add(documentEntity);
		getDocumentFacade().create(documentEntity);

		executeAction(documentEntity.getId(), register.getId());

		response.setStatus(OK);
		response.setDescription("Document created");
		response.setId(documentEntity.getId());
		return response;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see pl.dms.ejb.UserManager#executeAction(long, long)
	 */
	@Override
	public BaseResponse executeAction(long documentId, long actionId) {
		BaseResponse response = new BaseResponse();
		Action action = getActionFacade().find(actionId);
		if (action == null) {
			response.setStatus(BaseResponse.NOT_FOUND);
			response.setDescription("Action with id " + actionId + " not found");
			return response;
		}
		DocumentProcess document = getDocumentFacade().find(documentId);
		if (document == null) {
			response.setStatus(BaseResponse.NOT_FOUND);
			response.setDescription("Document process with id " + documentId + " not found");
			return response;
		}

		try {
			if (!actionIsAllowed(action)) {
				response.setStatus(BaseResponse.NOT_ALLOWED_OPERATION);
				response.setDescription("operation not allowed for your roles. You must have role " + action.getRole());
				return response;
			}
		} catch (ServerException e) {
			logger.error(ROLE_SERVICE_ERROR, e);
			response.setStatus(BaseResponse.SERVER_ERROR);
			response.setDescription(ROLE_SERVICE_ERROR);
			return response;
		}
		ActionTrace actionTrace = new ActionTrace();
		actionTrace.setLogin(login);
		actionTrace.setAction(action);
		actionTrace.setDocumentProcess(document);
		action.getActionTrace().add(actionTrace);
		document.getActionTraces().add(actionTrace);
		getActionTraceFacade().create(actionTrace);

		State destinationState = action.getToState();
		if (destinationState.getType() == StateType.END) {
			document.setPending(false);
		} else {
			document.setPending(true);
		}
		document.setCurrentState(destinationState);

		getDocumentFacade().edit(document);
		response.setStatus(OK);
		response.setDescription("Action " + action.getName() + " on process " + document.getName() + " executed");
		return response;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see pl.dms.ejb.UserManager#getPendingDocuments()
	 */
	@Override
	public DocumentProcessDTO[] getPendingDocuments() {
		try {
			List<DocumentProcessDTO> pendingDocumentsDto = getPendingDocumentsList();
			return pendingDocumentsDto.toArray(new DocumentProcessDTO[pendingDocumentsDto.size()]);
		} catch (Exception e) {
			logger.error("", e);
			return new DocumentProcessDTO[0];
		}
	}

	private List<DocumentProcessDTO> getPendingDocumentsList() throws RemoteException, UserException {
		RoleService roleService = new RoleServiceProxy();
		String[] roles = roleService.getUserRole(login);
		List<DocumentProcess> pendingDocuments = getDocumentFacade().getPendingDocuments(roles);
		List<DocumentProcessDTO> pendingDocumentsDto = new ArrayList<DocumentProcessDTO>();
		DocumentProcessConverter converter = new DocumentProcessConverter();
		for (DocumentProcess documentProcess : pendingDocuments) {
			DocumentProcessDTO dto = converter.entity2Dto(documentProcess);
			pendingDocumentsDto.add(dto);
		}
		return pendingDocumentsDto;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see pl.dms.ejb.UserManager#getAllDocuments()
	 */
	@Override
	public DocumentProcessDTO[] getAllDocuments() {
		try {
			List<DocumentProcessDTO> allDocuments = new ArrayList<DocumentProcessDTO>();
			if (!getUserAccountService().userHasRole(login, cfg.dmsAdminRole())) {
				List<DocumentProcessDTO> archiveDocumentsList = getArchiveDocumentsList();
				List<DocumentProcessDTO> pendingDocumentsList = getPendingDocumentsList();
				allDocuments.addAll(archiveDocumentsList);
				allDocuments.addAll(pendingDocumentsList);
			} else {
				List<DocumentProcess> all = getDocumentFacade().findAll();
				DocumentProcessConverter converter = new DocumentProcessConverter();
				for (DocumentProcess doc : all) {
					DocumentProcessDTO dto = converter.entity2Dto(doc);
					allDocuments.add(dto);
				}
			}
			DocumentProcessDTO[] documentsDtoArray = allDocuments.toArray(new DocumentProcessDTO[allDocuments.size()]);
			return documentsDtoArray;
		} catch (Exception e) {
			logger.error("", e);
			return new DocumentProcessDTO[0];
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see pl.dms.ejb.UserManager#getArchiveDocuments()
	 */
	@Override
	public DocumentProcessDTO[] getArchiveDocuments() {
		List<DocumentProcessDTO> archiveDocumentsDto = getArchiveDocumentsList();
		return archiveDocumentsDto.toArray(new DocumentProcessDTO[archiveDocumentsDto.size()]);

	}

	private List<DocumentProcessDTO> getArchiveDocumentsList() {
		List<DocumentProcess> archiveDocuments;
		archiveDocuments = getDocumentFacade().getArchiveDocuments(login);
		List<DocumentProcessDTO> archiveDocumentsDto;
		archiveDocumentsDto = new ArrayList<DocumentProcessDTO>();
		DocumentProcessConverter converter = new DocumentProcessConverter();
		for (DocumentProcess documentProcess : archiveDocuments) {
			DocumentProcessDTO dto = converter.entity2Dto(documentProcess);
			archiveDocumentsDto.add(dto);
		}
		return archiveDocumentsDto;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	@Override
	public UserAccountService getUserAccountService() {
		return userAccountService;
	}

	@Override
	public void setUserAccountService(UserAccountService userAccountService) {
		this.userAccountService = userAccountService;
	}

	public WorkflowFacade getWorkflowFacade() {
		return workflowFacade;
	}

	public void setWorkflowFacade(WorkflowFacade workflowFacade) {
		this.workflowFacade = workflowFacade;
	}

	public StateFacade getStateFacade() {
		return stateFacade;
	}

	public void setStateFacade(StateFacade stateFacade) {
		this.stateFacade = stateFacade;
	}

	public ActionFacade getActionFacade() {
		return actionFacade;
	}

	public void setActionFacade(ActionFacade actionFacade) {
		this.actionFacade = actionFacade;
	}

	/**
	 * @return the documentFacade
	 */
	public DocumentFacade getDocumentFacade() {
		return documentFacade;
	}

	/**
	 * @param documentFacade
	 *            the documentFacade to set
	 */
	public void setDocumentFacade(DocumentFacade documentFacade) {
		this.documentFacade = documentFacade;
	}

	/**
	 * @return the actionTraceFacade
	 */
	public ActionTraceFacade getActionTraceFacade() {
		return actionTraceFacade;
	}

	/**
	 * @param actionTraceFacade
	 *            the actionTraceFacade to set
	 */
	public void setActionTraceFacade(ActionTraceFacade actionTraceFacade) {
		this.actionTraceFacade = actionTraceFacade;
	}

	private boolean actionIsAllowed(Action a) throws ServerException {
		String role = a.getRole();
		return getUserAccountService().userHasRoles(login, role);
	}
}
