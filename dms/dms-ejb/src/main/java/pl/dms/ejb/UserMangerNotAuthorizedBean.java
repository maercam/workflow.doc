package pl.dms.ejb;

import pl.dms.service.ws.model.ActionDTO;
import pl.dms.service.ws.model.BaseResponse;
import pl.dms.service.ws.model.CreationResponse;
import pl.dms.service.ws.model.DocumentProcessDTO;
import pl.dms.service.ws.model.StateDTO;
import pl.dms.service.ws.model.WorkflowDTO;

public class UserMangerNotAuthorizedBean implements UserManager {

	private final String description;
	private final int status = BaseResponse.AUTHORIZATION_ERROR;
	
	public UserMangerNotAuthorizedBean(String description) {
		this.description = description;
	}

	@Override
	public WorkflowDTO[] getWorkflows() {
		return new WorkflowDTO[0];
	}

	@Override
	public CreationResponse addWorkflow(WorkflowDTO workflowDTO) {
		return getCreateResponse();
	}

	@Override
	public BaseResponse deleteWorkflow(long workflowId) {
		return getBaseResponse();
	}

	@Override
	public StateDTO[] getStates(long workflowId) {
		return new StateDTO[0];
	}

	@Override
	public CreationResponse addState(StateDTO stateDTO, long workflowId) {
		return getCreateResponse();
	}

	@Override
	public BaseResponse deleteState(long stateId) {
		return getBaseResponse();
	}

	@Override
	public ActionDTO[] getActions(long stateId) {
		return new ActionDTO[0];
	}

	@Override
	public CreationResponse addAcction(ActionDTO actionDTO, long stateId) {
		return getCreateResponse();
	}

	@Override
	public BaseResponse deleteAction(long actionId) {
		return getBaseResponse();
	}

	@Override
	public CreationResponse addDocument(DocumentProcessDTO documentProcessDTO) {
		return getCreateResponse();
	}

	@Override
	public BaseResponse executeAction(long documentId, long actionId) {
		return getBaseResponse();
	}

	@Override
	public DocumentProcessDTO[] getPendingDocuments() {
		return new DocumentProcessDTO[0];
	}

	@Override
	public DocumentProcessDTO[] getAllDocuments() {
		return new DocumentProcessDTO[0];
	}

	@Override
	public DocumentProcessDTO[] getArchiveDocuments() {
		return new DocumentProcessDTO[0];
	}

	@Override
	public void setLogin(String login) {	
	}

	@Override
	public String getLogin() {
		return null;
	}
	
	private CreationResponse getCreateResponse(){
		return new CreationResponse(status, description);
	}
	
	private BaseResponse getBaseResponse(){
		return new BaseResponse(status, description);
	}

	public void setUserAccountService(UserAccountService userAccountService) {
	}

	public UserAccountService getUserAccountService() {
		return null;
	}

}
