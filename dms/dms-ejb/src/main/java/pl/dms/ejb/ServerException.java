package pl.dms.ejb;

public class ServerException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6205072992144268712L;

	public ServerException() {
		super();
	}

	public ServerException(String message, Throwable cause) {
		super(message, cause);
	}

	public ServerException(String message) {
		super(message);
	}

	public ServerException(Throwable cause) {
		super(cause);
	}

}
