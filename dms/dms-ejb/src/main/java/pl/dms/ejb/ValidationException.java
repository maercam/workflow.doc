package pl.dms.ejb;

import pl.dms.service.ws.model.CreationResponse;

public class ValidationException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8036053433067832611L;
	
	private CreationResponse response;

	public ValidationException(CreationResponse response) {
		this.setResponse(response);
	}

	/**
	 * @return the response
	 */
	public CreationResponse getResponse() {
		return response;
	}

	/**
	 * @param response the response to set
	 */
	public void setResponse(CreationResponse response) {
		this.response = response;
	}
}
