package pl.dms.ejb;

import java.lang.reflect.Proxy;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import javax.ejb.Singleton;
import javax.naming.InitialContext;
import javax.naming.NamingException;


@Singleton
public class Auth {

	private static final String LOGIN_PASSWORD_OR_BOTH_INCORRECT = "Login, password or both incorrect";
	private static final String USER_MANAGER_NAME = "java:app/dms-ejb/UserManagerBean!pl.dms.ejb.UserManager";
	Map<String, UserManager> authMap = new ConcurrentHashMap<String, UserManager>();
			
    public Auth() {
    	
    }

	public String generateUserSession(String login) throws NamingException {
		String sid = UUID.randomUUID().toString();
		UserManager userManager = createUserManger(login);
		UserAccessProxyHandler userAccessProxyHandler = createUserMangerProxyHandler(userManager);

		UserManager userMangerProxy = (UserManager) Proxy
				.newProxyInstance(
						Thread.currentThread().getContextClassLoader(),
						new Class<?>[] { UserManager.class },
						userAccessProxyHandler);

		authMap.put(sid, userMangerProxy);
		return sid;
	}

	UserManager createUserManger(String login) throws NamingException {
		InitialContext ctx = new InitialContext();
		UserManager userManager = (UserManager) ctx.lookup(USER_MANAGER_NAME);
		userManager.setLogin(login);
		return userManager;
	}
	
	UserAccessProxyHandler createUserMangerProxyHandler(UserManager userManager) throws NamingException {
		UserAccessProxyHandler userManagerProxy = new UserAccessProxyHandler();
		userManagerProxy.setUserManager(userManager);
		return userManagerProxy;
	}
	
	public void logout(String sid) {
		authMap.remove(sid);
	}
	
	public UserManager getUserManager(String sid) {
		if (authMap.containsKey(sid)) {
			return authMap.get(sid);
		} else {
			return new UserMangerNotAuthorizedBean(LOGIN_PASSWORD_OR_BOTH_INCORRECT);
		}
	}
	

}
