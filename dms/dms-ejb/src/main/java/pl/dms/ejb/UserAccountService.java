package pl.dms.ejb;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.rmi.RemoteException;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.naming.NamingException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import pl.dms.commons.Role;
import pl.dms.commons.ServerConfig;
import pl.dms.commons.ServerConfigManager;
import pl.dms.service.ws.model.AuthenticationResponse;
import pl.dms.service.ws.model.BaseResponse;
import pl.edu.agh.useraccounts.service.RoleService;
import pl.edu.agh.useraccounts.service.RoleServiceProxy;
import pl.edu.agh.useraccounts.service.UserService;
import pl.edu.agh.useraccounts.service.UserServiceProxy;

@Stateless
public class UserAccountService {

	private final Logger logger = LogManager.getLogger(UserAccountService.class);
	
	private final static ServerConfig cfg = ServerConfigManager.getSeverConfig();

	
	
	@Inject
	private Auth auth;
	UserService userService = new UserServiceProxy();
    private RoleService roleService = new RoleServiceProxy();

	public AuthenticationResponse authorization(String login, String password) {
		try {
			// Zwracane jest 0 w przypadku powodzenia,
			// w przypadku niepowodzenia:
			// 1 jeśli login jest niepoprawny
			// 2 jeśli hasło jest niepoprawne
			int result = getUserService().authorization(login, password);
			if (result == 0) {
				String sid = getAuth().generateUserSession(login);
				return new AuthenticationResponse(AuthenticationResponse.OK, "Login ok", sid);
			} else {
				return new AuthenticationResponse(AuthenticationResponse.AUTHORIZATION_ERROR, "Username or password is invalid");
			}
		} catch (NamingException e) {
			logger.error("Unexpected remote exception", e);
			return new AuthenticationResponse(AuthenticationResponse.FATAL, "Unexpected exception");
		} catch (RemoteException e) {
			logger.error("UserService unavailable", e);
			return new AuthenticationResponse(AuthenticationResponse.FATAL, "UserService unavailable");
		}
	}

	public BaseResponse logout(String sid) {
		getAuth().logout(sid);
		return new BaseResponse(BaseResponse.OK, "Logout fine");
	}
	
	public boolean isAllowed(String login, Method action) throws ServerException {

		Annotation[] annotations = action.getAnnotations();
		if (annotations.length == 0) {
			return true;
		}
		if (!(annotations[0] instanceof Role)) {
			return true;
		}

		Role role = (Role) annotations[0];

		boolean forAdminRole = cfg.getProperty(role.value()).equals(cfg.dmsAdminRole());
		if(forAdminRole){
			return userHasRole(login, cfg.dmsAdminRole());
		}
		
		boolean forRegisterDocumentRole = cfg.getProperty(role.value()).equals(cfg.dmsRegisteDocumentRole());
		if (forRegisterDocumentRole) {
			return userHasRoles(login,
				cfg.dmsRegisteDocumentRole(),
				cfg.dmsAdminRole());
		}

		boolean forUserRole = cfg.getProperty(role.value()).equals(cfg.dmsUserRole());

		if (forUserRole) {
			return userHasRoles(login,
				cfg.dmsUserRole(),
				cfg.dmsRegisteDocumentRole(),
				cfg.dmsAdminRole());
		}
		return false;
	}
	
	public boolean userHasRoles(String login, String... roles) throws ServerException{
		try {
			String[] userRoles = getRoleService().getUserRole(login);
			if(userRoles == null){
				return false;
			}
			for(String userRole : userRoles){
				for(String role : roles){
					if(role.equals(userRole)){
						return true;
					}
				}
				
			}
			return false;
		} catch (RemoteException e) {
			logger.error("Server error", e);
			throw new ServerException("Server error", e);
		}
	}
	
	public boolean userHasRole(String login, String role) throws ServerException{
		try {
			String[] roles = getRoleService().getUserRole(login);
			if(roles == null){
				return false;
			}
			for(String userRole : roles){
				if(role.equals(userRole)){
					return true;
				}
			}
			return false;
		} catch (RemoteException e) {
			logger.error("Server error", e);
			throw new ServerException("Server error", e);
		}
		
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public Auth getAuth() {
		return auth;
	}

	public void setAuth(Auth auth) {
		this.auth = auth;
	}

	public RoleService getRoleService() {
		return roleService;
	}

	public void setRoleService(RoleService roleService) {
		this.roleService = roleService;
	}
}
