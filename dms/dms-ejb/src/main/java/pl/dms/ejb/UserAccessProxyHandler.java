package pl.dms.ejb;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class UserAccessProxyHandler implements InvocationHandler {

	private final Logger logger = LogManager.getLogger(UserAccessProxyHandler.class);

	private UserManager userManager;

	private String description = "You do not have permission to call that method";

	public UserAccessProxyHandler() {
	}

	public UserAccessProxyHandler(UserManager userManager) {
		this.userManager = userManager;
	}

	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		try {
			if (userManager.getUserAccountService().isAllowed(getUserManager().getLogin(), method)) {
				return method.invoke(getUserManager(), args);
			} else {
				return method.invoke(new UserMangerNotAuthorizedBean(description), args);
			}
		} catch (Exception ex) {
			logger.error("",ex);
			Throwable e = ex;
			if(e instanceof InvocationTargetException){
				e = e.getCause();
			}
			throw e;
		}
	}

	public UserManager getUserManager() {
		return userManager;
	}

	public void setUserManager(UserManager userManager) {
		this.userManager = userManager;
	}
}
