package pl.dms.ejb;

import pl.dms.service.ws.model.ActionDTO;
import pl.dms.service.ws.model.BaseResponse;
import pl.dms.service.ws.model.CreationResponse;
import pl.dms.service.ws.model.DocumentProcessDTO;
import pl.dms.service.ws.model.StateDTO;
import pl.dms.service.ws.model.WorkflowDTO;
import pl.dms.commons.Role;
import static pl.dms.commons.ServerConfig.DMS_ADMIN_ROLE;
import static pl.dms.commons.ServerConfig.DMS_REGISTER_DOCUMENT_ROLE;
import static pl.dms.commons.ServerConfig.DMS_USER_ROLE;

public interface UserManager {
	
	@Role(DMS_USER_ROLE)
	WorkflowDTO[] getWorkflows();

	@Role(DMS_ADMIN_ROLE)
	CreationResponse addWorkflow(WorkflowDTO workflowDTO);

	@Role(DMS_ADMIN_ROLE)
	BaseResponse deleteWorkflow(long workflowId);

	@Role(DMS_USER_ROLE)
	StateDTO[] getStates(long workflowId);
	
	@Role(DMS_ADMIN_ROLE)
	CreationResponse addState(StateDTO stateDTO, long workflowId);

	@Role(DMS_ADMIN_ROLE)
	BaseResponse deleteState(long stateId);

	@Role(DMS_USER_ROLE)
	ActionDTO[] getActions(long stateId);

	@Role(DMS_ADMIN_ROLE)
	CreationResponse addAcction(ActionDTO actionDTO, long stateId);

	@Role(DMS_ADMIN_ROLE)
	BaseResponse deleteAction(long actionId);

	@Role(DMS_REGISTER_DOCUMENT_ROLE)
	CreationResponse addDocument(DocumentProcessDTO documentProcessDTO);

	@Role(DMS_USER_ROLE)
	BaseResponse executeAction(long documentId, long actionId);

	@Role(DMS_USER_ROLE)
	DocumentProcessDTO[] getPendingDocuments();

	@Role(DMS_USER_ROLE)
	DocumentProcessDTO[] getAllDocuments();
	
	@Role(DMS_USER_ROLE)
	DocumentProcessDTO[] getArchiveDocuments();
	
	void setLogin(String login);
	
	String getLogin();

	void setUserAccountService(UserAccountService userAccountService);

	UserAccountService getUserAccountService();
	

}