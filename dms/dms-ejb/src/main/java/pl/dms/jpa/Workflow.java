package pl.dms.jpa;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

@Entity
public class Workflow {
	
	public Workflow() {
		
	}	
	
	public Workflow(long id) {
		this.id = id;
	}
	
	@Id
	@GeneratedValue
	private long id;
	
	private String name;
	
	@OneToMany(fetch=FetchType.LAZY,cascade=CascadeType.ALL)
	@JoinColumn(name="workflow")
	private List<State> states = new ArrayList<State>();
	
	@OneToMany(fetch=FetchType.LAZY,cascade=CascadeType.ALL)
	@JoinColumn(name="workflow")
	private List<DocumentProcess> documentProcesses;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public List<State> getStates() {
		return states;
	}
	
	public void setStates(List<State> states) {
		this.states = states;
	}

	/**
	 * @return the documentProcesses
	 */
	public List<DocumentProcess> getDocumentProcesses() {
		return documentProcesses;
	}

	/**
	 * @param documentProcesses the documentProcesses to set
	 */
	public void setDocumentProcesses(List<DocumentProcess> documentProcesses) {
		this.documentProcesses = documentProcesses;
	}
}
