package pl.dms.jpa.status;


public enum StateStatus {

	IDLE(1, "idle"), ACTIVE(2, "active");

	private int value;
	private String description;

	private StateStatus(int typeValue, String description) {
		this.value = typeValue;
		this.description = description;
	}

	public int getValue() {
		return value;
	}

	public String getDescription() {
		return description;
	}

	public static StateStatus of(int num) {
		
		switch (num) {
		case 1:
			return IDLE;
		case 2:
			return ACTIVE;
		default:
			return null;
		}

	}

}
