package pl.dms.jpa;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class ActionTrace {
	
	@Id
	@GeneratedValue
	private long id;
	
	private String login;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="actionTrace")
	private Action action;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="actionTraces")
	private DocumentProcess documentProcess;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	public Action getAction() {
		return action;
	}
	public void setAction(Action action) {
		this.action = action;
	}
	/**
	 * @return the login
	 */
	public String getLogin() {
		return login;
	}
	/**
	 * @param login the login to set
	 */
	public void setLogin(String login) {
		this.login = login;
	}
	/**
	 * @return the documentProcess
	 */
	public DocumentProcess getDocumentProcess() {
		return documentProcess;
	}
	/**
	 * @param documentProcess the documentProcess to set
	 */
	public void setDocumentProcess(DocumentProcess documentProcess) {
		this.documentProcess = documentProcess;
	}

}
