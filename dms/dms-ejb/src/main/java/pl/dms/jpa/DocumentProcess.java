package pl.dms.jpa;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class DocumentProcess {
	
	@Id
	@GeneratedValue
	private long id;
	
	private String name;
	
	private long documentId;
	
	private boolean pending;
	
	@ManyToOne
	@JoinColumn(name="documentProcesses")
	private Workflow workflow;
	
	@ManyToOne
	@JoinColumn(name="documentProcessesOfCurrentState")
	private State currentState;
	
	@OneToMany(fetch=FetchType.LAZY,cascade=CascadeType.ALL)
	@JoinColumn(name="documentProcess")
	private List<ActionTrace> actionTraces = new ArrayList<ActionTrace>();
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Workflow getWorkflow() {
		return workflow;
	}
	public void setWorkflow(Workflow workflow) {
		this.workflow = workflow;
	}
	public long getDocumentId() {
		return documentId;
	}
	public void setDocumentId(long documentId) {
		this.documentId = documentId;
	}
	public State getCurrentState() {
		return currentState;
	}
	public void setCurrentState(State currentState) {
		this.currentState = currentState;
	}
	public boolean isPending() {
		return pending;
	}
	public void setPending(boolean pending) {
		this.pending = pending;
	}
	public List<ActionTrace> getActionTraces() {
		return actionTraces;
	}
	public void setActionTraces(List<ActionTrace> actionTraces) {
		this.actionTraces = actionTraces;
	}

}
