package pl.dms.jpa.status;

public enum ActionType {

	REGISTER(1, "Register"), STANDARD(2, "Standard");

	public static ActionType of(int num) {
		switch (num) {
		case 1:
			return REGISTER;
		case 2:
			return STANDARD;
		default:
			return null;
		}

	}

	private int value;
	private String description;

	private ActionType(int typeValue, String description) {
		this.value = typeValue;
		this.description = description;

	}

	public int getValue() {
		return value;
	}

	public String getDescription() {
		return description;
	}

}
