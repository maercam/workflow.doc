package pl.dms.jpa;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import pl.dms.jpa.status.StateStatus;
import pl.dms.jpa.status.StateType;
import pl.dms.jpa.Action;

@Entity
public class State {

	public State(){
		
	}
	
	public State(long id){
		setId(id);
	}
	
	@Id
	@GeneratedValue
	private long id;
	
	private String name;
	
	private StateStatus status;
	
	private StateType type;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="states")
	private Workflow workflow;
	
	@OneToMany(fetch=FetchType.LAZY,cascade=CascadeType.ALL)
	@JoinColumn(name="currentState")
	private List<DocumentProcess> documentProcessesOfCurrentState;
	
	@OneToMany(fetch=FetchType.LAZY,cascade=CascadeType.ALL)
	@JoinColumn(name="fromState")
	private List<Action> actionsFrom = new ArrayList<Action>();
	
	@OneToMany(fetch=FetchType.LAZY,cascade=CascadeType.ALL)
	@JoinColumn(name="toState")
	private List<Action> actionsTo = new ArrayList<Action>();
	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public StateStatus getStatus() {
		return status;
	}
	public void setStatus(StateStatus status) {
		this.status = status;
	}
	public StateType getType() {
		return type;
	}
	public void setType(StateType type) {
		this.type = type;
	}
	public List<Action> getActionsFrom() {
		return actionsFrom;
	}
	public void setActionsFrom(List<Action> actionsFrom) {
		this.actionsFrom = actionsFrom;
	}

	public List<Action> getActionsTo() {
		return actionsTo;
	}
	
	public void setActionsTo(List<Action> actionsTo) {
		this.actionsTo = actionsTo;
	}

	public Workflow getWorkflow() {
		return workflow;
	}

	public void setWorkflow(Workflow workflow) {
		this.workflow = workflow;
	}

	/**
	 * @return the documentProcesses
	 */
	public List<DocumentProcess> getDocumentProcesses() {
		return documentProcessesOfCurrentState;
	}

	/**
	 * @param documentProcesses the documentProcesses to set
	 */
	public void setDocumentProcesses(List<DocumentProcess> documentProcesses) {
		this.documentProcessesOfCurrentState = documentProcesses;
	}
}
