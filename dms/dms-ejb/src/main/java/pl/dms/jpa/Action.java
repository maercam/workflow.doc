package pl.dms.jpa;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import pl.dms.jpa.status.ActionType;

@Entity
public class Action {
	
	@Id
	@GeneratedValue
	private long id;
	
	private String role;
	
	private String name;
	
	private ActionType type;
	
	@OneToMany(fetch=FetchType.LAZY,cascade=CascadeType.ALL)
	@JoinColumn(name="action")
	private List<ActionTrace> actionTrace = new ArrayList<ActionTrace>();
	
	@ManyToOne
	@JoinColumn(name="actionsFrom")
	private State fromState;
	
	@ManyToOne
	@JoinColumn(name="actionsTo")
	private State toState;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public State getFromState() {
		return fromState;
	}
	public void setFromState(State fromState) {
		this.fromState = fromState;
	}
	public State getToState() {
		return toState;
	}
	public void setToState(State toState) {
		this.toState = toState;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public ActionType getType() {
		return type;
	}
	public void setType(ActionType type) {
		this.type = type;
	}
	/**
	 * @return the actionTrace
	 */
	public List<ActionTrace> getActionTrace() {
		return actionTrace;
	}
	/**
	 * @param actionTrace the actionTrace to set
	 */
	public void setActionTrace(List<ActionTrace> actionTrace) {
		this.actionTrace = actionTrace;
	}
}
