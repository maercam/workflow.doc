package pl.dms.service.ws;

import static org.easymock.EasyMock.anyObject;
import static org.easymock.EasyMock.anyString;
import static org.easymock.EasyMock.expect;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.powermock.api.easymock.PowerMock.createMock;
import static org.powermock.api.easymock.PowerMock.createStrictMock;
import static org.powermock.api.easymock.PowerMock.expectLastCall;
import static org.powermock.api.easymock.PowerMock.replay;
import static org.powermock.api.easymock.PowerMock.replayAll;
import static org.powermock.api.easymock.PowerMock.verifyAll;
import static pl.dms.service.ws.model.BaseResponse.AUTHORIZATION_ERROR;
import static pl.dms.service.ws.model.BaseResponse.OK;

import java.util.ArrayList;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import pl.dms.commons.ServerConfig;
import pl.dms.commons.ServerConfigManager;
import pl.dms.data.facade.WorkflowFacade;
import pl.dms.ejb.Auth;
import pl.dms.ejb.UserAccountService;
import pl.dms.jpa.Workflow;
import pl.dms.service.ws.model.AuthenticationResponse;
import pl.dms.service.ws.model.BaseResponse;
import pl.dms.service.ws.model.CreationResponse;
import pl.dms.service.ws.model.StateDTO;
import pl.dms.service.ws.model.WorkflowDTO;
import pl.edu.agh.useraccounts.service.RoleService;
import pl.edu.agh.useraccounts.service.UserService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ UserService.class, Auth.class, WorkflowFacade.class })
@PowerMockIgnore("org.apache.logging.log4j.*")
public class DMSServiceWSL1Test extends DMSServiceTestBase {

	private static final String USER = "user";
	private static final String ADMIN = "admin";
	private static DMSServiceWS service;
	String sid;
	
	private EntityManager emMock;

	ServerConfig cfg = ServerConfigManager.getSeverConfig();
	private WorkflowFacade wfMock;
	@Before
	public void setUp() throws Exception {
		UserService userServiceMock = createMock(UserService.class);

		expect(userServiceMock.authorization(ADMIN, ADMIN)).andReturn(0).anyTimes();
		expect(userServiceMock.authorization(USER, USER)).andReturn(0).anyTimes();
		expect(userServiceMock.authorization(anyString(), anyString())).andReturn(1).anyTimes();

		RoleService roleServiceMock = createMock(RoleService.class);

		expect(roleServiceMock.getUserRole(USER)).andReturn(new String[] { USER }).anyTimes();
		expect(roleServiceMock.getUserRole(ADMIN)).andReturn(new String[] { cfg.dmsAdminRole() }).anyTimes();
		UserAccountService userAccountService = new UserAccountService();

		emMock = createMock(EntityManager.class);

		wfMock = createMock(WorkflowFacade.class);
		
		wfMock.setEntityManager(emMock);
		
		Auth authMock = initAuthMock(userAccountService, emMock, wfMock);

		service = new DMSServiceWS();

		service.userAccountService = userAccountService;
		service.userAccountService.setUserService(userServiceMock);
		service.userAccountService.setRoleService(roleServiceMock);
		service.userAccountService.setAuth(authMock);
		service.auth = authMock;

		replay(authMock, userServiceMock, roleServiceMock);
	}

	@After
	public void tearDown() {
		verifyAll();
	}

	public void loginAsAdmin() {
		AuthenticationResponse response = service.login(ADMIN, ADMIN);
		sid = response.getSid();
	}

	public void loginAsUser() {
		AuthenticationResponse response = service.login(USER, USER);
		sid = response.getSid();
	}

	@Test
	@Ignore
	public void testGetWorkflows() {
		fail("not implemented yet");

		// WorkflowDTO[] getWorkflows(String sid);
	}

	@Test
	public void testAddWorkflowForAdmin() {
		// arrange
		expect(wfMock.findAll()).andReturn(new ArrayList<Workflow>()).anyTimes();
		wfMock.create((Workflow)anyObject());
		expectLastCall().once();
		emMock.persist(anyObject());
		expectLastCall().times(4);
		//3 states
		//1 action
		replay(emMock);
		replay(wfMock);
		int expectedStatus = OK;
		WorkflowDTO workflowDTO = new WorkflowDTO();
		workflowDTO.setName("Name");

		// act
		loginAsAdmin();
		CreationResponse response = service.addWorkflow(sid, workflowDTO);
		int actualStatus = response.getStatus();

		// assert
		assertThat(actualStatus, is(expectedStatus));

	}

	@Test
	public void testAddWorkflowForDefaultUser() {
		replayAll();
		// arrange
		int expectedStatus = AUTHORIZATION_ERROR;
		String expectedDescription = "You do not have permission to call that method";
		WorkflowDTO workflowDTO = new WorkflowDTO();
		workflowDTO.setName("Name");

		// act
		loginAsUser();
		CreationResponse response = service.addWorkflow(sid, workflowDTO);
		int actualStatus = response.getStatus();
		String actualDescription = response.getDescription();

		// assert
		assertThat(actualStatus, is(expectedStatus));
		assertThat(actualDescription, is(expectedDescription));
	}

	@Test
	@Ignore
	public void testDeleteWorkflow() {
		
		// arrange
		loginAsAdmin();
		emMock.remove(anyObject());
		expectLastCall().once();
		long workflowId = 0;
		int expectedResult = OK;
		String expectedDescription = null;
		
		// act
		BaseResponse actualResponse = service.deleteWorkflow(sid, workflowId);
		int actualStatus = actualResponse.getStatus();
		String actualDescription = actualResponse.getDescription();

		// assert
		assertThat(actualStatus, is(expectedResult));
		assertThat(actualDescription, is(expectedDescription));
	}

	@Test
	@Ignore
	public void testGetStates() {
		loginAsAdmin();
		WorkflowDTO dto = new WorkflowDTO();
		dto.setName("my");
		service.addWorkflow(sid, dto);
		long id = dto.getId();
		StateDTO[] states = service.getStates(sid, id);
	
		assertThat(states.length, is(3));
	}

	@Test
	@Ignore
	public void testAddState() {
		fail("not implemented yet");
		// CreationResponse addState(String sid, StateDTO stateDTO, long
		// workflowId);
	}

	@Test
	@Ignore
	public void testDeleteState() {
		fail("not implemented yet");
		// BaseResponse deleteState(String sid, long stateId);
	}

	@Test
	@Ignore
	public void testGetActions() {
		fail("not implemented yet");
		// ActionDTO[] getActions(String sid, long stateId);
	}

	@Test
	@Ignore
	public void testAddAcction() {
		fail("not implemented yet");
		// CreationResponse addAcction(String sid, ActionDTO actionDTO, long
		// stateId);
	}

	@Test
	@Ignore
	public void testDeleteAction() {
		fail("not implemented yet");
		// BaseResponse deleteAction(String sid, long actionId);
	}

	@Test
	@Ignore
	public void testAddDocument() {
		fail("not implemented yet");
		// CreationResponse addDocument(String sid, DocumentProcessDTO
		// documentProcessDTO);
	}

	@Test
	@Ignore
	public void testExecuteAction() {
		fail("not implemented yet");
		// BaseResponse executeAction(String sid, long documentId, long
		// actionId);
	}

	@Test
	@Ignore
	public void testGetPendingDocuments() {
		fail("not implemented yet");
		// DocumentProcessDTO[] getPendingDocuments(String sid);
	}

	@Test
	@Ignore
	public void testGetAllDocuments() {
		fail("not implemented yet");
		// DocumentProcessDTO[] getAllDocuments(String sid);
	}

	@Test
	@Ignore
	public void testGetArchiveDocuments() {
		fail("not implemented yet");
		// DocumentProcessDTO[] getArchiveDocuments(String sid);
	}

}
