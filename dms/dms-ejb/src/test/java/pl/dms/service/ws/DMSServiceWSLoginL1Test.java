package pl.dms.service.ws;

import static org.easymock.EasyMock.anyString;
import static org.easymock.EasyMock.expect;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.assertThat;
import static org.powermock.api.easymock.PowerMock.createMock;
import static org.powermock.api.easymock.PowerMock.replayAll;
import static pl.dms.commons.RegexMatcher.matches;
import static pl.dms.service.ws.model.BaseResponse.AUTHORIZATION_ERROR;
import static pl.dms.service.ws.model.BaseResponse.OK;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import pl.dms.ejb.Auth;
import pl.dms.ejb.UserAccountService;
import pl.dms.service.ws.model.AuthenticationResponse;
import pl.dms.service.ws.model.BaseResponse;
import pl.edu.agh.useraccounts.service.UserService;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ UserService.class, Auth.class })
@PowerMockIgnore("org.apache.logging.log4j.*")
public class DMSServiceWSLoginL1Test extends DMSServiceTestBase {

	private DMSServiceWS service;
	private String SID_REGEX_MATCH = "[0-9a-fA-F\\-]*";
	
	@Before
	public void setUp() throws Exception {

		UserService userServiceMock = createMock(UserService.class);

		expect(userServiceMock.authorization("admin", "admin")).andReturn(0).anyTimes();
		expect(userServiceMock.authorization("user", "user")).andReturn(0).anyTimes();
		expect(userServiceMock.authorization(anyString(), anyString())).andReturn(1).anyTimes();

		UserAccountService userAccountService = new UserAccountService();
		Auth authMock = initAuthMock(userAccountService,null,null);

		replayAll();

		service = new DMSServiceWS();
		
		service.userAccountService = userAccountService;
		service.userAccountService.setUserService(userServiceMock);
		service.userAccountService.setAuth(authMock);
	}

	@Test
	public void testLoginAsAdmin() {
		// arrange
		String login = "admin";
		String password = "admin";
		int expectedStatus = OK;
		String expectedDescription = "Login ok";
		
		//act
		testLogin(login, password, expectedStatus, expectedDescription, SID_REGEX_MATCH);
	}

	@Test
	public void testLoginAsUser() {
		// arrange
		String login = "user";
		String password = "user";
		int expectedStatus = OK;
		String expectedDescription = "Login ok";
		
		//act
		testLogin(login, password, expectedStatus, expectedDescription,SID_REGEX_MATCH);
	}

	@Test
	public void testLoginAsNotRegisteredUser() {
		// arrange
		String login = "inny";
		String password = "inny";
		int expectedStatus = AUTHORIZATION_ERROR;
		String expectedDescription = "Username or password is invalid";
		
		//act
		testLogin(login, password, expectedStatus, expectedDescription, null);
	}

	private void testLogin(String login, String password, int expectedStatus, String expectedDescription, String expectedSid) {
		// act
		AuthenticationResponse actualResponse = service.login(login, password);
		int actualStatus = actualResponse.getStatus();
		String actualDescription = actualResponse.getDescription();
		String actualSid = actualResponse.getSid();

		// assert
		assertThat(actualDescription, is(expectedDescription));
		assertThat(actualStatus, is(expectedStatus));
		assertThat(actualSid, matches(expectedSid));
		assertThat(actualSid, is(not("")));
	}

	@Test
	public void testLogout() {
		// arrange
		String sid = "";
		int expectedStatus = OK;
		String expectedDescription = "Logout fine";

		// act
		BaseResponse actualResonse = service.logout(sid);
		int actualStatus = actualResonse.getStatus();
		String actualDescription = actualResonse.getDescription();

		// assert
		assertThat(actualStatus, is(expectedStatus));
		assertThat(actualDescription, is(expectedDescription));
	}

}
