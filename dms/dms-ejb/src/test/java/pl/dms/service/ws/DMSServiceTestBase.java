package pl.dms.service.ws;

import static org.easymock.EasyMock.anyObject;
import static org.easymock.EasyMock.anyString;
import static org.easymock.EasyMock.getCurrentArguments;
import static org.powermock.api.easymock.PowerMock.createMock;
import static org.powermock.api.easymock.PowerMock.expectLastCall;
import static org.powermock.api.easymock.PowerMock.expectPrivate;

import java.lang.reflect.Method;

import javax.persistence.EntityManager;

import org.easymock.ConstructorArgs;
import org.easymock.IAnswer;

import pl.dms.data.facade.ActionFacade;
import pl.dms.data.facade.StateFacade;
import pl.dms.data.facade.WorkflowFacade;
import pl.dms.ejb.Auth;
import pl.dms.ejb.UserAccessProxyHandler;
import pl.dms.ejb.UserAccountService;
import pl.dms.ejb.UserManager;
import pl.dms.ejb.UserManagerBean;

public class DMSServiceTestBase {

	private static final String CREATE_USER_MANGER_PROXY_HANDLER = "createUserMangerProxyHandler";
	protected static final String CREATE_USER_MANGER = "createUserManger";

	protected Auth initAuthMock(UserAccountService userAccountService, EntityManager entityManager, WorkflowFacade workflowFacade) throws NoSuchMethodException, Exception {
		final UserAccountService _userAccountService = userAccountService;
		final EntityManager managerMock = entityManager;		
		Auth authMock = createMock(
			Auth.class,
			new ConstructorArgs(Auth.class.getConstructor(null)),
			new Method[] {
				Auth.class.getDeclaredMethod(CREATE_USER_MANGER, new Class[] { String.class }),
				Auth.class.getDeclaredMethod(CREATE_USER_MANGER_PROXY_HANDLER, new Class[] { UserManager.class })
			}
		);

		
		final WorkflowFacade wf = workflowFacade;
		expectPrivate(authMock, CREATE_USER_MANGER, anyString()).andAnswer(new IAnswer<Object>() {
			
			WorkflowFacade workflowFacade;
			{
				workflowFacade = wf;
			}
			
			@Override
			public Object answer() throws Throwable {
				String login = (String) getCurrentArguments()[0];
				UserManagerBean userManagerBean = new UserManagerBean(login);
				userManagerBean.setUserAccountService(_userAccountService);
				if (workflowFacade != null) {
					workflowFacade.setEntityManager(managerMock);
					userManagerBean.setWorkflowFacade(workflowFacade);
				}
				StateFacade stateFacade = new StateFacade();
				stateFacade.setEntityManager(managerMock);
				userManagerBean.setStateFacade(stateFacade);
				
				ActionFacade actionFacade = new ActionFacade();
				actionFacade.setEntityManager(managerMock);
				userManagerBean.setActionFacade(actionFacade);
	
				return userManagerBean;
			}
		}).anyTimes();
		
		expectPrivate(authMock, CREATE_USER_MANGER_PROXY_HANDLER, (UserManager)anyObject()).andAnswer(new IAnswer<Object>() {
			@Override
			public Object answer() throws Throwable {
				UserManager userManager = (UserManager) getCurrentArguments()[0];
				UserAccessProxyHandler userAccessProxyHandler = new UserAccessProxyHandler(userManager);
				return userAccessProxyHandler;
			}
		}).anyTimes();
		
		return authMock;
	}

}