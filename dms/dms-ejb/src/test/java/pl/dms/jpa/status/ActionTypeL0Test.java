package pl.dms.jpa.status;

import static org.hamcrest.core.Is.*;
import static org.junit.Assert.*;

import org.junit.Test;

public class ActionTypeL0Test {

	@Test
	public void testOf(){
		for(ActionType actionType : ActionType.values()){
			testOf(actionType);
		}
	}
	
	public void testOf(ActionType actionType) {
		
		 //act
		 ActionType actualType = ActionType.of(actionType.getValue());
		 
		 //assert
		 assertThat(actualType, is(actionType));
	}

	@Test
	public void testGetValueRegister() {
		assertThat(ActionType.REGISTER.getValue(),is(1));
	}
	
	@Test
	public void testGetValueStandard() {
		assertThat(ActionType.STANDARD.getValue(),is(2));
	}

	@Test
	public void testGetDescriptionRegister() {
		assertThat(ActionType.REGISTER.getDescription(),is("Register"));
	}
	
	@Test
	public void testGetDescriptionStandard() {
		assertThat(ActionType.STANDARD.getDescription(),is("Standard"));
	}

}
