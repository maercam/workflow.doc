package pl.dms.jpa.status;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

import org.junit.Test;

public class StateStatusL0Test {

	@Test
	public void testOf(){
		for(StateStatus stateStatus : StateStatus.values()){
			testOf(stateStatus);
		}
	}
	
	public void testOf(StateStatus stateStatus) {
		
		 //act
		 StateStatus actualType = StateStatus.of(stateStatus.getValue());
		 
		 //assert
		 assertThat(actualType, is(stateStatus));
	}


}
