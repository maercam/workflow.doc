package pl.dms.jpa.status;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

import org.junit.Test;

public class StateTypeL0Test {

	@Test
	public void testOf(){
		for(StateType stateType : StateType.values()){
			testOf(stateType);
		}
	}
	
	public void testOf(StateType stateType) {
		
		 //act
		 StateType actualType = StateType.of(stateType.getValue());
		 
		 //assert
		 assertThat(actualType, is(stateType));
	}
}
