package pl.dms.commons;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;

public class RegexMatcher extends BaseMatcher<String> {
	private final String regex;

	public RegexMatcher(String regex) {
		this.regex = regex;
	}

	public boolean matches(Object o) {
		if(o == null){
			return regex == null;
		}
		return ((String) o).matches(regex);

	}

	public void describeTo(Description description) {
		description.appendText("matches regex=").appendText(regex);
	}

	public static RegexMatcher matches(String regex) {
		return new RegexMatcher(regex);
	}

}