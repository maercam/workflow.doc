package pl.dms.commons;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

import org.aeonbits.owner.ConfigFactory;


public class ServerConfigManager implements InvocationHandler{
	
	private ServerConfigManager(){};
	private static ServerConfig instance;
	
	private final static String dmsUrlMethodName = "documentManagementSystemUrl";
	private final static String uamRoleServiceUrl = "uamRoleServiceUrl";
	private final static String	uamUserServiceUrl = "uamUserServiceUrl";
	private final static String	uamUsersParamsServiceUrl = "uamUsersParamsServiceUrl";

	public static ServerConfig getSeverConfig() {
		if(instance == null){
			instance = ConfigFactory.create(ServerConfig.class);
		}
		
		return (ServerConfig)Proxy
				.newProxyInstance(
						ServerConfigManager.class.getClassLoader(),
						new Class<?>[] { ServerConfig.class },
						new ServerConfigManager());
    }

	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		
		String methodName = method.getName();
		if(methodName.endsWith("Url")){
			return handleUrl(methodName);
		}
		
		try{
			return method.invoke(instance, args);
		}catch(InvocationTargetException e){
			throw e.getTargetException();
		}
	}

	private String handleUrl(String methodName) {
		String host;
		String port;
		String path;
		if (dmsUrlMethodName.equals(methodName)) {
			host = instance.documentManagementSystemHost();
			port = instance.documentManagementSystemPort();
			path = instance.documentManagementSystemPath();
			return createUrl(host, port, path);
		} else if (uamRoleServiceUrl.equals(methodName)) {
			host = getUamHost(instance.uamRoleServiceHost());
			port = getUamPort(instance.uamRoleServicePort());
			path = instance.uamRoleServicePath();
			return createUrl(host, port, path);
		} else if (uamUserServiceUrl.equals(methodName)) {
			host = getUamHost(instance.uamUserServiceHost());
			port = getUamPort(instance.uamUserServicePort());
			path = instance.uamUserServicePath();
		} else if (uamUsersParamsServiceUrl.equals(methodName)) {
			host = getUamHost(instance.uamUsersParamsServiceHost());
			port = getUamPort(instance.uamUsersParamsServicePort());
			path = instance.uamUsersParamsServicePath();
		} else {
			throw new RuntimeException("Not implemented method wnds with url");
		}
		return createUrl(host, port, path);
	}

	private String getUamHost(String host) {
		if(host == null){
			return instance.uamHost();
		}
		return host;
	}
	
	private String getUamPort(String port) {
		if(port == null){
			return instance.uamPort();
		}
		return port;
	}

	private String createUrl( String host, String port, String path) {
		StringBuilder sb = new StringBuilder("http://");
		sb.append(host)
		.append(":")
		.append(port)
		.append(path);
		return sb.toString();
	}
	
	
}
