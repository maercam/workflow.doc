package pl.dms.commons;

import org.aeonbits.owner.Accessible;
import org.aeonbits.owner.Config;
import org.aeonbits.owner.Config.Sources;
import org.aeonbits.owner.Mutable;

   
@Sources({ "file:${DMS_PROPERTIES}", "classpath:pl/dms/commons/dms.properties" })
public interface ServerConfig extends Mutable , Config, Accessible {
    
	public static final String DMS_USER_ROLE = "dms.user.role";
	public static final String DMS_REGISTER_DOCUMENT_ROLE = "dms.register_document.role";
	public static final String DMS_ADMIN_ROLE = "dms.admin.role";
	public static final String UAM_HOST = "uam.host";
	public static final String UAM_PORT = "uam.port";
	
	public static final String UAM_USERS_PARAMS_SERVICE_URL = "uam.UsersParamsService.url";
	public static final String UAM_USERS_PARAMS_SERVICE_PATH = "uam.UsersParamsService.path";
	public static final String UAM_USERS_PARAMS_SERVICE_PORT = "uam.UsersParamsService.port";
	public static final String UAM_USERS_PARAMS_SERVICE_HOST = "uam.UsersParamsService.host";
	
	public static final String UAM_ROLE_SERVICE_URL = "uam.RoleService.url";
	public static final String UAM_ROLE_SERVICE_PATH = "uam.RoleService.path";
	public static final String UAM_ROLE_SERVICE_PORT = "uam.RoleService.port";
	public static final String UAM_ROLE_SERVICE_HOST = "uam.RoleService.host";
	
	public static final String UAM_USER_SERVICE_URL = "uam.UserService.url";
	public static final String UAM_USER_SERVICE_PATH = "uam.UserService.path";
	public static final String UAM_USER_SERVICE_PORT = "uam.UserService.port";
	public static final String UAM_USER_SERVICE_HOST = "uam.UserService.host";
	
	public static final String DMS_API_URL = "dms.api.url";
	public static final String DMS_API_HOST = "dms.api.host";
	public static final String DMS_API_PORT = "dms.api.port";
	public static final String DMS_API_PATH = "dms.api.path";
	
	public static final String JBOSS_JMX_PORT = "jboss.jmx.port";
	
	@Key(UAM_USER_SERVICE_URL)
	public String uamUserServiceUrl();

	@Key(UAM_USER_SERVICE_HOST)
	public String uamUserServiceHost();

	@Key(UAM_USER_SERVICE_PORT)
	public String uamUserServicePort();

	@Key(UAM_USER_SERVICE_PATH)
	public String uamUserServicePath();
	
	
	@Key(UAM_ROLE_SERVICE_URL)
	public String uamRoleServiceUrl();

	@Key(UAM_ROLE_SERVICE_HOST)
	public String uamRoleServiceHost();

	@Key(UAM_ROLE_SERVICE_PORT)
	public String uamRoleServicePort();

	@Key(UAM_ROLE_SERVICE_PATH)
	public String uamRoleServicePath();
	
	
	
	@Key(UAM_USERS_PARAMS_SERVICE_URL)
	public String uamUsersParamsServiceUrl();

	@Key(UAM_USERS_PARAMS_SERVICE_HOST)
	public String uamUsersParamsServiceHost();

	@Key(UAM_USERS_PARAMS_SERVICE_PORT)
	public String uamUsersParamsServicePort();

	@Key(UAM_USERS_PARAMS_SERVICE_PATH)
	public String uamUsersParamsServicePath();

	@Key(UAM_HOST)
	@DefaultValue("localhost")
	public String uamHost();
	
	@Key(UAM_PORT)
	@DefaultValue("8080")
	public String uamPort();
	
	/**
	 * dms api props
	 */
	
	@Key(DMS_API_URL)
    public String documentManagementSystemUrl();
	
	@Key(DMS_API_HOST)
	@DefaultValue("localhost")
    public String documentManagementSystemHost();
	
	@Key(DMS_API_PORT)
	@DefaultValue("8080")
    public String documentManagementSystemPort();
	
    @Key(DMS_API_PATH)
    public String documentManagementSystemPath();
    
    @Key(DMS_ADMIN_ROLE)
    @DefaultValue("DMS_ADMIN")
    public String dmsAdminRole();
    
    @Key(DMS_REGISTER_DOCUMENT_ROLE)
    @DefaultValue("DMS_REGISTER_DOCUMENT")
    public String dmsRegisteDocumentRole();
    
    @Key(DMS_USER_ROLE)
    @DefaultValue("DMS_USER")
    public String dmsUserRole();
    
    @Key(JBOSS_JMX_PORT)
    @DefaultValue("9999")
    public String jbossJmxPort();
    
    
}

