package pl.dms.commons;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertThat;

import static org.hamcrest.core.IsNot.*;
import org.junit.Before;
import org.junit.Test;


public class ServerConfigL0Test {

	
	private ServerConfig cfg;
	
	@Before
	public void setUp(){
		cfg = ServerConfigManager.getSeverConfig();
		
	}
	
	@Test
	public void testGetDocumentManagementSystemUrl(){
		//arrange
		String expectedUrl = "/dms-ejb/DMSServiceWS/DMSServiceWSPortType";
		
		//act
		String actualUrl = cfg.documentManagementSystemPath();
		
		//assert
		assertThat(actualUrl, is(expectedUrl));
	}
	
	@Test
	public void testSetDocumentManagementSystemPath(){
		//arrange
		String expectedUrl = "aaa";
		String currValue = cfg.documentManagementSystemPath();
		cfg.setProperty(ServerConfig.DMS_API_PATH, expectedUrl);
		
		//act
		String actualUrl = cfg.documentManagementSystemPath();
		
		//assert
		assertThat(actualUrl, is(expectedUrl));
		
		//after
		cfg.setProperty(ServerConfig.DMS_API_PATH, currValue);
		
	}
	
	@Test
	public void testSetDocumentManagementSystemPathFormOtherInstance(){
		//arrange
		String expectedUrl = "aaa";
		String currValue = cfg.documentManagementSystemPath();
		cfg.setProperty(ServerConfig.DMS_API_PATH, expectedUrl);

		ServerConfig cfg2 =  ServerConfigManager.getSeverConfig();
		//act
		String actualUrl = cfg2.documentManagementSystemPath();
		
		//assert
		assertThat(actualUrl, is(expectedUrl));
		
		//after
		cfg.setProperty(ServerConfig.DMS_API_PATH, currValue);
	}
	
	@Test
	public void testUamUserServiceUrl(){
		if(System.getenv("DMS_PROPERTIES") == null){
			//arrange
			String expectedUrl = "http://dev-wfdoc.rhcloud.com:80/uam-mock/services/UserServiceImplPort";
			
			//act
			String actualUrl = cfg.uamUserServiceUrl();
			
			//assert
			assertThat(actualUrl, is(expectedUrl));
		}
	}
	
	@Test
	public void testChangeDmsHost(){
		//arrange
		String expectedUrlAfter = "http://otherHost:80/dms-ejb/DMSServiceWS/DMSServiceWSPortType";
		
		//act
		assertThat(cfg.documentManagementSystemUrl(), is(not(expectedUrlAfter)));
		cfg.setProperty(ServerConfig.DMS_API_HOST, "otherHost");
		cfg.setProperty(ServerConfig.DMS_API_PORT, "80");
		String actualUrl = cfg.documentManagementSystemUrl();
		
		//assert
		assertThat(actualUrl, is(expectedUrlAfter));
		
		//tearDown
		cfg.setProperty(ServerConfig.DMS_API_HOST, "localhost");
	}
	
	@Test
	public void testNotSetAnnotationKey(){
		String jbossJmxPort = cfg.jbossJmxPort();
		assertThat(jbossJmxPort, is(notNullValue()));
	}
}
