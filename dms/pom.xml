<?xml version="1.0" encoding="UTF-8"?>
<!-- JBoss, Home of Professional Open Source Copyright 2013, Red Hat, Inc. 
	and/or its affiliates, and individual contributors by the @authors tag. See 
	the copyright.txt in the distribution for a full listing of individual contributors. 
	Licensed under the Apache License, Version 2.0 (the "License"); you may not 
	use this file except in compliance with the License. You may obtain a copy 
	of the License at http://www.apache.org/licenses/LICENSE-2.0 Unless required 
	by applicable law or agreed to in writing, software distributed under the 
	License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS 
	OF ANY KIND, either express or implied. See the License for the specific 
	language governing permissions and limitations under the License. -->
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
	<name>dms</name>
	<modelVersion>4.0.0</modelVersion>
	<groupId>pl.dms</groupId>
	<artifactId>dms</artifactId>
	<version>1.0-SNAPSHOT</version>
	<packaging>pom</packaging>

	<url>http://jboss.org/jbossas</url>
	<licenses>
		<license>
			<name>Apache License, Version 2.0</name>
			<distribution>repo</distribution>
			<url>http://www.apache.org/licenses/LICENSE-2.0.html</url>
		</license>
	</licenses>
	<modules>
		<module>dms-ejb</module>
		<module>dms-web</module>
		<module>dms-ear</module>
		<module>uam</module>
		<module>dms-commons</module>
		<module>dms-api</module>
		<module>dms-ejb-config</module>
	</modules>

	<properties>
		<!-- Explicitly declaring the source encoding eliminates the following 
			message: -->
		<!-- [WARNING] Using platform encoding (UTF-8 actually) to copy filtered 
			resources, i.e. build is platform dependent! -->
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
		<!-- JBoss dependency versions -->

		<version.jboss.maven.plugin>7.4.Final</version.jboss.maven.plugin>

		<!-- Define the version of the JBoss BOMs we want to import to specify 
			tested stacks. -->
		<!-- Certified version of the JBoss EAP components we want to use -->
		<!-- <version.jboss.bom>1.0.4.Final-redhat-4</version.jboss.bom> -->
		<!-- Alternatively, comment out the above line, and un-comment the line 
			below to use version 1.0.7.Final which is based on community built dependencies. -->
		<version.jboss.bom>1.0.7.Final</version.jboss.bom>

		<version.jboss.as>7.2.0.Final</version.jboss.as>
		<!-- Alternatively, comment out the above line, and un-comment the line 
			below to use version 7.2.0.Final-redhat-8 which is a release certified to 
			work with JBoss EAP 6. It requires you have access to the JBoss EAP 6 maven 
			repository. -->
		<!-- <version.jboss.as>7.2.0.Final-redhat-8</version.jboss.as> -->

		<!-- other plugin versions -->
		<version.ear.plugin>2.6</version.ear.plugin>
		<version.ejb.plugin>2.3</version.ejb.plugin>
		<version.surefire.plugin>2.11</version.surefire.plugin>
		<version.war.plugin>2.1.1</version.war.plugin>

		<!-- maven-compiler-plugin -->
		<maven.compiler.target>1.6</maven.compiler.target>
		<maven.compiler.source>1.6</maven.compiler.source>

		<!-- dependency versions -->
		<version.junit>4.11</version.junit>
		<version.log4j>2.0-beta9</version.log4j>
		<version.powermock>1.5.2</version.powermock>
		<version.easymock>3.2</version.easymock>
	</properties>

	<repositories>
		<repository>
			<id>prime-repo</id>
			<name>PrimeFaces Maven Repository</name>
			<url>http://repository.primefaces.org</url>
			<layout>default</layout>
		</repository>
		<repository>
			<id>jboss-public-repository-group</id>
			<name>JBoss Public Repository Group</name>
			<url>http://repository.jboss.org/nexus/content/groups/public/</url>
			<layout>default</layout>
			<releases>
				<enabled>true</enabled>
				<updatePolicy>never</updatePolicy>
			</releases>
			<snapshots>
				<enabled>true</enabled>
				<updatePolicy>never</updatePolicy>
			</snapshots>
		</repository>
	</repositories>
	<pluginRepositories>
		<pluginRepository>
			<id>eviwarePluginRepository</id>
			<url>http://www.soapui.org/repository/maven2/</url>
		</pluginRepository>
	</pluginRepositories>

	<dependencyManagement>
		<dependencies>

			<!-- Define the version of the EJB jar so that we don't need to repeat 
				ourselves in every module -->
			<dependency>
				<groupId>pl.dms</groupId>
				<artifactId>dms-ejb</artifactId>
				<version>${project.version}</version>
				<type>ejb</type>
			</dependency>

			<dependency>
				<groupId>pl.dms</groupId>
				<artifactId>dms-ejb-config</artifactId>
				<version>${project.version}</version>
				<type>ejb</type>
			</dependency>

			<!-- Define the version of the WAR so that we don't need to repeat ourselves 
				in every module -->
			<dependency>
				<groupId>pl.dms</groupId>
				<artifactId>dms-web</artifactId>
				<version>${project.version}</version>
				<type>war</type>
				<scope>compile</scope>
			</dependency>

			<dependency>
				<groupId>pl.dms</groupId>
				<artifactId>dms-commons</artifactId>
				<version>${project.version}</version>
				<type>jar</type>
			</dependency>

			<dependency>
				<groupId>pl.dms</groupId>
				<artifactId>dms-api</artifactId>
				<version>${project.version}</version>
				<type>jar</type>
			</dependency>

			<dependency>
				<groupId>pl.dms</groupId>
				<artifactId>uam-api</artifactId>
				<version>${project.version}</version>
			</dependency>

			<dependency>
				<groupId>axis</groupId>
				<artifactId>axis</artifactId>
				<version>1.4</version>
			</dependency>

			<!-- JBoss distributes a complete set of Java EE 6 APIs including a Bill 
				of Materials (BOM). A BOM specifies the versions of a "stack" (or a collection) 
				of artifacts. We use this here so that we always get the correct versions 
				of artifacts. Here we use the jboss-javaee-6.0-with-tools stack (you can 
				read this as the JBoss stack of the Java EE 6 APIs, with some extras tools 
				for your project, such as Arquillian for testing) and the jboss-javaee-6.0-with-hibernate 
				stack you can read this as the JBoss stack of the Java EE 6 APIs, with extras 
				from the Hibernate family of projects) -->
			<dependency>
				<groupId>org.jboss.bom</groupId>
				<artifactId>jboss-javaee-6.0-with-tools</artifactId>
				<version>${version.jboss.bom}</version>
				<type>pom</type>
				<scope>import</scope>
			</dependency>

			<dependency>
				<groupId>org.jboss.bom</groupId>
				<artifactId>jboss-javaee-6.0-with-hibernate</artifactId>
				<version>${version.jboss.bom}</version>
				<type>pom</type>
				<scope>import</scope>
			</dependency>

			<dependency>
				<groupId>junit</groupId>
				<artifactId>junit</artifactId>
				<version>${version.junit}</version>
				<scope>test</scope>
			</dependency>
			<dependency>
				<groupId>org.hamcrest</groupId>
				<artifactId>hamcrest-core</artifactId>
				<version>1.3</version>
			</dependency>

			<dependency>
				<groupId>org.apache.logging.log4j</groupId>
				<artifactId>log4j-api</artifactId>
				<version>${version.log4j}</version>
			</dependency>

			<dependency>
				<groupId>org.apache.logging.log4j</groupId>
				<artifactId>log4j-core</artifactId>
				<version>${version.log4j}</version>
			</dependency>

			<dependency>
				<groupId>org.powermock</groupId>
				<artifactId>powermock-core</artifactId>
				<version>${version.powermock}</version>
			</dependency>

			<dependency>
				<groupId>org.powermock</groupId>
				<artifactId>powermock-api-easymock</artifactId>
				<version>${version.powermock}</version>
			</dependency>

			<dependency>
				<groupId>org.powermock</groupId>
				<artifactId>powermock-module-junit4</artifactId>
				<version>${version.powermock}</version>
			</dependency>



			<dependency>
				<groupId>org.easymock</groupId>
				<artifactId>easymock</artifactId>
				<version>${version.easymock}</version>
			</dependency>

			<dependency>
				<groupId>org.jboss.as</groupId>
				<artifactId>jboss-as-jmx</artifactId>
				<version>7.1.1.Final</version>
			</dependency>
		</dependencies>
	</dependencyManagement>
	<dependencies>
		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
		</dependency>

	</dependencies>
	<build>
		<pluginManagement>
			<plugins>
				<!-- The JBoss AS plugin deploys your ear to a local JBoss AS container -->
				<!-- Due to Maven's lack of intelligence with EARs we need to configure 
					the jboss-as maven plugin to skip deployment for all modules. We then enable 
					it specifically in the ear module. -->
				<plugin>
					<groupId>org.jboss.as.plugins</groupId>
					<artifactId>jboss-as-maven-plugin</artifactId>
					<version>${version.jboss.maven.plugin}</version>
					<inherited>true</inherited>
					<configuration>
						<skip>true</skip>
					</configuration>
				</plugin>
				<plugin>
					<groupId>org.codehaus.mojo</groupId>
					<artifactId>cobertura-maven-plugin</artifactId>
					<version>2.6</version>
					<configuration>
						<check>
							<totalLineRate>60</totalLineRate>
						</check>
						<formats>
							<format>xml</format>
							<format>html</format>
						</formats>
						<instrumentation>
							<ignoreTrivial>true</ignoreTrivial>
							<ignores>
								<ignore>org.apache.logging.log4j.*</ignore>
								<ignore>java.util.logging.*</ignore>
								<ignore>pl.edu.agh.useraccounts.service.*</ignore>
							</ignores>
						</instrumentation>

					</configuration>
					<executions>
						<execution>
							<id>cobertura-check</id>
							<phase>test</phase>
							<goals>
								<goal>cobertura</goal>
							</goals>
						</execution>
					</executions>
				</plugin>
				<plugin>
					<artifactId>maven-surefire-plugin</artifactId>
					<version>2.11</version>
					<executions>
						<execution>
							<id>default-test</id>
							<phase>test</phase>
							<goals>
								<goal>test</goal>
							</goals>
							<configuration>
								<excludes>
									<exclude>**/*L2Test.java</exclude>
								</excludes>
							</configuration>
						</execution>
					</executions>
					<dependencies>
						<dependency>
							<groupId>org.apache.maven.surefire</groupId>
							<artifactId>surefire-junit47</artifactId>
							<version>2.12</version>
						</dependency>
					</dependencies>
				</plugin>
			</plugins>
		</pluginManagement>
	</build>
	<profiles>
		<profile>
			<id>integration-test</id>
			<build>
				<plugins>
					<plugin>
						<groupId>org.codehaus.cargo</groupId>
						<artifactId>cargo-maven2-plugin</artifactId>
						<version>1.4.5</version>
						<configuration>
							<!-- Container configuration -->
							<container>
								<containerId>jboss71x</containerId>
								<home>D:\dev\servers\jboss-as-7.1.1.Final</home>
							</container>
						</configuration>
						<!-- <executions> <execution> <id>start-server</id> <phase>pre-integration-test</phase> 
							<goals> <goal>start</goal> </goals> </execution> <execution> <id>stop-server</id> 
							<phase>post-integration-test</phase> <goals> <goal>stop</goal> </goals> </execution> 
							</executions> -->
					</plugin>
					<plugin>
						<artifactId>maven-surefire-plugin</artifactId>
						<version>2.10</version>
						<executions>
							<execution>
								<id>default-test</id>
								<phase>test</phase>
								<goals>
									<goal>test</goal>
								</goals>
								<configuration>
									<skipTests>true</skipTests>
								</configuration>
							</execution>
							<execution>
								<id>integration-tests</id>
								<phase>integration-test</phase>
								<goals>
									<goal>test</goal>
								</goals>
								<configuration>
									<includes>
										<include>**/*L2Test.java</include>
									</includes>
								</configuration>
							</execution>
						</executions>
						<dependencies>
							<dependency>
								<groupId>org.apache.maven.surefire</groupId>
								<artifactId>surefire-junit47</artifactId>
								<version>2.12</version>
							</dependency>
						</dependencies>
					</plugin>
				</plugins>
			</build>
		</profile>
	</profiles>
</project>
