/**
 * DMSServiceWS.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package pl.dms.service.ws;

public interface DMSServiceWS extends javax.xml.rpc.Service {
    public java.lang.String getDMSServiceWSPortAddress();

    public pl.dms.service.ws.DMSServiceWSPortType getDMSServiceWSPort() throws javax.xml.rpc.ServiceException;

    public pl.dms.service.ws.DMSServiceWSPortType getDMSServiceWSPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
