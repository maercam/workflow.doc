/**
 * Action.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package pl.dms.service.ws.types;

public class Action  implements java.io.Serializable {
    private long id;

    private long fromStateId;

    private long toStateId;

    private java.lang.String role;

    private java.lang.String name;

    private int type;

    public Action() {
    }
/**
 * @deprecated
 * @param id
 * @param fromStateId
 * @param toStateId
 * @param role
 * @param name
 * @param type
 */
    public Action(
           long id,
           long fromStateId,
           long toStateId,
           java.lang.String role,
           java.lang.String name,
           int type) {
           this.id = id;
           this.fromStateId = fromStateId;
           this.toStateId = toStateId;
           this.role = role;
           this.name = name;
           this.type = type;
    }


    /**
     * Gets the id value for this Action.
     * 
     * @return id
     */
    public long getId() {
        return id;
    }


    /**
     * Sets the id value for this Action.
     * @deprecated
     * @param id
     */
    public void setId(long id) {
        this.id = id;
    }


    /**
     * Gets the fromStateId value for this Action.
     * 
     * @return fromStateId
     */
    public long getFromStateId() {
        return fromStateId;
    }


    /**
     * Sets the fromStateId value for this Action.
     * 
     * @param fromStateId
     */
    public void setFromStateId(long fromStateId) {
        this.fromStateId = fromStateId;
    }


    /**
     * Gets the toStateId value for this Action.
     * 
     * @return toStateId
     */
    public long getToStateId() {
        return toStateId;
    }


    /**
     * Sets the toStateId value for this Action.
     * 
     * @param toStateId
     */
    public void setToStateId(long toStateId) {
        this.toStateId = toStateId;
    }


    /**
     * Gets the role value for this Action.
     * 
     * @return role
     */
    public java.lang.String getRole() {
        return role;
    }


    /**
     * Sets the role value for this Action.
     * 
     * @param role
     */
    public void setRole(java.lang.String role) {
        this.role = role;
    }


    /**
     * Gets the name value for this Action.
     * 
     * @return name
     */
    public java.lang.String getName() {
        return name;
    }


    /**
     * Sets the name value for this Action.
     * 
     * @param name
     */
    public void setName(java.lang.String name) {
        this.name = name;
    }


    /**
     * Gets the type value for this Action.
     * 
     * @return <b>type</b>
     * <p>
     * Possible values:
     * <ul>
     * <li>REGISTER = 1</li>
     * <li>STANDARD = 2</li>
     * </ul>
     * </p>
     */
    public int getType() {
        return type;
    }


    /**
     * Sets the type value for this Action.
     * 
     * @param type
     */
    public void setType(int type) {
        this.type = type;
    }

    private java.lang.Object __equalsCalc = null;
    /**
     * @deprecated
     */
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Action)) return false;
        Action other = (Action) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.id == other.getId() &&
            this.fromStateId == other.getFromStateId() &&
            this.toStateId == other.getToStateId() &&
            ((this.role==null && other.getRole()==null) || 
             (this.role!=null &&
              this.role.equals(other.getRole()))) &&
            ((this.name==null && other.getName()==null) || 
             (this.name!=null &&
              this.name.equals(other.getName()))) &&
            this.type == other.getType();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    /**
     * @deprecated
     */
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += new Long(getId()).hashCode();
        _hashCode += new Long(getFromStateId()).hashCode();
        _hashCode += new Long(getToStateId()).hashCode();
        if (getRole() != null) {
            _hashCode += getRole().hashCode();
        }
        if (getName() != null) {
            _hashCode += getName().hashCode();
        }
        _hashCode += getType();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Action.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://service.dms.pl/ws/types/", "Action"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fromStateId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fromStateId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("toStateId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "toStateId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("role");
        elemField.setXmlName(new javax.xml.namespace.QName("", "role"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("name");
        elemField.setXmlName(new javax.xml.namespace.QName("", "name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("type");
        elemField.setXmlName(new javax.xml.namespace.QName("", "type"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * @deprecated
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * @deprecated
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * @deprecated
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
