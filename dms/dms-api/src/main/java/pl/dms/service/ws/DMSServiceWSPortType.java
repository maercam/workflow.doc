/**
 * DMSServiceWSPortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package pl.dms.service.ws;

import java.rmi.Remote;
import java.rmi.RemoteException;

import pl.dms.service.ws.types.Action;
import pl.dms.service.ws.types.AuthenticationResponse;
import pl.dms.service.ws.types.BaseResponse;
import pl.dms.service.ws.types.CreationResponse;
import pl.dms.service.ws.types.DocumentProcess;
import pl.dms.service.ws.types.State;
import pl.dms.service.ws.types.Workflow;

public interface DMSServiceWSPortType extends Remote {
	/**
	 * @param sid session ID
	 * @return
	 * @throws RemoteException
	 */
	public DocumentProcess[] getArchiveDocuments(String sid) throws RemoteException;

	/**
	 * Allow only for user with role DMS_REGISTER. 
	 * After call that method document will be created and go from state UREGISTERED to state START
	 * @param sid session ID
	 * @param documentProcess
	 * @return CreationResponse
	 * @throws RemoteException
	 */
	public CreationResponse addDocument(String sid, DocumentProcess documentProcess) throws RemoteException;

	/**
	 * Allow only for user with role DMS_ADMIN.
	 * @param sid session ID
	 * @param workflow
	 * @return CreationResponse
	 * @throws RemoteException
	 */
	public CreationResponse addWorkflow(String sid, Workflow workflow) throws RemoteException;

	/**
	 * Allow only for user with role DMS_ADMIN.
	 * @param sid session ID
	 * @param actionId
	 * @return BaseResponse
	 * @throws RemoteException
	 */
	public BaseResponse deleteAction(String sid, long actionId) throws RemoteException;

	/**
	 * @param sid session ID
	 * @return
	 * @throws RemoteException
	 */
	public Workflow[] getWorkflows(String sid) throws RemoteException;

	/**
	 * Allow only for user with role DMS_ADMIN.
	 * @param sid session ID
	 * @param stateId
	 * @return
	 * @throws RemoteException
	 */
	public BaseResponse deleteState(String sid, long stateId) throws RemoteException;

	/**
	 * @param sid session ID
	 * @param workflowId
	 * @return <b>State[]</b> - array of states for workflow with workflowId
	 * @throws RemoteException
	 */
	public State[] getStates(String sid, long workflowId) throws RemoteException;

	/**
	 * @param sid session ID
	 * @return BaseResponse
	 * @throws RemoteException
	 */
	public BaseResponse logout(String sid) throws RemoteException;

	/**
	 * @param login
	 * @param password
	 * @return
	 * @throws RemoteException
	 */
	public AuthenticationResponse login(String login, String password) throws RemoteException;

	/**
	 * @param sid session ID
	 * @return DocumentProcess[] - array of pending DocumentProcess
	 * @throws RemoteException
	 */
	public DocumentProcess[] getPendingDocuments(String sid) throws RemoteException;

	/**
	 * @param sid session ID
	 * @param documentId document ID
	 * @param actionId action ID
	 * @return baseResponse
	 * @throws RemoteException
	 */
	public BaseResponse executeAction(String sid, long documentId, long actionId) throws RemoteException;

	/**
	 * @param sid session ID
	 * @return lis
	 * @throws RemoteException
	 */
	public DocumentProcess[] getAllDocuments(String sid) throws RemoteException;

	/**
	 * @deprecated
	 * @param sid session ID
	 * @param workflowId
	 * @param completed
	 * @return
	 * @throws RemoteException
	 */
	public BaseResponse setWorkflowCompleted(String sid, long workflowId, boolean completed) throws RemoteException;

	/**
	 * @param sid session ID
	 * @param stateId
	 * @return
	 * @throws RemoteException
	 */
	public Action[] getActions(String sid, long stateId) throws RemoteException;

	/**
	 * Allow only for user with role DMS_ADMIN.
	 * @param sid session ID
	 * @param state
	 * @param workflowId
	 * @return
	 * @throws RemoteException
	 */
	public CreationResponse addState(String sid, State state, long workflowId) throws RemoteException;

	/**
	 * Allow only for user with role DMS_ADMIN.
	 * @param sid session ID
	 * @param action
	 * @param stateId
	 * @return
	 * @throws RemoteException
	 */
	public CreationResponse addAcction(String sid, Action action, long stateId) throws RemoteException;

	/**
	 * Allow only for user with role DMS_ADMIN.
	 * @param sid session ID
	 * @param workflowId
	 * @return
	 * @throws RemoteException
	 */
	public BaseResponse deleteWorkflow(String sid, long workflowId) throws RemoteException;
}
