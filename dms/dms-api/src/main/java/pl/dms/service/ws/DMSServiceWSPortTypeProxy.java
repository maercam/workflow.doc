package pl.dms.service.ws;

public class DMSServiceWSPortTypeProxy implements pl.dms.service.ws.DMSServiceWSPortType {
  private String _endpoint = null;
  private pl.dms.service.ws.DMSServiceWSPortType dMSServiceWSPortType = null;
  
  public DMSServiceWSPortTypeProxy() {
    _initDMSServiceWSPortTypeProxy();
  }
  
  public DMSServiceWSPortTypeProxy(String endpoint) {
    _endpoint = endpoint;
    _initDMSServiceWSPortTypeProxy();
  }
  
  private void _initDMSServiceWSPortTypeProxy() {
    try {
      dMSServiceWSPortType = (new pl.dms.service.ws.DMSServiceWSLocator()).getDMSServiceWSPort();
      if (dMSServiceWSPortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)dMSServiceWSPortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)dMSServiceWSPortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (dMSServiceWSPortType != null)
      ((javax.xml.rpc.Stub)dMSServiceWSPortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public pl.dms.service.ws.DMSServiceWSPortType getDMSServiceWSPortType() {
    if (dMSServiceWSPortType == null)
      _initDMSServiceWSPortTypeProxy();
    return dMSServiceWSPortType;
  }
  
  public pl.dms.service.ws.types.DocumentProcess[] getArchiveDocuments(java.lang.String sid) throws java.rmi.RemoteException{
    if (dMSServiceWSPortType == null)
      _initDMSServiceWSPortTypeProxy();
    return dMSServiceWSPortType.getArchiveDocuments(sid);
  }
  
  public pl.dms.service.ws.types.CreationResponse addDocument(java.lang.String sid, pl.dms.service.ws.types.DocumentProcess documentProcess) throws java.rmi.RemoteException{
    if (dMSServiceWSPortType == null)
      _initDMSServiceWSPortTypeProxy();
    return dMSServiceWSPortType.addDocument(sid, documentProcess);
  }
  
  public pl.dms.service.ws.types.CreationResponse addWorkflow(java.lang.String sid, pl.dms.service.ws.types.Workflow workflow) throws java.rmi.RemoteException{
    if (dMSServiceWSPortType == null)
      _initDMSServiceWSPortTypeProxy();
    return dMSServiceWSPortType.addWorkflow(sid, workflow);
  }
  
  public pl.dms.service.ws.types.BaseResponse deleteAction(java.lang.String sid, long actionId) throws java.rmi.RemoteException{
    if (dMSServiceWSPortType == null)
      _initDMSServiceWSPortTypeProxy();
    return dMSServiceWSPortType.deleteAction(sid, actionId);
  }
  
  public pl.dms.service.ws.types.Workflow[] getWorkflows(java.lang.String sid) throws java.rmi.RemoteException{
    if (dMSServiceWSPortType == null)
      _initDMSServiceWSPortTypeProxy();
    return dMSServiceWSPortType.getWorkflows(sid);
  }
  
  public pl.dms.service.ws.types.BaseResponse deleteState(java.lang.String sid, long stateId) throws java.rmi.RemoteException{
    if (dMSServiceWSPortType == null)
      _initDMSServiceWSPortTypeProxy();
    return dMSServiceWSPortType.deleteState(sid, stateId);
  }
  
  public pl.dms.service.ws.types.State[] getStates(java.lang.String sid, long workflowId) throws java.rmi.RemoteException{
    if (dMSServiceWSPortType == null)
      _initDMSServiceWSPortTypeProxy();
    return dMSServiceWSPortType.getStates(sid, workflowId);
  }
  
  public pl.dms.service.ws.types.BaseResponse logout(java.lang.String sid) throws java.rmi.RemoteException{
    if (dMSServiceWSPortType == null)
      _initDMSServiceWSPortTypeProxy();
    return dMSServiceWSPortType.logout(sid);
  }
  
  public pl.dms.service.ws.types.AuthenticationResponse login(java.lang.String login, java.lang.String password) throws java.rmi.RemoteException{
    if (dMSServiceWSPortType == null)
      _initDMSServiceWSPortTypeProxy();
    return dMSServiceWSPortType.login(login, password);
  }
  
  public pl.dms.service.ws.types.DocumentProcess[] getPendingDocuments(java.lang.String sid) throws java.rmi.RemoteException{
    if (dMSServiceWSPortType == null)
      _initDMSServiceWSPortTypeProxy();
    return dMSServiceWSPortType.getPendingDocuments(sid);
  }
  
  public pl.dms.service.ws.types.BaseResponse executeAction(java.lang.String sid, long documentId, long actionId) throws java.rmi.RemoteException{
    if (dMSServiceWSPortType == null)
      _initDMSServiceWSPortTypeProxy();
    return dMSServiceWSPortType.executeAction(sid, documentId, actionId);
  }
  
  public pl.dms.service.ws.types.DocumentProcess[] getAllDocuments(java.lang.String sid) throws java.rmi.RemoteException{
    if (dMSServiceWSPortType == null)
      _initDMSServiceWSPortTypeProxy();
    return dMSServiceWSPortType.getAllDocuments(sid);
  }
  
  public pl.dms.service.ws.types.BaseResponse setWorkflowCompleted(java.lang.String sid, long workflowId, boolean completed) throws java.rmi.RemoteException{
    if (dMSServiceWSPortType == null)
      _initDMSServiceWSPortTypeProxy();
    return dMSServiceWSPortType.setWorkflowCompleted(sid, workflowId, completed);
  }
  
  public pl.dms.service.ws.types.Action[] getActions(java.lang.String sid, long stateId) throws java.rmi.RemoteException{
    if (dMSServiceWSPortType == null)
      _initDMSServiceWSPortTypeProxy();
    return dMSServiceWSPortType.getActions(sid, stateId);
  }
  
  public pl.dms.service.ws.types.CreationResponse addState(java.lang.String sid, pl.dms.service.ws.types.State state, long workflowId) throws java.rmi.RemoteException{
    if (dMSServiceWSPortType == null)
      _initDMSServiceWSPortTypeProxy();
    return dMSServiceWSPortType.addState(sid, state, workflowId);
  }
  
  public pl.dms.service.ws.types.CreationResponse addAcction(java.lang.String sid, pl.dms.service.ws.types.Action action, long stateId) throws java.rmi.RemoteException{
    if (dMSServiceWSPortType == null)
      _initDMSServiceWSPortTypeProxy();
    return dMSServiceWSPortType.addAcction(sid, action, stateId);
  }
  
  public pl.dms.service.ws.types.BaseResponse deleteWorkflow(java.lang.String sid, long workflowId) throws java.rmi.RemoteException{
    if (dMSServiceWSPortType == null)
      _initDMSServiceWSPortTypeProxy();
    return dMSServiceWSPortType.deleteWorkflow(sid, workflowId);
  }
  
  
}