/**
 * DocumentProcess.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package pl.dms.service.ws.types;

public class DocumentProcess  implements java.io.Serializable {
    private long id;

    private java.lang.String name;

    private long workflowId;

    private long documentId;

    private long currentStateId;

    private boolean pending;

    private pl.dms.service.ws.types.ActionTrace[] actionTraces;

    public DocumentProcess() {
    }
/**
 * @deprecated
 * @param id
 * @param name
 * @param workflowId
 * @param documentId
 * @param currentStateId
 * @param pending
 * @param actionTraces
 */
    public DocumentProcess(
           long id,
           java.lang.String name,
           long workflowId,
           long documentId,
           long currentStateId,
           boolean pending,
           pl.dms.service.ws.types.ActionTrace[] actionTraces) {
           this.id = id;
           this.name = name;
           this.workflowId = workflowId;
           this.documentId = documentId;
           this.currentStateId = currentStateId;
           this.pending = pending;
           this.actionTraces = actionTraces;
    }


    /**
     * Gets the id value for this DocumentProcess.
     * 
     * @return id
     */
    public long getId() {
        return id;
    }


    /**
     * Sets the id value for this DocumentProcess.
     * @deprecated
     * @param id
     */
    public void setId(long id) {
        this.id = id;
    }


    /**
     * Gets the name value for this DocumentProcess.
     * 
     * @return name
     */
    public java.lang.String getName() {
        return name;
    }


    /**
     * Sets the name value for this DocumentProcess.
     * 
     * @param name
     */
    public void setName(java.lang.String name) {
        this.name = name;
    }


    /**
     * Gets the workflowId value for this DocumentProcess.
     * 
     * @return workflowId
     */
    public long getWorkflowId() {
        return workflowId;
    }


    /**
     * Sets the workflowId value for this DocumentProcess.
     * 
     * @param workflowId
     */
    public void setWorkflowId(long workflowId) {
        this.workflowId = workflowId;
    }


    /**
     * Gets the documentId value for this DocumentProcess.
     * 
     * @return documentId
     */
    public long getDocumentId() {
        return documentId;
    }


    /**
     * Sets the documentId value for this DocumentProcess.
     * 
     * @param documentId
     */
    public void setDocumentId(long documentId) {
        this.documentId = documentId;
    }


    /**
     * Gets the currentStateId value for this DocumentProcess.
     * 
     * @return currentStateId
     */
    public long getCurrentStateId() {
        return currentStateId;
    }


    /**
     * Sets the currentStateId value for this DocumentProcess.
     * 
     * @param currentStateId
     */
    public void setCurrentStateId(long currentStateId) {
        this.currentStateId = currentStateId;
    }


    /**
     * Gets the pending value for this DocumentProcess.
     * 
     * @return pending
     */
    public boolean isPending() {
        return pending;
    }


    /**
     * Sets the pending value for this DocumentProcess.
     * 
     * @param pending
     */
    public void setPending(boolean pending) {
        this.pending = pending;
    }


    /**
     * Gets the actionTraces value for this DocumentProcess.
     * 
     * @return actionTraces
     */
    public pl.dms.service.ws.types.ActionTrace[] getActionTraces() {
        return actionTraces;
    }


    /**
     * Sets the actionTraces value for this DocumentProcess.
     * 
     * @param actionTraces
     */
    public void setActionTraces(pl.dms.service.ws.types.ActionTrace[] actionTraces) {
        this.actionTraces = actionTraces;
    }

    private java.lang.Object __equalsCalc = null;
    /**
     * @deprecated
     */
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DocumentProcess)) return false;
        DocumentProcess other = (DocumentProcess) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.id == other.getId() &&
            ((this.name==null && other.getName()==null) || 
             (this.name!=null &&
              this.name.equals(other.getName()))) &&
            this.workflowId == other.getWorkflowId() &&
            this.documentId == other.getDocumentId() &&
            this.currentStateId == other.getCurrentStateId() &&
            this.pending == other.isPending() &&
            ((this.actionTraces==null && other.getActionTraces()==null) || 
             (this.actionTraces!=null &&
              java.util.Arrays.equals(this.actionTraces, other.getActionTraces())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    /**
     * @deprecated
     */
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += new Long(getId()).hashCode();
        if (getName() != null) {
            _hashCode += getName().hashCode();
        }
        _hashCode += new Long(getWorkflowId()).hashCode();
        _hashCode += new Long(getDocumentId()).hashCode();
        _hashCode += new Long(getCurrentStateId()).hashCode();
        _hashCode += (isPending() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        if (getActionTraces() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getActionTraces());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getActionTraces(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DocumentProcess.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://service.dms.pl/ws/types/", "DocumentProcess"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("name");
        elemField.setXmlName(new javax.xml.namespace.QName("", "name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("workflowId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "workflowId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("documentId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "documentId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("currentStateId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "currentStateId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pending");
        elemField.setXmlName(new javax.xml.namespace.QName("", "pending"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("actionTraces");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ActionTraces"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://service.dms.pl/ws/types/", "ActionTrace"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("", "actionTraces"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * @deprecated
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * @deprecated
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }
    
    /**
     * @deprecated
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
