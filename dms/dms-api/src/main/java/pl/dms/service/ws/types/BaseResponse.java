/**
 * BaseResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package pl.dms.service.ws.types;

public class BaseResponse  implements java.io.Serializable {
	
	public final static int OK = 0;
	public final static int FATAL = 1;
	public final static int DB_ERROR = 2;
	public final static int AUTHORIZATION_ERROR = 3;
	public final static int SERVER_ERROR = 4;
	public final static int ALREADY_EXISTS = 5;
	public final static int NOT_FOUND = 6;
	public final static int NOT_ALLOWED_OPERATION = 7;
	public final static int VALIDATION_ERROR = 8;
	
    private int status;

    private java.lang.String description;
/**
 * @deprecated
 */
    public BaseResponse() {
    }
/**
 * @deprecated
 * @param status
 * @param description
 */
    public BaseResponse(
           int status,
           java.lang.String description) {
           this.status = status;
           this.description = description;
    }


    /**
     * Gets the status value for this BaseResponse.
     * 
     * @return status
     * <p>
     *  Possible values:
     *  <ul>
     *  <li>OK = 0</li>
	 *<li>FATAL = 1</li>
	 *<li>DB_ERROR = 2</li>
	 *<li>AUTHORIZATION_ERROR = 3</li>
	 *<li>SERVER_ERROR = 4</li>
	 *<li>ALREADY_EXISTS = 5</li>
	 *<li>NOT_FOUND = 6</li>
	 *<li>NOT_ALLOWED_OPERATION = 7</li>
	 *<li>VALIDATION_ERROR = 8</li>
	 *</ul>
	 *</p>
     */
    public int getStatus() {
        return status;
    }


    /**
     * Sets the status value for this BaseResponse.
     * @deprecated
     * @param status
     */
    public void setStatus(int status) {
        this.status = status;
    }


    /**
     * Gets the description value for this BaseResponse.
     * 
     * @return description
     */
    public java.lang.String getDescription() {
        return description;
    }


    /**
     * Sets the description value for this BaseResponse.
     * @deprecated
     * @param description
     */
    public void setDescription(java.lang.String description) {
        this.description = description;
    }

    private java.lang.Object __equalsCalc = null;
    /**
     * @deprecated
     */
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof BaseResponse)) return false;
        BaseResponse other = (BaseResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.status == other.getStatus() &&
            ((this.description==null && other.getDescription()==null) || 
             (this.description!=null &&
              this.description.equals(other.getDescription())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    /**
     * @deprecated
     */
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getStatus();
        if (getDescription() != null) {
            _hashCode += getDescription().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(BaseResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://service.dms.pl/ws/types/", "BaseResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("status");
        elemField.setXmlName(new javax.xml.namespace.QName("", "status"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("description");
        elemField.setXmlName(new javax.xml.namespace.QName("", "description"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * @deprecated
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * @deprecated
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * @deprecated
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
