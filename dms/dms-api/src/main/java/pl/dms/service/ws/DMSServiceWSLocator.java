/**
 * DMSServiceWSLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package pl.dms.service.ws;

import org.aeonbits.owner.ConfigFactory;

import pl.dms.commons.ServerConfig;
import pl.dms.commons.ServerConfigManager;

public class DMSServiceWSLocator extends org.apache.axis.client.Service implements pl.dms.service.ws.DMSServiceWS {

	ServerConfig cfg = ServerConfigManager.getSeverConfig();
	
	private final static String documentManagementSystemUrl = "documentManagementSystemUrl";
	
    public DMSServiceWSLocator() {
    }


    public DMSServiceWSLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public DMSServiceWSLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for DMSServiceWSPort
    public java.lang.String getDMSServiceWSPortAddress() {
        return cfg.documentManagementSystemUrl();
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String DMSServiceWSPortWSDDServiceName = "DMSServiceWSPort";

    public java.lang.String getDMSServiceWSPortWSDDServiceName() {
        return DMSServiceWSPortWSDDServiceName;
    }

    public void setDMSServiceWSPortWSDDServiceName(java.lang.String name) {
        DMSServiceWSPortWSDDServiceName = name;
    }

    public pl.dms.service.ws.DMSServiceWSPortType getDMSServiceWSPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(getDMSServiceWSPortAddress());
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getDMSServiceWSPort(endpoint);
    }

    public pl.dms.service.ws.DMSServiceWSPortType getDMSServiceWSPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            pl.dms.service.ws.DMSServiceWSSoapBindingStub _stub = new pl.dms.service.ws.DMSServiceWSSoapBindingStub(portAddress, this);
            _stub.setPortName(getDMSServiceWSPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setDMSServiceWSPortEndpointAddress(java.lang.String address) {
    	cfg.setProperty(documentManagementSystemUrl, address);
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (pl.dms.service.ws.DMSServiceWSPortType.class.isAssignableFrom(serviceEndpointInterface)) {
                pl.dms.service.ws.DMSServiceWSSoapBindingStub _stub = new pl.dms.service.ws.DMSServiceWSSoapBindingStub(new java.net.URL(getDMSServiceWSPortAddress()), this);
                _stub.setPortName(getDMSServiceWSPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("DMSServiceWSPort".equals(inputPortName)) {
            return getDMSServiceWSPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://service.dms.pl/ws", "DMSServiceWS");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://service.dms.pl/ws", "DMSServiceWSPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("DMSServiceWSPort".equals(portName)) {
            setDMSServiceWSPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
