package pl.dms.commons;

import javax.management.JMX;
import javax.management.MBeanServerConnection;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;

public class ServerConfigRemoteManager {

	private ServerConfigRemoteManager(){}
	
	public static ServerConfigServiceApi createServerConfigRemote() throws Throwable{
		
		ServerConfig cfg = ServerConfigManager.getSeverConfig();
		String host = cfg.documentManagementSystemHost();
		String port = cfg.jbossJmxPort();
		String urlString = System.getProperty("jmx.service.url", "service:jmx:remoting-jmx://" + host + ":" + port);
		JMXServiceURL serviceURL = new JMXServiceURL(urlString);
		JMXConnector jmxConnector = JMXConnectorFactory.connect(serviceURL, null);
		MBeanServerConnection connection = jmxConnector.getMBeanServerConnection();

		ObjectName mbeanName = new ObjectName("DMSManagement", "type", "pl.dms.ejb.ServerConfigService");
		return JMX.newMBeanProxy(connection, mbeanName, ServerConfigServiceApi.class, true);
	}
}
	
