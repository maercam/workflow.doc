package pl.dms.commons;

public interface Install {

	public final static String DMS_ADMIN_USER = "admin";
	
	void createAdminAccount(String user, String password, String email);

}