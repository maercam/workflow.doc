package pl.dms.service.ws;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertThat;
import static pl.dms.service.ws.types.BaseResponse.OK;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import pl.dms.commons.ServerConfig;
import pl.dms.commons.ServerConfigManager;
import pl.dms.service.ws.types.Action;
import pl.dms.service.ws.types.ActionTrace;
import pl.dms.service.ws.types.AuthenticationResponse;
import pl.dms.service.ws.types.BaseResponse;
import pl.dms.service.ws.types.CreationResponse;
import pl.dms.service.ws.types.DocumentProcess;
import pl.dms.service.ws.types.State;
import pl.dms.service.ws.types.Workflow;
import static pl.dms.service.ws.IntegrationTestsUtil.*;
public class DocumentManagementL2Test extends RemoteBaseL2Test{

	private static final String USER = "user";
	private static final String DOCUMENT = "document";
	private static final String ADMIN = "admin";
	private static final String OPERATOR = "operator";
	private static String sid;
	private static DMSServiceWSPortType dmsProxy;

	static ServerConfig cfg = ServerConfigManager.getSeverConfig();
	private static long workflowId;
	private DocumentProcess[] allDocuments;

	@Before
	public void setUpFixture() throws RemoteException {
		dmsProxy = new DMSServiceWSPortTypeProxy();
		login(ADMIN, ADMIN);
		Workflow[] workflows = dmsProxy.getWorkflows(sid);
		for (Workflow workflow : workflows) {
			BaseResponse deleteWorkflow = dmsProxy.deleteWorkflow(sid, workflow.getId());
			assertThat(deleteWorkflow.getStatus(), is(0));
		}

		workflows = dmsProxy.getWorkflows(sid);
		assertThat(workflows.length, is(0));
		initWorkflows();
	}

	private static void initWorkflows() throws RemoteException {
		for (int i = 0; i < 1; ++i) {
			Workflow workflow = new Workflow();
			workflow.setName("workflow" + i);
			workflowId = dmsProxy.addWorkflow(sid, workflow).getId();
			for (int j = 0; j < 4; ++j) {
				State state = new State();
				state.setName("state" + j);
				dmsProxy.addState(sid, state, workflowId).getId();
			}
			State[] states = dmsProxy.getStates(sid, workflowId);
			Long begin = null;
			Long end = null;
			List<Long> statesIds = new ArrayList<Long>();
			for (State state : states) {
				if (state.getType() == 2) {
					begin = state.getId();
				} else if (state.getType() == 3) {
					end = state.getId();
				} else if (state.getType() == 1) {
					continue;
				} else {
					statesIds.add(state.getId());
				}
			}

			for (int m = 0; m < statesIds.size() - 1; ++m) {
				Action action = new Action();
				action.setName("action" + (m + 1));
				action.setFromStateId(statesIds.get(m));
				action.setToStateId(statesIds.get(m + 1));
				action.setRole(cfg.dmsUserRole());
				dmsProxy.addAcction(sid, action, statesIds.get(m));
			}
			Action action = new Action();
			action.setName("action" + 0);
			action.setFromStateId(begin);
			action.setToStateId(statesIds.get(0));
			action.setRole(cfg.dmsUserRole());
			dmsProxy.addAcction(sid, action, begin);

			Action actionEnd = new Action();
			actionEnd.setName("action" + statesIds.size());
			actionEnd.setFromStateId(statesIds.get(statesIds.size() - 1));
			actionEnd.setToStateId(end);
			action.setRole(cfg.dmsUserRole());
			dmsProxy.addAcction(sid, actionEnd, statesIds.get(statesIds.size() - 1));

		}

	}

	private static void login(String user, String password) throws RemoteException {
		AuthenticationResponse response = dmsProxy.login(user, password);
		assertResponse(response, "Login ok", OK);
		sid = response.getSid();
	}

	@Test
	public void testCreateDocument() throws Throwable {
		AuthenticationResponse response = dmsProxy.login(OPERATOR, OPERATOR);
		sid = response.getSid();
		DocumentProcess process = new DocumentProcess();

		process.setName("doc");
		process.setWorkflowId(workflowId);
		CreationResponse creationResponse = dmsProxy.addDocument(sid, process);

		assertThat(creationResponse.getStatus(), is(BaseResponse.OK));

		

	}

	@Test
	public void testSimpleCreateWorkflow() throws RemoteException {

		// arrange
		String expectedDocumentName = DOCUMENT;

		// act
		login(ADMIN, ADMIN);
		Workflow workflow = new Workflow();
		workflow.setName("taki");
		CreationResponse createResponse = dmsProxy.addWorkflow(sid, workflow);
		assertResponse(createResponse, "Workflow created", OK);

		workflowId = createResponse.getId();

		dmsProxy.logout(sid);
		login(OPERATOR, OPERATOR);
		DocumentProcess documentProcess = new DocumentProcess();
		documentProcess.setName(expectedDocumentName);
		documentProcess.setWorkflowId(workflowId);
		createResponse = dmsProxy.addDocument(sid, documentProcess);
		assertResponse(createResponse, "Document created", OK);

		dmsProxy.logout(sid);
		login(ADMIN, ADMIN);
		allDocuments = dmsProxy.getAllDocuments(sid);

		assertThat(allDocuments.length, is(not(0)));

		DocumentProcess createdDocument = allDocuments[0];

		assertThat(createdDocument.getName(), is(expectedDocumentName));
		dmsProxy.logout(sid);


	}

	@Test
	public void testGetActionTrace() throws RemoteException {
		login(OPERATOR, OPERATOR);

		DocumentProcess documentProcess = new DocumentProcess();
		documentProcess.setWorkflowId(workflowId);
		documentProcess.setName("nazwadda");

		CreationResponse response = dmsProxy.addDocument(sid, documentProcess);

		assertResponse(response, "Document created", OK);
		
		long documentId = response.getId();

		State[] states = dmsProxy.getStates(sid, workflowId);
		
		assertThat(states.length, is(7));

		State unregisterdState = getStateWithType(states, 1);

		Action[] actions = dmsProxy.getActions(sid, unregisterdState.getId());

		long actionId = actions[0].getId();

		dmsProxy.executeAction(sid, documentId, actionId);
		dmsProxy.logout(sid);
		login(ADMIN, ADMIN);

		DocumentProcess[] docs = dmsProxy.getAllDocuments(sid);
		DocumentProcess doc = docs[0];

		ActionTrace[] trace = doc.getActionTraces();
		
		assertThat(trace, is(notNullValue()));
		
		assertThat(trace.length, is(not(0)));

	}
	
	@Test
	public void testDeleteActionWithExistingActionTrace() throws RemoteException{
		dmsProxy.logout(sid);
		login(OPERATOR, OPERATOR);
		DocumentProcess documentProcess = new DocumentProcess();
		documentProcess.setName("name");
		documentProcess.setWorkflowId(workflowId);
		
		CreationResponse creationResponse = dmsProxy.addDocument(sid, documentProcess);
		assertResponse(creationResponse, "Document created", BaseResponse.OK);
		
		long documentId = creationResponse.getId();
		
		State[] states = dmsProxy.getStates(sid, workflowId);
		assertThat(states.length, is(7));
		
		State startState = getStateWithType(states, 2);
		
		long stateId = startState.getId();
		
		Action[] actions = dmsProxy.getActions(sid, stateId);
		
		Action action = actions[0];
		
		dmsProxy.logout(sid);
		login(USER, USER);
		
		BaseResponse executeResponse = dmsProxy.executeAction(sid, documentId, action.getId());
		assertResponse(executeResponse, "Action action0 on process name executed", BaseResponse.OK);
		
		dmsProxy.logout(sid);
		
		login(ADMIN, ADMIN);
		
		BaseResponse deleteResponse = dmsProxy.deleteAction(sid, action.getId());
		assertResponse(deleteResponse, "Cannon remove executed actions."
				+ " You only remove all workflow but all connected documents will be removed.", BaseResponse.VALIDATION_ERROR);
		
	}
	
	@Test
	public void deleteCurrentStateSameDocument() throws RemoteException{
		dmsProxy.logout(sid);
		login(OPERATOR, OPERATOR);
		
		DocumentProcess documentProcess = new DocumentProcess();
		documentProcess.setName("sss");
		documentProcess.setWorkflowId(workflowId);
		CreationResponse creationResponse = dmsProxy.addDocument(sid, documentProcess);
		assertResponse(creationResponse, "Document created", BaseResponse.OK);
		
		long documentId = creationResponse.getId();
		
		
		State[] states = dmsProxy.getStates(sid, workflowId);
		assertThat(states.length, is(7));;
		
		State startState = getStateWithType(states, 2);
		
		dmsProxy.logout(sid);
		
		login(USER,USER);
		
		Action[] actions = dmsProxy.getActions(sid, startState.getId());
		
		Action action = actions[0];
		
		dmsProxy.executeAction(sid, documentId, action.getId());
		
		long currentStateId = action.getToStateId();
		
		dmsProxy.logout(sid);
		login(ADMIN,ADMIN);
		
		BaseResponse deleteResponse = dmsProxy.deleteState(sid, currentStateId);
		assertResponse(deleteResponse, "This state cannot be removed. "
				+ "Remove action from and action to before", BaseResponse.VALIDATION_ERROR);
		
	}

	


}
