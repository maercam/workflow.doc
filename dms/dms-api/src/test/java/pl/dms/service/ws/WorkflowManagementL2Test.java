package pl.dms.service.ws;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static pl.dms.service.ws.IntegrationTestsUtil.assertResponse;
import static pl.dms.service.ws.IntegrationTestsUtil.getStateWithType;

import java.rmi.RemoteException;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import pl.dms.service.ws.types.Action;
import pl.dms.service.ws.types.AuthenticationResponse;
import pl.dms.service.ws.types.BaseResponse;
import pl.dms.service.ws.types.CreationResponse;
import pl.dms.service.ws.types.State;
import pl.dms.service.ws.types.Workflow;
public class WorkflowManagementL2Test extends RemoteBaseL2Test{

	private static DMSServiceWSPortType dmsProxy ;
	private static String sid;

	@BeforeClass
	public static void setUpFixture() throws RemoteException {
		dmsProxy = new DMSServiceWSPortTypeProxy();
		sid = login("admin","admin");
		
	}

	private static String login(String user, String password) throws RemoteException {
		AuthenticationResponse response = dmsProxy.login(user, password);
		return response.getSid();
	}

	@Before
	public void setUp() throws RemoteException{
		Workflow[] workflows = dmsProxy.getWorkflows(sid);
		for(Workflow workflow : workflows){
			BaseResponse deleteWorkflow = dmsProxy.deleteWorkflow(sid, workflow.getId());
			assertThat(deleteWorkflow.getStatus(), is(0));
		}
		
		workflows = dmsProxy.getWorkflows(sid);
		assertThat(workflows.length, is(0));
	}
	
	@Test
	public void testCrudWorkflow() throws RemoteException {
		//arrange
		Workflow workflow = new Workflow();
		workflow.setName("sampleName");
		String expectedDescription = "Workflow created";
		
		//act
		CreationResponse response = dmsProxy.addWorkflow(sid, workflow);
		int actualStatus = response.getStatus();
		long actualId = response.getId();
		String actualDescription = response.getDescription();
		
		//assert
		assertThat(actualDescription, is(expectedDescription));
		assertThat(actualId, is(notNullValue()));
		assertThat(actualStatus, is(0));
		
		Workflow [] workflows = dmsProxy.getWorkflows(sid);
		
		assertThat(workflows.length, is(1));
		
		long createdWorkflowId = workflows[0].getId();
		State[] states = dmsProxy.getStates(sid, createdWorkflowId);
		
		assertThat(states.length, is(3));
		
		
		//tearDown
		String expectedDeleteDescription = "Workflow with id " + actualId + " deleted";
		BaseResponse deleteResponse = dmsProxy.deleteWorkflow(sid, actualId);
		int actualDeleteStatus = deleteResponse.getStatus();
		String actualDeleteDescription = deleteResponse.getDescription();
		
		assertThat(actualDeleteDescription, is(expectedDeleteDescription));
		assertThat(actualDeleteStatus, is(0));
		
		
		workflows = dmsProxy.getWorkflows(sid);
		assertThat(workflows.length, is(0));
	}
	
	@Test
	public void testCreateState() throws Throwable{
		Workflow workflow = new Workflow();
		workflow.setName("aaa");
		CreationResponse responseWorkflow = dmsProxy.addWorkflow(sid, workflow);
		assertThat(responseWorkflow.getStatus(), is(0));
		
		long id = responseWorkflow.getId();
		
		State newState = new State();
		newState.setName("bbb");
		CreationResponse response = dmsProxy.addState(sid, newState, id);
		
		
		assertThat(response.getStatus(), is(0));
		
		long createdStateId = response.getId();
		
		State[] states = dmsProxy.getStates(sid, id);
		
		boolean found = false;
		for(State state : states){
			if(state.getName().equals("bbb")){
				found = true;
			}
		}
		if(!found){
			fail("State not created");
		}
		
		//tearDown
		
		BaseResponse deleteResponse = dmsProxy.deleteState(sid, createdStateId);
		
		int deleteStatus = deleteResponse.getStatus();
		String deleteDescription = deleteResponse.getDescription();
		
		assertThat(deleteStatus, is(0));
		assertThat(deleteDescription, is("State with name bbb deleted"));
		
	}
	
	
	@Test
	public void testCreateAction() throws Throwable{
		Workflow workflow = new Workflow();
		workflow.setName("Workflow1");
		CreationResponse responseWorkflow = dmsProxy.addWorkflow(sid, workflow);
		
		long id = responseWorkflow.getId();
		
		State stateA = new State();
		stateA.setName("StateA");
		CreationResponse responseState = dmsProxy.addState(sid, stateA, id);
		long stateAId = responseState.getId();
		
		State stateB = new State();
		stateB.setName("StateB");
		responseState = dmsProxy.addState(sid, stateB, id);
		long stateBId = responseState.getId();
		Action actionFormAToB = new Action();
		actionFormAToB.setName("akcja");
		actionFormAToB.setFromStateId(stateAId);
		actionFormAToB.setToStateId(stateBId);
		
		CreationResponse creationResponse = dmsProxy.addAcction(sid, actionFormAToB, stateAId);
		
		assertThat(creationResponse.getDescription(), is("Action with name akcja created"));
		assertThat(creationResponse.getStatus(), is(0));
		
		Action[] actions = dmsProxy.getActions(sid, stateAId);
		boolean found = false;
		for(Action action : actions){
			if(action.getName().equals(actionFormAToB.getName())){
				found = true;
			}
		}
		
		if(!found){
			fail("Action not created");
		}
		long actionId = creationResponse.getId();
		
		BaseResponse deleteResponse = dmsProxy.deleteAction(sid, actionId);
		
		assertThat(deleteResponse.getDescription(), is("action deleted"));
		assertThat(deleteResponse.getStatus(), is(0));
		
		Action[] actionsAfterRemove = dmsProxy.getActions(sid, stateAId);
		found = false;
		for(Action action : actionsAfterRemove){
			if(action.getName().equals(actionFormAToB.getName())){
				found = true;
			}
		}
		
		if(found){
			fail("Action not deleted");
		}
	
	}
	
	@Test
	public void testDeleteState() throws RemoteException{
		Workflow workflow = new Workflow();
		workflow.setName("www");
		
		CreationResponse response = dmsProxy.addWorkflow(sid, workflow);
		assertResponse(response, "Workflow created", BaseResponse.OK);	
		
		long workflowId = response.getId();
		State state = new State();
		state.setName("state0");
		
		response = dmsProxy.addState(sid, state, workflowId);
		assertResponse(response, "State with name state0 created", BaseResponse.OK);
		
		long newStateId = response.getId();
		
		State[] states = dmsProxy.getStates(sid, workflowId);
		State unregistered = getStateWithType(states, 1);
		State start = getStateWithType(states, 2);
		State end = getStateWithType(states, 3);
		
		Action action = new Action();
		action.setName("kup nożyczki");
		action.setFromStateId(start.getId());
		action.setToStateId(newStateId);
		
		response = dmsProxy.addAcction(sid, action, start.getId());
		assertResponse(response, "Action with name kup nożyczki created", BaseResponse.OK);
		
		BaseResponse deleteResponse = dmsProxy.deleteState(sid, newStateId);
		assertResponse(deleteResponse, "This state cannot be removed. Remove action from and action to before", BaseResponse.VALIDATION_ERROR);
		
		deleteResponse = dmsProxy.deleteAction(sid, response.getId());
		assertResponse(deleteResponse, "action deleted", BaseResponse.OK);
		
		deleteResponse = dmsProxy.deleteState(sid, newStateId);
		assertResponse(deleteResponse, "State with name state0 deleted", BaseResponse.OK);
	}
	
}
