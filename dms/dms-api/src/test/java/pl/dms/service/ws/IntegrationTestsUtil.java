package pl.dms.service.ws;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import pl.dms.service.ws.types.BaseResponse;
import pl.dms.service.ws.types.State;

public class IntegrationTestsUtil {
	public static void assertResponse(BaseResponse response, String expectedDescription, int expectedStatus) {
		assertThat(response.getDescription(), is(expectedDescription));
		assertThat(response.getStatus(), is(expectedStatus));
	}
	
	public static State getStateWithType(State[] states, int type) {
		for (State state : states) {
			if (state.getType() == type) {
				return state;
			}
		}
		return null;
	}
}
