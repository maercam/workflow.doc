package pl.dms.service.ws;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertThat;

import java.rmi.RemoteException;

import org.junit.BeforeClass;
import org.junit.Test;

import pl.dms.service.ws.types.AuthenticationResponse;
import pl.edu.agh.useraccounts.service.UserService;
import pl.edu.agh.useraccounts.service.UserServiceProxy;

public class UserManagementL2Test extends RemoteBaseL2Test{

	@Test
	public void testLogin() throws RemoteException {
		// arrange
		DMSServiceWSPortType dmsServiceProxy = new DMSServiceWSPortTypeProxy();
		
		int expectedStatus = 0;
		String expectedDescription = "Login ok";

		// act
		
		
		AuthenticationResponse response = dmsServiceProxy.login("admin", "admin");
		String actualSid = response.getSid();
		int actutalStatus = response.getStatus();
		String actualDescription = response.getDescription();

		// assert
		assertThat(actualDescription, is(expectedDescription));
		assertThat(actutalStatus, is(expectedStatus));
		assertThat(actualSid, is(notNullValue()));
	}
	
	@Test
	public void testGetUsers() throws RemoteException{
		UserService userService = new UserServiceProxy();
		String[] users = userService.getUsers();
		
		assertThat(users.length, is(3));
	}

}
