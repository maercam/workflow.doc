package pl.dms;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

import java.rmi.RemoteException;

import org.junit.BeforeClass;
import org.junit.Test;

import pl.dms.service.ws.DMSServiceWSPortType;
import pl.dms.service.ws.DMSServiceWSPortTypeProxy;
import pl.dms.service.ws.RemoteBaseL2Test;
import pl.dms.service.ws.types.AuthenticationResponse;
import pl.dms.service.ws.types.BaseResponse;
import pl.dms.service.ws.types.Workflow;


public class WorkflowBeanL2Test extends RemoteBaseL2Test{

	private static DMSServiceWSPortType dmsProxy ;
	private static String sid;

	@BeforeClass
	public static void setUpFixture() throws RemoteException {
		dmsProxy = new DMSServiceWSPortTypeProxy();
		sid = login("admin","admin");
		Workflow[] workflows = dmsProxy.getWorkflows(sid);
		for(Workflow workflow : workflows){
			BaseResponse deleteWorkflow = dmsProxy.deleteWorkflow(sid, workflow.getId());
			assertThat(deleteWorkflow.getStatus(), is(0));
		}
		
		workflows = dmsProxy.getWorkflows(sid);
		assertThat(workflows.length, is(0));
	}

	private static String login(String user, String password) throws RemoteException {
		AuthenticationResponse response = dmsProxy.login(user, password);
		return response.getSid();
	}

	@Test
	public void testCrudWorkflow() throws RemoteException {
		//CREATE WORKFLOW przekierowanie na worflow z podanym ID
		//Faces.redirect("dashboard/workflow/workflow.xhtml?workflowId=%s", String.valueOf(response.getId()));

		//MOŻNA USUNĄĆ WORFLOW O PODANYM ID

	}
	
}
