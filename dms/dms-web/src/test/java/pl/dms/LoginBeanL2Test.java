package pl.dms;

import static org.easymock.EasyMock.expect;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertThat;
import static org.powermock.api.easymock.PowerMock.createMock;
import static org.powermock.api.easymock.PowerMock.expectLastCall;
import static org.powermock.api.easymock.PowerMock.mockStatic;
import static org.powermock.api.easymock.PowerMock.replayAll;

import java.io.IOException;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.omnifaces.util.Faces;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import pl.dms.bean.LoginBean;
import pl.dms.bean.session.UserSessionBean;
import pl.dms.service.ws.DMSServiceWSPortType;
import pl.dms.service.ws.DMSServiceWSPortTypeProxy;
import pl.dms.service.ws.RemoteBaseL2Test;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ Faces.class, FacesContext.class })
@PowerMockIgnore("org.apache.logging.log4j.*")
public class LoginBeanL2Test extends RemoteBaseL2Test {

	private static DMSServiceWSPortType dmsProxy;

	@Test
	public void testLoginLogoutAdmin() throws IOException {

		mockStatic(Faces.class);
		Faces.redirect("dashboard/main.xhtml");
		expectLastCall().once();
		
		Faces.redirect("/index.xhtml");
		expectLastCall().once();
		
		Faces.invalidateSession();
		expectLastCall().once();
		
		mockStatic(FacesContext.class);

		FacesContext facesContextMock = createMock(FacesContext.class);
		ExternalContext ecMock = createMock(ExternalContext.class);

		expect(FacesContext.getCurrentInstance()).andReturn(facesContextMock).once();
		expect(facesContextMock.getExternalContext()).andReturn(ecMock).once();
		expect(ecMock.getRequestContextPath()).andReturn("").once();

		replayAll(facesContextMock, ecMock);

		dmsProxy = new DMSServiceWSPortTypeProxy();

		LoginBean loginBean = new LoginBean();
		loginBean.setUserSessionBean(new UserSessionBean());
		loginBean.setLogin("admin");
		loginBean.setPassword("admin");
		loginBean.login();

		assertThat(loginBean.getUserSessionBean().getSid(), is(notNullValue()));

		loginBean.logout();

	}
}
