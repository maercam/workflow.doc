package pl.dms.jpa.status;

public enum StateType {

	NOTREGISTERED(1, "Niezarejestrowany"), START(2, "Początek"), END(3, "Koniec"), STANDARD(4, "Standardowy");

	private int value;
	private String description;

	private StateType(int typeValue, String description) {
		this.value = typeValue;
		this.description = description;
	}

	public int getValue() {
		return value;
	}

	public String getDescription() {
		return description;
	}

	public static StateType of(int num) {

		switch (num) {
		case 1:
			return NOTREGISTERED;
		case 2:
			return START;
		case 3:
			return END;
		case 4:
			return STANDARD;
		default:
			return null;
		}

	}

}
