package pl.dms.util;

import pl.dms.commons.ServerConfigManager;

/**
 * Dwie najważniejsze role w ramach systemu DMS
 */

public enum UserRole {

	DMS_USER(0,  ServerConfigManager.getSeverConfig().dmsUserRole(), "Role for normal users."),
	DMS_ADMIN(1, ServerConfigManager.getSeverConfig().dmsAdminRole(), "Role for administratiors."),
	DMS_REGISTER_DOCUMENT(2, ServerConfigManager.getSeverConfig().dmsRegisteDocumentRole(), "Role for user, who register documents.");

	private int value;
	private String name;
	private String description;

	private UserRole(int value, String name, String description) {
		this.value = value;
		this.name = name;
		this.description = description;
	}

	public int getValue() {
		return value;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

}
