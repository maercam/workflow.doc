package pl.dms.filter;

import java.io.IOException;
import java.util.logging.Logger;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.omnifaces.filter.HttpFilter;

import pl.dms.bean.session.UserSessionBean;
import pl.dms.util.UserRole;

@WebFilter("/dashboard/*")
public class DashboardFilter extends HttpFilter {

	private static final Logger logger = Logger.getLogger(DashboardFilter.class.toString());

	@Override
	public void doFilter(HttpServletRequest req, HttpServletResponse resp, HttpSession session, FilterChain chain) throws ServletException, IOException {
		logger.info("DashboardFilter.doFilter, invoked for localAddr: " + req.getLocalAddr() + ", url: " + req.getRequestURI());

		if (session != null) {
			UserSessionBean userSession = (UserSessionBean) session.getAttribute("userSessionBean");
			if (userSession.isLogged() == true) {

				if (req.getRequestURI().matches("^(.*/dashboard/workflow/.*)$") && userSession.getRoles().contains(UserRole.DMS_ADMIN.getName()) == false) {
					resp.sendRedirect(req.getContextPath() + "/dashboard/main.xhtml");
				} else {
					chain.doFilter(req, resp);
				}

			} else {
				resp.sendRedirect(req.getContextPath() + "/index.xhtml");
			}
		} else {
			resp.sendRedirect(req.getContextPath() + "/index.xhtml");
		}

	}

}
