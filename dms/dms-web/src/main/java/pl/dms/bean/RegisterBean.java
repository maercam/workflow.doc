package pl.dms.bean;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@ManagedBean
@RequestScoped
public class RegisterBean {

	private static final Logger logger = LogManager.getLogger(RegisterBean.class);

	
	
	public RegisterBean() {
	}

	public void register() {
		logger.info("RegisterBean.register - invoke");
	}

}
