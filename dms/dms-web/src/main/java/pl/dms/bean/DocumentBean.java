package pl.dms.bean;

import java.io.IOException;
import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.omnifaces.util.Faces;
import org.omnifaces.util.Messages;

import pl.dms.bean.session.UserSessionBean;
import pl.dms.service.ws.DMSServiceWSPortTypeProxy;
import pl.dms.service.ws.types.Action;
import pl.dms.service.ws.types.ActionTrace;
import pl.dms.service.ws.types.BaseResponse;
import pl.dms.service.ws.types.CreationResponse;
import pl.dms.service.ws.types.DocumentProcess;
import pl.dms.service.ws.types.State;
import pl.dms.service.ws.types.Workflow;
import pl.dms.util.DMSWebUtil;
import pl.dms.util.UserRole;

@ManagedBean
@ViewScoped
public class DocumentBean implements Serializable {

	private static final long serialVersionUID = 1235532065234908038L;
	private static final Logger logger = Logger.getLogger(DocumentBean.class.toString());

	@ManagedProperty(value = "#{userSessionBean}")
	private UserSessionBean userSessionBean;

	private Long documentId;

	private DocumentProcess document;

	private List<Workflow> workflows;
	private long workflowId;

	private Workflow workflow;
	private List<State> statesForWorkflow;
	private Map<Long, List<Action>> actionsForState;

	public DocumentBean() {
	}

	public void preRender() {
		logger.info("DocumentBean.preRender, documentId: " + documentId);

		DMSServiceWSPortTypeProxy dmsProxy = new DMSServiceWSPortTypeProxy();

		try {
			Workflow[] workflowArray = dmsProxy.getWorkflows(userSessionBean.getSid());
			if (workflowArray != null) {
				workflows = Arrays.asList(workflowArray);
			}
		} catch (RemoteException e) {
			logger.log(Level.SEVERE, "DocumentBean.preRender.DMSServiceWSPortTypeProxy.workflows.RemoteException", e);
		}

		if (DMSWebUtil.isIdSet(documentId) == true) {
			// edycja dokumentu

			try {
				DocumentProcess[] documents = dmsProxy.getAllDocuments(userSessionBean.getSid());
				if (documents != null) {

					for (DocumentProcess doc : documents) {

						if (documentId.equals(doc.getId()) == true) {
							document = doc;
							break;
						}

					}

					if (document != null) {

						workflowId = document.getWorkflowId();
						for (Workflow w : workflows) {
							if (w.getId() == workflowId) {
								workflow = w;
								break;
							}
						}

						if (workflow != null) {
							State[] states = dmsProxy.getStates(userSessionBean.getSid(), workflowId);
							if (states != null) {
								statesForWorkflow = Arrays.asList(states);
								actionsForState = new HashMap<Long, List<Action>>();
								for (State state : statesForWorkflow) {
									Action[] actions = dmsProxy.getActions(userSessionBean.getSid(), state.getId());
									if (actions != null) {
										actionsForState.put(state.getId(), Arrays.asList(actions));
									}
								}

							}
						}

					}

				}

			} catch (RemoteException e) {
				logger.log(Level.SEVERE, "DocumentBean.preRender.DMSServiceWSPortTypeProxy.RemoteException", e);
			}

		} else {
			// nowy dokument

			document = new DocumentProcess();
			workflowId = -1;
		}

	}

	public String stateColor(long stateId) {

		if (document.getCurrentStateId() == stateId) {
			return "color:green";
		}

		List<Action> actions = actionsForState.get(stateId);
		ActionTrace[] actionTraceArray = document.getActionTraces();

		if (actions == null || actionTraceArray == null) {
			return "color:black";
		}

		List<ActionTrace> actionTraces = Arrays.asList(actionTraceArray);
		for (ActionTrace at : actionTraces) {
			for (Action a : actions) {
				if (at.getActionId() == a.getId()) {
					return "color:blue";
				}
			}
		}

		return "color:black";
	}

	public String actionColor(long actionId) {

		ActionTrace[] actionTraceArray = document.getActionTraces();

		if (actionTraceArray != null) {
			logger.info("DocumentBean.actionColor -> actionId:" + actionId + ", size:" + actionTraceArray.length);
			List<ActionTrace> actionTraces = Arrays.asList(actionTraceArray);
			for (ActionTrace at : actionTraces) {
				logger.info("ActionId:" + actionId + ", ActionTrace.actionId:" + at.getActionId());
				if (at.getActionId() == actionId) {
					logger.info("ActionId:" + actionId + ", ActionTrace.actionId:" + at.getActionId() + ", should be blue!");
					return "color:blue";
				}
			}
		}

		return "color:black";
	}

	public String nextStateName(long stateId) {
		for (State s : statesForWorkflow) {
			if (s.getId() == stateId) {
				return s.getName();
			}
		}
		return "";
	}

	public String roleNameForAction(long actionId) {
		for (List<Action> actions : actionsForState.values()) {
			for (Action action : actions) {
				if (action.getId() == actionId) {
					return action.getRole();
				}
			}
		}
		return "";
	}

	public void executeAction(long actionId) {
		logger.info("DocumentBean.executeAction - invoked, actionId: " + actionId);

		try {

			DMSServiceWSPortTypeProxy dmsProxy = new DMSServiceWSPortTypeProxy();

			BaseResponse response = dmsProxy.executeAction(userSessionBean.getSid(), documentId, actionId);

			if (response.getStatus() == BaseResponse.OK) {
				logger.info("DocumentBean.executeAction - executeAction for : " + actionId);
				Faces.redirect("dashboard/document/document.xhtml?faces-redirect=true&documentId=%s", String.valueOf(documentId));
			} else {
				Messages.addGlobalError(response.getDescription());
				logger.warning("DocumentBean.executeAction - Bad response status: " + response.getStatus());
			}

		} catch (RemoteException e) {
			logger.log(Level.SEVERE, "DocumentBean.preRender.DMSServiceWSPortTypeProxy.workflows.RemoteException", e);
		} catch (IOException e) {
			Messages.addGlobalError("Redirection error!");
		}
	}

	public void createDocument() {
		logger.info("DocumentBean.createDocument - invoked, for workflowId: " + workflowId);

		try {

			document.setWorkflowId(workflowId);

			DMSServiceWSPortTypeProxy dmsProxy = new DMSServiceWSPortTypeProxy();
			CreationResponse response = dmsProxy.addDocument(userSessionBean.getSid(), document);

			if (response.getStatus() == BaseResponse.OK) {
				logger.info("DocumentBean.createDocument - created document with id: " + response.getId());
				Faces.redirect("dashboard/document/document.xhtml?faces-redirect=true&documentId=%s", String.valueOf(response.getId()));
			} else {
				Messages.addGlobalError("Nie masz prawa do tworzenia dokumentów.");
				logger.warning("DocumentBean.createDocument - Bad response status: " + response.getStatus());
			}

		} catch (RemoteException e) {
			logger.log(Level.SEVERE, "DocumentBean.createDocument.RemoteException", e);
		} catch (IOException e) {
			logger.log(Level.SEVERE, "DocumentBean.createDocument.IOException", e);
			Messages.addGlobalError("Problem podczas przekierowania na stronę.");
		} catch (Exception e) {
			logger.log(Level.SEVERE, "DocumentBean.createDocument.Exception", e);
		}

	}

	public boolean activeState(long stateId) {

		if (document.getCurrentStateId() == stateId) {
			return true;
		}

		return false;

	}

	public boolean hasUserRoleForAction(long actionId) {

		for (List<Action> actions : actionsForState.values()) {

			for (Action action : actions) {

				if (action.getId() == actionId) {
					if (userSessionBean.getRoles().contains(action.getRole()) == true) {
						return true;
					} else {
						return false;
					}
				}

			}

		}

		return false;
	}

	public List<Action> getActionsForState(Long stateId) {

		if (stateId == null || actionsForState == null || actionsForState.get(stateId) == null) {
			return new ArrayList<Action>();
		}

		return actionsForState.get(stateId);
	}

	public Long getDocumentId() {
		return documentId;
	}

	public void setDocumentId(Long documentId) {
		this.documentId = documentId;
	}

	public DocumentProcess getDocument() {
		return document;
	}

	public void setDocument(DocumentProcess document) {
		this.document = document;
	}

	public UserSessionBean getUserSessionBean() {
		return userSessionBean;
	}

	public void setUserSessionBean(UserSessionBean userSessionBean) {
		this.userSessionBean = userSessionBean;
	}

	public List<Workflow> getWorkflows() {
		return workflows;
	}

	public void setWorkflows(List<Workflow> workflows) {
		this.workflows = workflows;
	}

	public long getWorkflowId() {
		return workflowId;
	}

	public void setWorkflowId(long workflowId) {
		this.workflowId = workflowId;
	}

	public Workflow getWorkflow() {
		return workflow;
	}

	public void setWorkflow(Workflow workflow) {
		this.workflow = workflow;
	}

	public List<State> getStatesForWorkflow() {
		return statesForWorkflow;
	}

	public void setStatesForWorkflow(List<State> statesForWorkflow) {
		this.statesForWorkflow = statesForWorkflow;
	}

	public Map<Long, List<Action>> getActionsForState() {
		return actionsForState;
	}

	public void setActionsForState(Map<Long, List<Action>> actionsForState) {
		this.actionsForState = actionsForState;
	}

}
