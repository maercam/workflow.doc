package pl.dms.bean;

import java.io.IOException;
import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.omnifaces.util.Faces;
import org.omnifaces.util.Messages;

import pl.dms.bean.session.UserSessionBean;
import pl.dms.service.ws.DMSServiceWSPortTypeProxy;
import pl.dms.service.ws.types.Workflow;

@ManagedBean
@ViewScoped
public class WorkflowsBean implements Serializable {

	private static final long serialVersionUID = 5260657634553013729L;
	private static final Logger logger = Logger.getLogger(WorkflowsBean.class.toString());

	@ManagedProperty(value = "#{userSessionBean}")
	private UserSessionBean userSessionBean;

	private List<Workflow> workflows;

	public WorkflowsBean() {
	}

	@PostConstruct
	private void init() {
		logger.info("WorkflowsBean.init - invoked for user, userSessionBean.login: " + userSessionBean.getLogin());

		DMSServiceWSPortTypeProxy dmsProxy = new DMSServiceWSPortTypeProxy();

		try {
			Workflow[] workflowsTable = dmsProxy.getWorkflows(userSessionBean.getSid());

			if (workflowsTable != null) {
				workflows = Arrays.asList(workflowsTable);
			} else {
				workflows = new ArrayList<Workflow>();
			}

		} catch (RemoteException e) {
			logger.log(Level.SEVERE, "WorkflowsBean.init.RemoteException", e);
		}
	}

	public void navigateToWorkflow(long workflowId) {

		try {
			Faces.redirect("dashboard/workflow/workflow.xhtml?workflowId=%s", String.valueOf(workflowId));
		} catch (IOException e) {
			Messages.addGlobalError("Problem podczas przekierowania na stronę.");
		}

	}

	public List<Workflow> getWorkflows() {
		return workflows;
	}

	public void setWorkflows(List<Workflow> workflows) {
		this.workflows = workflows;
	}

	public UserSessionBean getUserSessionBean() {
		return userSessionBean;
	}

	public void setUserSessionBean(UserSessionBean userSessionBean) {
		this.userSessionBean = userSessionBean;
	}

}
