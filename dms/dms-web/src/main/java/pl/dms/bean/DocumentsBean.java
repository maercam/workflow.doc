package pl.dms.bean;

import java.io.IOException;
import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.omnifaces.util.Faces;
import org.omnifaces.util.Messages;

import pl.dms.bean.session.UserSessionBean;
import pl.dms.service.ws.DMSServiceWSPortTypeProxy;
import pl.dms.service.ws.types.DocumentProcess;
import pl.dms.util.UserRole;

@ManagedBean
@ViewScoped
public class DocumentsBean implements Serializable {

	private static final long serialVersionUID = -5908856166357416281L;

	private static final Logger logger = Logger.getLogger(DocumentsBean.class.toString());

	@ManagedProperty(value = "#{userSessionBean}")
	private UserSessionBean userSessionBean;

	private List<DocumentProcess> documents;

	public DocumentsBean() {
	}

	@PostConstruct
	public void init() {
		logger.info("DocumentsBean.init - invoked");

		DMSServiceWSPortTypeProxy dmsProxy = new DMSServiceWSPortTypeProxy();

		try {
			DocumentProcess[] documentArray = null;

			if (userSessionBean.getRoles().contains(UserRole.DMS_ADMIN.getName()) == true) {
				documentArray = dmsProxy.getAllDocuments(userSessionBean.getSid());
			} else {
				documentArray = dmsProxy.getPendingDocuments(userSessionBean.getSid());
			}

			if (documentArray != null) {
				documents = Arrays.asList(documentArray);
			}

		} catch (RemoteException e) {
			logger.log(Level.SEVERE, "DocumentBean.preRender.DMSServiceWSPortTypeProxy.documents.RemoteException", e);
		}

	}

	public void navigateToDocument(long documentId) {
		logger.info("DocumentsBean.navigateToDocument - invoke, documentId: " + documentId + ", userSessionBean.login: " + userSessionBean.getLogin());

		try {
			Faces.redirect("dashboard/document/document.xhtml?documentId=%s", String.valueOf(documentId));
		} catch (IOException e) {
			Messages.addGlobalError("Problem podczas przekierowania na stronę.");
		}

	}

	public UserSessionBean getUserSessionBean() {
		return userSessionBean;
	}

	public void setUserSessionBean(UserSessionBean userSessionBean) {
		this.userSessionBean = userSessionBean;
	}

	public List<DocumentProcess> getDocuments() {
		return documents;
	}

	public void setDocuments(List<DocumentProcess> documents) {
		this.documents = documents;
	}

}
