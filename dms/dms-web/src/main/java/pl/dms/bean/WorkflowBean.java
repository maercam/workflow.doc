package pl.dms.bean;

import java.io.IOException;
import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.omnifaces.util.Faces;
import org.omnifaces.util.Messages;

import pl.dms.bean.session.UserSessionBean;
import pl.dms.jpa.status.ActionType;
import pl.dms.jpa.status.StateStatus;
import pl.dms.jpa.status.StateType;
import pl.dms.service.ws.DMSServiceWSPortTypeProxy;
import pl.dms.service.ws.types.Action;
import pl.dms.service.ws.types.BaseResponse;
import pl.dms.service.ws.types.CreationResponse;
import pl.dms.service.ws.types.State;
import pl.dms.service.ws.types.Workflow;
import pl.dms.util.DMSWebUtil;
import pl.edu.agh.useraccounts.service.RoleServiceProxy;

@ManagedBean
@ViewScoped
public class WorkflowBean implements Serializable {

	private static final long serialVersionUID = 4644791165717654683L;
	private static final Logger logger = Logger.getLogger(WorkflowBean.class.toString());

	@ManagedProperty(value = "#{userSessionBean}")
	private UserSessionBean userSessionBean;

	private String newStateName;

	private Long workflowId;

	private String newActionName;
	private List<String> roles;
	private String selectedRole;
	private long fromStateId;
	private long selectedStateId;

	private List<State> statesForAction;

	private Workflow workflow;
	private List<State> statesForWorkflow;
	private Map<Long, List<Action>> actionsForState;

	public WorkflowBean() {
	}

	public void preRender() {
		logger.info("WorkflowBean.preRender, workflowId:" + workflowId);

		try {
			RoleServiceProxy roleProxy = new RoleServiceProxy();
			String[] rolesArray = roleProxy.getAllRoles();
			if (rolesArray != null) {
				roles = Arrays.asList(rolesArray);
			}
		} catch (RemoteException e) {
			logger.log(Level.SEVERE, "WorkflowBean.preRender.RoleServiceProxy.RemoteException", e);
		}

		if (Faces.isPostback() == false) {

			if (DMSWebUtil.isIdSet(workflowId) == true) {
				// edycja workflow

				DMSServiceWSPortTypeProxy dmsProxy = new DMSServiceWSPortTypeProxy();
				try {
					Workflow[] workflows = dmsProxy.getWorkflows(userSessionBean.getSid());
					if (workflows != null) {
						for (Workflow w : workflows) {
							if (workflowId.equals(w.getId()) == true) {
								workflow = w;
							}
						}
					}

					if (workflow != null) {
						State[] states = dmsProxy.getStates(userSessionBean.getSid(), workflowId);
						if (states != null) {
							statesForWorkflow = Arrays.asList(states);
							actionsForState = new HashMap<Long, List<Action>>();
							for (State state : statesForWorkflow) {
								Action[] actions = dmsProxy.getActions(userSessionBean.getSid(), state.getId());
								if (actions != null) {
									actionsForState.put(state.getId(), Arrays.asList(actions));
								}
							}

						}
					}

				} catch (RemoteException e) {
					logger.log(Level.SEVERE, "WorkflowBean.preRender.DMSServiceWSPortTypeProxy.RemoteException", e);
				} catch (Exception e) {
					logger.log(Level.SEVERE, "WorkflowBean.preRender.Exception", e);
				}

				// workflow = new Workflow(10, "Test workflow");

				// statesForWorkflow.add(new State(1, "Niezarejestrowany", StateStatus.EXAMPLE.getValue(), StateType.NOTREGISTERED.getValue()));
				// statesForWorkflow.add(new State(2, "Początek", StateStatus.EXAMPLE.getValue(), StateType.START.getValue()));
				// statesForWorkflow.add(new State(3, "Koniec", StateStatus.EXAMPLE.getValue(), StateType.END.getValue()));
				// statesForWorkflow.add(new State(4, "W urzędzie 1", StateStatus.EXAMPLE.getValue(), StateType.STANDARD.getValue()));
				// statesForWorkflow.add(new State(5, "W urzędzie 2", StateStatus.EXAMPLE.getValue(), StateType.STANDARD.getValue()));

				// actionsForState = new HashMap<Long, List<Action>>();
				// actionsForState.put(1L, Arrays.asList(new Action[] { new Action(1, 1, 2, UserRole.DMS_USER.getName(), "N_to_P", ActionType.REGISTER.getValue()) }));
				// actionsForState.put(2L, Arrays.asList(new Action[] { new Action(3, 2, 4, UserRole.DMS_USER.getName(), "P_to_WU1", ActionType.STANDARD.getValue()), new Action(2, 2, 5, UserRole.DMS_USER.getName(), "P_to_WU2", ActionType.STANDARD.getValue()) }));
				// actionsForState.put(4L, Arrays.asList(new Action[] { new Action(4, 4, 3, UserRole.DMS_USER.getName(), "WU1_to_K", ActionType.STANDARD.getValue()) }));
				// actionsForState.put(5L, Arrays.asList(new Action[] { new Action(5, 5, 3, UserRole.DMS_USER.getName(), "WU2_to_K", ActionType.STANDARD.getValue()) }));

			} else {
				// nowy workflow
				workflow = new Workflow();

			}

			selectedStateId = -1;

		}

	}

	public String nextStateName(long stateId) {
		for (State s : statesForWorkflow) {
			if (s.getId() == stateId) {
				return s.getName();
			}
		}
		return "";
	}

	public String roleNameForAction(long actionId) {
		for (List<Action> actions : actionsForState.values()) {
			for (Action action : actions) {
				if (action.getId() == actionId) {
					return action.getRole();
				}
			}
		}
		return "";
	}

	public void chosenStateId(long fromStateId) {
		logger.info("WorkflowBean.chosenStateId, fromStateId:" + fromStateId);
		this.fromStateId = fromStateId;

		statesForAction = new ArrayList<State>();
		for (State state : statesForWorkflow) {

			if (state.getType() == StateType.NOTREGISTERED.getValue() || state.getType() == StateType.START.getValue() || fromStateId == state.getId()) {
				continue;
			}
			statesForAction.add(state);

		}
	}

	public List<String> getRoles() {
		return roles;
	}

	public void setRoles(List<String> roles) {
		this.roles = roles;
	}

	public String getSelectedRole() {
		return selectedRole;
	}

	public void setSelectedRole(String selectedRole) {
		this.selectedRole = selectedRole;
	}

	public long getSelectedStateId() {
		return selectedStateId;
	}

	public void setSelectedStateId(long selectedStateId) {
		this.selectedStateId = selectedStateId;
	}

	public List<State> getStatesForAction() {
		return statesForAction;
	}

	public void createState() {
		logger.info("WorkflowBean.createState - invoked, newStateName: " + newStateName);

		try {

			State state = new State();
			state.setName(newStateName);
			state.setStatus(StateStatus.IDLE.getValue());
			state.setType(StateType.STANDARD.getValue());

			DMSServiceWSPortTypeProxy dmsProxy = new DMSServiceWSPortTypeProxy();
			CreationResponse response = dmsProxy.addState(userSessionBean.getSid(), state, workflow.getId());

			if (response.getStatus() == BaseResponse.OK) {
				logger.info("WorkflowBean.createState - created state with id: " + response.getId());
				Faces.redirect("dashboard/workflow/workflow.xhtml?faces-redirect=true&workflowId=%s", String.valueOf(workflowId));
			} else {
				logger.warning("WorkflowBean.createState - Bad response status: " + response.getStatus());
			}

		} catch (RemoteException e) {
			logger.log(Level.SEVERE, "WorkflowBean.createWorkflow.RemoteException", e);
		} catch (IOException e) {
			Messages.addGlobalError("Problem podczas przekierowania na stronę.");
		}
	}

	public void deleteState(long stateId) {
		logger.info("WorkflowBean.deleteState - invoked, stateId:" + stateId);

		try {

			DMSServiceWSPortTypeProxy dmsProxy = new DMSServiceWSPortTypeProxy();
			BaseResponse response = dmsProxy.deleteState(userSessionBean.getSid(), stateId);

			if (response.getStatus() == BaseResponse.OK) {
				logger.info("WorkflowBean.deleteState - deleted state with id: " + stateId);
				Faces.redirect("dashboard/workflow/workflow.xhtml?faces-redirect=true&workflowId=%s", String.valueOf(workflowId));
			} else {
				logger.warning("WorkflowBean.deleteState - Bad response status: " + response.getStatus());
				Messages.addGlobalError("Problem podczas usuwania stanu.");
			}

		} catch (RemoteException e) {
			logger.log(Level.SEVERE, "WorkflowBean.deleteState.RemoteException", e);
		} catch (IOException e) {
			Messages.addGlobalError("Problem podczas przekierowania na stronę.");
		}

	}

	public void createAction() {
		logger.info("WorkflowBean.addAction - invoked, newActionName:" + newActionName + ", fromStateId:" + fromStateId + ", selectedStateId:" + selectedStateId + ", selectedRole:" + selectedRole);

		// Action action = new Action(0, 1L, 2L, UserRole.DMS_USER.getName(), newActionName, ActionType.STANDARD.getValue());
		Action action = new Action();
		action.setFromStateId(fromStateId);
		action.setToStateId(selectedStateId);
		action.setName(newActionName);
		action.setRole(selectedRole);
		action.setType(ActionType.STANDARD.getValue());

		try {

			DMSServiceWSPortTypeProxy dmsProxy = new DMSServiceWSPortTypeProxy();
			CreationResponse response = dmsProxy.addAcction(userSessionBean.getSid(), action, fromStateId);

			if (response.getStatus() == BaseResponse.OK) {
				logger.info("WorkflowBean.createAction - added Action with id: " + response.getId());
				Faces.redirect("dashboard/workflow/workflow.xhtml?faces-redirect=true&workflowId=%s", String.valueOf(workflowId));
			} else {
				logger.warning("WorkflowBean.createAction - Bad response status: " + response.getStatus());
				Messages.addGlobalError("Problem podczas dodawania akcji.");
			}

		} catch (RemoteException e) {
			logger.log(Level.SEVERE, "WorkflowBean.deleteState.RemoteException", e);
		} catch (IOException e) {
			Messages.addGlobalError("Problem podczas przekierowania na stronę.");
		}
	}

	public void deleteAction(long actionId) {
		logger.info("WorkflowBean.deleteAction - invoked, actionId: " + actionId + ", workflowId: " + workflowId);

		try {

			DMSServiceWSPortTypeProxy dmsProxy = new DMSServiceWSPortTypeProxy();
			BaseResponse response = dmsProxy.deleteAction(userSessionBean.getSid(), actionId);

			if (response.getStatus() == BaseResponse.OK) {
				logger.info("WorkflowBean.deleteAction - deleted Action with id: " + actionId);
				Faces.redirect("dashboard/workflow/workflow.xhtml?workflowId=%s", String.valueOf(workflowId));
			} else {
				logger.warning("WorkflowBean.deleteAction - Bad response status: " + response.getStatus());
				Messages.addGlobalError("Problem podczas usuwania akcji.");
			}

		} catch (RemoteException e) {
			logger.log(Level.SEVERE, "WorkflowBean.deleteState.RemoteException", e);
		} catch (IOException e) {
			logger.log(Level.SEVERE, "WorkflowBean.deleteState.IOException", e);
			Messages.addGlobalError("Problem podczas przekierowania na stronę.");
		} catch (Exception e) {
			logger.log(Level.SEVERE, "WorkflowBean.deleteState.Exception", e);
		}

	}

	public void createWorkflow() {
		logger.info("WorkflowBean.createWorkflow - invoked");

		try {

			DMSServiceWSPortTypeProxy dmsProxy = new DMSServiceWSPortTypeProxy();
			CreationResponse response = dmsProxy.addWorkflow(userSessionBean.getSid(), workflow);

			if (response.getStatus() == BaseResponse.OK) {
				logger.info("WorkflowBean.createWorkflow - created workflow with id: " + response.getId());
				Faces.redirect("dashboard/workflow/workflow.xhtml?workflowId=%s", String.valueOf(response.getId()));
			} else {
				logger.warning("WorkflowBean.createWorkflow - Bad response status: " + response.getStatus());
			}

		} catch (RemoteException e) {
			logger.log(Level.SEVERE, "WorkflowBean.createWorkflow.RemoteException", e);
		} catch (IOException e) {
			logger.log(Level.SEVERE, "WorkflowBean.createWorkflow.IOException", e);
			Messages.addGlobalError("Problem podczas przekierowania na stronę.");
		}

	}

	public void deleteWorkflow() {
		logger.info("WorkflowBean.deleteWorkflow - invoked");

		try {

			DMSServiceWSPortTypeProxy dmsProxy = new DMSServiceWSPortTypeProxy();
			BaseResponse response = dmsProxy.deleteWorkflow(userSessionBean.getSid(), workflow.getId());

			if (response.getStatus() == BaseResponse.OK) {
				logger.info("WorkflowBean.deleteWorkflow - delete workflow with id: " + workflow.getId());
				Faces.redirect("dashboard/workflow/main.xhtml");
			} else {
				logger.warning("WorkflowBean.deleteWorkflow - Bad response status: " + response.getStatus());
				Messages.addGlobalError("Problem podczas usuwania workflow.");
			}

		} catch (RemoteException e) {
			logger.log(Level.SEVERE, "WorkflowBean.deleteWorkflow.RemoteException", e);
		} catch (IOException e) {
			Messages.addGlobalError("Problem podczas przekierowania na stronę.");
		}

	}

	public List<Action> getActionsForState(Long stateId) {

		if (stateId == null || actionsForState == null || actionsForState.get(stateId) == null) {
			return new ArrayList<Action>();
		}

		return actionsForState.get(stateId);
	}

	public Long getWorkflowId() {
		return workflowId;
	}

	public void setWorkflowId(Long workflowId) {
		this.workflowId = workflowId;
	}

	public Workflow getWorkflow() {
		return workflow;
	}

	public void setWorkflow(Workflow workflow) {
		this.workflow = workflow;
	}

	public List<State> getStatesForWorkflow() {
		return statesForWorkflow;
	}

	public void setStatesForWorkflow(List<State> statesForWorkflow) {
		this.statesForWorkflow = statesForWorkflow;
	}

	public Map<Long, List<Action>> getActionsForState() {
		return actionsForState;
	}

	public void setActionsForState(Map<Long, List<Action>> actionsForState) {
		this.actionsForState = actionsForState;
	}

	public String getNewStateName() {
		return newStateName;
	}

	public void setNewStateName(String newStateName) {
		this.newStateName = newStateName;
	}

	public String getNewActionName() {
		return newActionName;
	}

	public void setNewActionName(String newActionName) {
		this.newActionName = newActionName;
	}

	public UserSessionBean getUserSessionBean() {
		return userSessionBean;
	}

	public void setUserSessionBean(UserSessionBean userSessionBean) {
		this.userSessionBean = userSessionBean;
	}

}
