package pl.dms.bean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import pl.dms.jpa.status.ActionType;
import pl.dms.jpa.status.StateStatus;
import pl.dms.jpa.status.StateType;

@ManagedBean
@ApplicationScoped
public class StatusBean {

	private StateType notregister;
	private StateType standard;
	private StateType end;

	@PostConstruct
	private void init() {
		notregister = StateType.NOTREGISTERED;
		standard = StateType.STANDARD;
		end = StateType.END;
	}

	public String stateTypeName(int value) {
		return StateType.of(value).getDescription();
	}

	public String stateStatusName(int value) {
		return StateStatus.of(value).getDescription();
	}

	public String actionTypeName(int value) {
		return ActionType.of(value).getDescription();
	}

	public StateType getNotregister() {
		return notregister;
	}

	public void setNotregister(StateType notregister) {
		this.notregister = notregister;
	}

	public StateType getStandard() {
		return standard;
	}

	public void setStandard(StateType standard) {
		this.standard = standard;
	}

	public StateType getEnd() {
		return end;
	}

	public void setEnd(StateType end) {
		this.end = end;
	}

}
