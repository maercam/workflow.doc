package pl.dms.bean.session;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@ManagedBean
@SessionScoped
public class UserSessionBean implements Serializable {

	private static final long serialVersionUID = -1445441423556600697L;
	private static final Logger logger = LogManager.getLogger(UserSessionBean.class);

	private String login;
	private String sid;
	private boolean logged;
	private List<String> roles;

	public UserSessionBean() {
	}

	@PostConstruct
	public void init() {
		logger.info("UserSessionBean.init - session created");
		roles = new ArrayList<String>();
		logged = false;
		login = "";
		sid = "";
	}

	public boolean hasRole(String roleName) {
		for (String role : roles) {
			if (role.equals(roleName) == true) {
				return true;
			}
		}
		return false;
	}

	public List<String> getRoles() {
		return roles;
	}

	public void setRoles(List<String> roles) {
		this.roles = roles;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public boolean isLogged() {
		return logged;
	}

	public void setLogged(boolean logged) {
		this.logged = logged;
	}

	public String getSid() {
		return sid;
	}

	public void setSid(String sid) {
		this.sid = sid;
	}

}
