package pl.dms.bean;

import java.io.IOException;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.omnifaces.util.Faces;
import org.omnifaces.util.Messages;

import pl.dms.bean.session.UserSessionBean;
import pl.dms.service.ws.DMSServiceWSPortTypeProxy;
import pl.dms.service.ws.types.AuthenticationResponse;
import pl.dms.service.ws.types.BaseResponse;
import pl.edu.agh.useraccounts.service.RoleServiceProxy;

@ManagedBean
@RequestScoped
public class LoginBean {

	private static final Logger logger = Logger.getLogger(LoginBean.class.toString());

	@ManagedProperty(value = "#{userSessionBean}")
	private UserSessionBean userSessionBean;

	private String login;
	private String password;

	public LoginBean() {
	}

	public void login() {
		logger.info("LoginBean.login, try for login: " + login);

		try {

			DMSServiceWSPortTypeProxy dmsProxy = new DMSServiceWSPortTypeProxy();
			AuthenticationResponse response = dmsProxy.login(login, password);

			if (response.getStatus() == BaseResponse.OK) {

				logger.info("LoginBean.login success for login: " + login);
				userSessionBean.setLogin(login);
				userSessionBean.setLogged(true);
				userSessionBean.setSid(response.getSid());

				RoleServiceProxy roleProxy = new RoleServiceProxy();
				String[] roles = roleProxy.getUserRole(login);
				userSessionBean.setRoles(Arrays.asList(roles));

				Faces.redirect("dashboard/main.xhtml");

			} else {
				logger.info("LoginBean.login error for login: " + login);
				Messages.addGlobalError("Login i/lub hasło są niepoprawne.");
			}

			// if (login != null && login.equals("test") == true && password != null && password.equals("test") == true) {
			// userSessionBean.setLogged(true);
			// userSessionBean.setLogin("test");
			// userSessionBean.setRoles(Arrays.asList(new String[] { UserRole.DMS_USER.getName(), UserRole.DMS_REGISTER_DOCUMENT.getName() }));
			//
			// Faces.redirect("dashboard/main.xhtml");
			// } else if (login != null && login.equals("admin") == true && password != null && password.equals("admin") == true) {
			// userSessionBean.setLogged(true);
			// userSessionBean.setLogin("admin");
			// userSessionBean.setRoles(Arrays.asList(new String[] { UserRole.DMS_ADMIN.getName(), UserRole.DMS_REGISTER_DOCUMENT.getName() }));
			//
			// Faces.redirect("dashboard/main.xhtml");
			// } else {
			// Messages.addGlobalError("Login i/lub hasło są niepoprawne.");
			// }

		} catch (IOException e) {
			logger.log(Level.SEVERE, "LoginBean.login.IOException", e);
		}

	}

	public void logout() {
		logger.info("LoginBean.logout, try for login: " + login);
		try {
			Faces.invalidateSession();
			ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
			Faces.redirect(ec.getRequestContextPath() + "/index.xhtml");
		} catch (IOException e) {
			logger.log(Level.SEVERE, "LoginBean.logout.IOException" + e.getMessage());
		}
	}

	public UserSessionBean getUserSessionBean() {
		return userSessionBean;
	}

	public void setUserSessionBean(UserSessionBean userSessionBean) {
		this.userSessionBean = userSessionBean;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
